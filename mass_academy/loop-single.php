<?php if(have_posts()):
	while(have_posts()): the_post(); ?>
		<h1><?= the_title() ?></h1>
		<div class="post-meta">
			<?php get_blog_date($post->ID); ?>
		</div>
		<?php the_content() ?>
	<?php endwhile;
else: ?>
	<h2>Posts are coming soon!</h2>
<?php endif; ?>