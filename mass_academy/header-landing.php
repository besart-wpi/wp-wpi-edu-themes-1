<!--[if IE]><![endif]-->
<!DOCTYPE html>
<!--[if IE 9]><html class="no-js ie ie9" lang="en"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
  <title><?php wp_title(); ?></title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width" />
  <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.ico" />
  <?php wp_head() ?>
  <script src="https://use.typekit.net/odi2lsm.js"></script>
</head>
<body <?php body_class() ?>>

<?php if(is_front_page()): ?>
<h1 class="sr-only"><?= get_bloginfo('name') ?></h1>
<?php endif; ?>

<div id="body-wrap">
  <header class="page-header main" role="banner" aria-label="<?= get_bloginfo('name') ?> header">
    <div class="header-content-wrapper">
      <div class="logo-wrapper">
        <div class="logo">
          <a href="<?= bloginfo('url') ?>" title="Link to <?= get_bloginfo(); ?> home"><?= img(ot_get_option('header_logo'), array( 'class' => 'header-logo', 'alt' => get_bloginfo() . ' Header Logo' )) ?></a>
        </div>
        <div class="site-title-wrapper">
          <a href="<?= bloginfo('url') ?>" title="<?= get_bloginfo(); ?>"><div class="site-title"><?= get_bloginfo('name') ?></div></a>
        </div>
        <div class="desktop-search-box">
          <gcse:search></gcse:search>
        </div>
      </div>
    </div>

    <div class="search-and-menu">
      <div class="mobile-menu-button icon-menu top-bar-icon-mobile"></div>
      <div class="mobile-search-button icon-search top-bar-icon-mobile"></div>
    </div>
    <div class="search-wrapper mobile-dropdown-content">
      <gcse:search></gcse:search>
    </div>
    <nav class="menu-wrapper mobile-dropdown-content" role="navigation" aria-label="<?= get_bloginfo('name') ?> main navigation">
      <?php wp_nav_menu(array(
        'theme_location'  => 'main',
        'menu_id' => 'menu-main'
      )) ?>
    </nav>
  </header>