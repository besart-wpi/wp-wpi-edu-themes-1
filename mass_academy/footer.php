		<div class="footer-wrap">
      <footer class="main" role="contentinfo" arial-label="<?= get_bloginfo(); ?> Footer">
        <div class="footer-logo-wrapper">
          <a href="<?= bloginfo('url') ?>" title="<?= get_bloginfo(); ?>">
            <?php echo img( ot_get_option('header_logo'), array( 'class' => 'footer-logo', 'alt' => get_bloginfo() . ' Logo' )); ?>
          </a>
        </div>
        <div class="footer-contact-info">
          <div class="footer-site-title uppercase">
            <?php echo get_bloginfo(); ?>
          </div>
          <?php  apply_filters('footer_content', 'address'); ?>
          <?php  apply_filters('footer_content', 'phone_number'); ?>
          <?php  apply_filters('footer_content', 'email'); ?>
        </div>

        <div class="footer-text-links">
          <?php  apply_filters('footer_text_links', 'contact_us'); ?>
          <?php  apply_filters('footer_text_links', 'directions'); ?>
        </div>

        <div class="footer-social-media-icons">
          <?php  apply_filters('footer_social_media', 'facebook'); ?>
          <?php  apply_filters('footer_social_media', 'twitter'); ?>
        </div>

        <?php  apply_filters('bookstore_link', 'bookstore_link'); ?>

      </footer>
		</div>
	</div>
	<?php wp_footer(); ?>
</body>
</html>