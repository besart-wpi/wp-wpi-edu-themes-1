<?php
// Limit Theme Options to Network Admin
// =================================================
function ot_theme_options_custom_cap(){
	return 'manage_network';
}
add_filter('ot_theme_options_capability','ot_theme_options_custom_cap');

define('WPI_POST_TYPE_FRAMEWORKS', true);
define('WPI_PTF_HOMEPAGESLIDESHOW', true); 
define('WPI_PTF_TESTIMONIALS', true); 
define('WPI_PTF_PHOTOGALLERY', true);

$site_name = get_bloginfo();

load_template( trailingslashit( get_template_directory() ) . 'includes/easy-tinymce-class/Easy-tinyMCE.class.php' );
load_template( trailingslashit( get_template_directory() ) . 'includes/easy-tinymce-class/Easy-tinyMCE-content.class.php' );

// =================================================
// 	:: Strip Empty Paragraphs ::
//  Strip any content wrapped with attribute-less 
//  <p> tags - Like: <p>Sample</p>
//  This is a workaround for the_editor() stripping
//  line breaks and adding paragraph tags before
//  content is sent to the database. It causes
//  oEmbed to not work in this situation.
// -------------------------------------------------
function strip_empty_paragraphs($content){
	$content = preg_replace("/<p>(.*?)<\/p>/", "$1" . PHP_EOL . PHP_EOL, $content);
	return $content;
}
add_filter('the_content','strip_empty_paragraphs', 1);

// =================================================
// 	:: Post Order Whitelist ::
// -------------------------------------------------
global $post_order_whitelist;
$post_order_whitelist = array('home_page_slideshow', 'testimonials');

include 'inc/helpers.php';
include 'inc/framework-of-oz/init.php'; class Oz extends Framework_of_Oz{};
include 'inc/post-types.php';
include 'inc/theme-options.php';
include 'inc/widgets/testimonials.php';
include 'inc/quote-shortcode-tinymce/quote-shortcode-tinymce.php';
include 'lib/black-studio-tinymce-widget/black-studio-tinymce-widget.php';

// Filter Yoast Meta Priority
add_filter( 'wpseo_metabox_prio', function() { return 'low';});

// =================================================
// 	:: Menus ::
// -------------------------------------------------
register_nav_menus(array(
	'main'	=> 'Main Navigation'
));

// =================================================
// :: Thumbnails and Favicon::
// -------------------------------------------------
add_image_size('testimonial-small', 120, 120, true);
add_filter('upload_mimes', 'customMimes');
function customMimes($mimes){
	$mimes['ico'] = 'image/x-icon';
	return $mimes;
}


// =================================================
// :: Enqueues ::
// -------------------------------------------------
add_action('wp_enqueue_scripts', 'enqueue_mass_academy_scripts', 11);
function enqueue_mass_academy_scripts(){
  wp_deregister_style('wpi-parent-reset');
  wp_dequeue_style('wpi-parent-reset');
  
  wp_deregister_style( 'wpi-parent-highlight-box');
  wp_dequeue_style( 'wpi-parent-highlight-box');
	
  wp_enqueue_script('jquery');
  wp_enqueue_script('mass-academy-main', get_stylesheet_directory_uri() . '/js/main.min.js', array( 'jquery' ), 1, true);

	wp_enqueue_style( 'mass_academy-styles', get_stylesheet_directory_uri() . '/style.css');
}

function google_cse_snippet(){ ?>
  <script>
    (function() {
      var cx = '000720435508958471272:ht4r4lckytk';
      var gcse = document.createElement('script');
      gcse.type = 'text/javascript';
      gcse.async = true;
      gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(gcse, s);
    })();
  </script>
<?php }

add_action('wp_footer', 'google_cse_snippet');

// ======================================================
// :: Check if shortcode has attribute without a value ::
// ------------------------------------------------------
function is_flag( $flag, $atts ) {
  if (is_array($atts) || is_object($atts)) {
    foreach ( $atts as $key => $value ) {
      if ( $value === $flag && is_int( $key ) ) {
        return true;
      }
    }
    return false;
  }
}

// Function to build out the html for column shortcode
function get_column_shortcode_output($content, $classes, $large_text = NULL) {
  $output = "<div class='$classes'>";

  if (!empty($large_text)) {
    $output .= '<div class="large-text">' . $large_text . '</div>';
  }

  $output .= apply_filters('the_content', $content);
  $output .= "</div>";
  return $output;
}

// Function to build out the html for button shortcode
function get_button_shortcode_output($content, $button_wrapper_classes, $button_classes, $url) {
  $output = "<div class='button-wrapper$button_wrapper_classes'>";
  $output .= "<a href='$url' class='button $button_classes' title='$content'>$content</a>";
  $output .= "</div>";
  return $output;
}

// =================================================
// ::  Shortcodes::
// -------------------------------------------------
/**
 * Displays the current year
 * @return [STR] The month...just kidding, the year
 */

add_shortcode('year', 'shortcode_year');
function shortcode_year(){
	return date('Y');
}

/**
 * Displays a testimonial
 */
add_shortcode('quote', 'shortcode_testimonial');
function shortcode_testimonial($origAtts){
	$atts = shortcode_atts(array(
		'id'	=> 0
	), $origAtts);
	return displayTestimonial($atts['id']);
}

/*
 * Column Shortcode.
 * 
 * This shortcode accepts half and third attributes and adds classes and markup accordingly.
 */

$available_background_colors = array('red', 'dark-gray', 'light-gray');

function shortcode_column( $atts, $content = null ) {
  global $available_background_colors;
  $classes = 'col';

  $a = shortcode_atts( array(
        'background_color' => '',
        'large_text' => '',
    ), $atts );

  if ( in_array($a['background_color'], $available_background_colors) ) {
    $classes .= ' ' . $a['background_color'] . '-bg';
  } else {
    $classes .= ' white-bg';
  }

  if ( is_flag( 'show_on_mobile', $atts) && is_flag('third', $atts) ) {
    $classes .= ' show-on-mobile';
  }

  if ( is_flag( 'third', $atts ) ) {
    $classes .= ' third rounded-for-medium-up ';
  }

  if ( is_flag( 'half', $atts ) ) {
    $classes .= ' half';
  }

  // Take attribute to figure out positioning
  if ( is_flag( 'middle', $atts ) && is_flag('third', $atts) ) {
    $classes .= ' middle';
    $output = get_column_shortcode_output($content, $classes, $a['large_text']);
  } elseif ( is_flag( 'last', $atts ) ) {
    $classes .= ' last';
    $output = get_column_shortcode_output($content, $classes, $a['large_text']);
    $output .= '</div>'; // Close the .equalized-children div.
    $output .= '<div class="clear"></div>';
  } else {
    $classes .= ' first';
    $output = '<div class="row equalize-children columns-wrapper">';
    $output .= get_column_shortcode_output($content, $classes, $a['large_text']);
  }

  return $output;
}

add_shortcode('column', 'shortcode_column');


// Add button shortcode
$available_button_colors = array('red', 'gray');
function shortcode_button( $atts, $content = null ) {
  global $available_button_colors;
  $button_wrapper_classes = '';
  $output = '';
  $button_classes = 'rounded';

  $a = shortcode_atts( array(
    'button_color' => '',
    'url' => '',
    'wrapper_color' => '',
  ), $atts );

  if ( in_array($a['button_color'], $available_button_colors) ) {
    $button_classes .= ' ' . $a['button_color'] . '-bg button-has-bg-color';
  } else {
    $button_classes .= ' icon-right-arrow default-button';
  }

  // Take attribute to figure out positioning
  if (is_flag( 'first', $atts) && is_flag( 'last', $atts) ){
    $wrapper_bg = $a['wrapper_color'] ? $a['wrapper_color']. '-bg wrapper-has-bg-color' : '';
    $output = "<div class='buttons-wrapper $wrapper_bg'>";
    $output .= get_button_shortcode_output($content, $button_wrapper_classes, $button_classes, $a['url']);
    $output .= '</div>'; // Close the .button-wrapper div.
    $output .= '<div class="clear"></div>';
  } else {
    if (is_flag('first', $atts)) {
      $button_wrapper_classes .= ' first';
      $wrapper_bg = $a['wrapper_color'] ? $a['wrapper_color'] . '-bg wrapper-has-bg-color' : '';
      $output = "<div class='buttons-wrapper $wrapper_bg'>";
      $output .= get_button_shortcode_output($content, $button_wrapper_classes, $button_classes, $a['url']);
    }
    if (is_flag('last', $atts)) {
      $button_wrapper_classes .= ' last';
      $output = get_button_shortcode_output($content, $button_wrapper_classes, $button_classes, $a['url']);
      $output .= '</div>'; // Close the .button-wrapper div.
      $output .= '<div class="clear"></div>';
    }
    if (!is_flag('first', $atts) && !is_flag('last', $atts)) {
      $output = get_button_shortcode_output($content, $button_wrapper_classes, $button_classes, $a['url']);
    }
  }
  return $output;
}
add_shortcode('button', 'shortcode_button');


// Add shortcode to pull in timely content

function get_timely_content($atts){
  $a = shortcode_atts( array(
    'items' => 3,
    'title' => 'Timely Notices',
    'see_more_link' => '',
  ), $atts );

  $recent_posts = wp_get_recent_posts(array(
    'numberposts' => $a['items'], // Number of recent posts thumbnails to display
    'post_status' => 'publish' // Show only the published posts
  ));

  if  (!empty($recent_posts)) {
    $output = '<div class="timely-content-wrapper">';
    $output .= '<h2 class="timely-content-title line-below">';
    $output .= $a['title'] ? $a['title'] : 'Timely Notices';
    $output .= '</h2>';

    foreach ($recent_posts as $key => $post) {
      $date = strtotime($post['post_date']);
      $formatted_date = date('F d, Y', $date);
      $end_date = get_post_meta($post['ID'], 'end-date', false);
      $end_date = isset($end_date[0]) ? trim($end_date[0]) : '';
      $end_date = !empty($end_date) ? ' - ' . $end_date : '';
      $link = get_post_permalink($post['ID']);

      $output .= '<article class="timely-content-item item-' . ++$key . '">';
      $output .= '<div class="timely-content-item-date">';
      $output .= '<a href="'.$link.'" title="' . $post['post_title'] . '" class="timely-content-date-link">';
      $output .= $formatted_date . $end_date;
      $output .= '</a>';
      $output .= '</div>';
      $output .= '<p class="timely-content-item-title">' . $post['post_title'] . '</p>';
      $output .= '</article>'; // close .timely-content-item
    }

    if ($a['see_more_link'] !== '') {
      $output .= '<a href="' . $a['see_more_link'] . '" class="button default-button" title="Go to See More content"><span>See More<span></span></a>';
    }

    $output .= '</div>'; // Close the timely-content-wrapper

    return $output;
  }

  return NULL;
}

add_shortcode('timely_content', 'get_timely_content');

// =================================================
// :: Add Editor Style ::
// -------------------------------------------------
add_action('init', 'custom_editor_styles');
function custom_editor_styles(){
	add_editor_style('css/editor.css');
}
add_filter('mce_buttons_2', 'add_styleselect_to_tinymce');
function add_styleselect_to_tinymce( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
add_filter( 'tiny_mce_before_init', 'custom_tinymce_before_inserts' );  
function custom_tinymce_before_inserts( $init ){
	$formats = array(
		array(
			'title'		=> 'Callout',
			'block'		=> 'div',
			'classes'   => 'inline-callout',
			'wrapper'	=> true
		)
	);
	$init['style_formats'] = json_encode($formats);
	return $init;
}

// =================================================
// ::  Sidebars ::
// -------------------------------------------------
register_sidebar(array(
	'name'	=> 'News',
	'id'	=> 'news',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
));
register_sidebar(array(
	'name'	=> 'Global',
	'id'	=> 'global',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
));

register_sidebar(array(
	'name'	=> 'Home Page',
	'id'	=> 'home',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h3 class="widgettitle">',
	'after_title'   => '</h3>'
));
register_sidebar(array(
	'name'	=> 'Home Page Footer',
	'id'	=> 'homefooter',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h3 class="widgettitle">',
	'after_title'   => '</h3>'
));

/**
 * Helps determine if the content should be displayed 
 * in full width or not
 * 
 * @param  string $template [Template identifier]
 * @return [STR]
 */
function getSidebar($template = 'default'){
	global $post;
	ob_start();
		switch($template){
			case 'news': 
			case 'single':
				dynamic_sidebar('news');
				if($sidebar = get_post_meta($post->ID, 'sidebar', true)){
					$sidebar = array_merge(array('content'=>''), $sidebar);
					echo apply_filters('the_content', $sidebar['content']);
				}
			break;
			case 'page':
				if($sidebar = get_post_meta($post->ID, 'sidebar', true)){
					$sidebar = array_merge(array('content'=>''), $sidebar);
					echo apply_filters('the_content', $sidebar['content']);
				}
			break;
		}
	return ob_get_clean();
}

// =================================================
// :: Blog ::
// -------------------------------------------------
/**
 * Gets the archive label to show next to "News" 
 * (or whatever it's set to)
 * 
 * @return [STR]
 */
function getArchiveLabel(){
	global $wp_query;
	if(is_category()) {
		$cat = get_category(get_query_var('cat'),false);
		return '- Category:  <strong>' . $cat->name . '</strong>';
	}
	if(is_tag()){
		$tag = get_term_by('slug', get_query_var('tag'), $wp_query->tax_query->queries[0]['taxonomy']);
		return '- Tag: <strong>' . $tag->name . '</strong>';
	}
	if(is_author()){
		return '- Author:  <strong>' . get_query_var('author_name') . '</strong>';
	}
	if(is_archive()){
		return '- Archive: <strong>' . get_query_var('monthnum') . '/' . get_query_var('year') . '</strong>';		
	}
	if(is_search()){
		return '- ' . get_search_query();
	}
}

// Remove the site title from the page title
function getPageTitle() {
  $page_title = wp_title('', 0);
  $site_title = '- ' . get_bloginfo('name', 0);
  $found_it = strpos($page_title, $site_title);
  if ( $found_it !== false ) {
    $page_title = substr($page_title, 0, $found_it);
  }
  $page_title = trim($page_title);
  return print $page_title;
}

// =================================================
// :: Homepage Slideshow Meta Box ::
// -------------------------------------------------
add_action( 'admin_init', 'mass_academy_homepage_slideshow_meta_box' );

function mass_academy_homepage_slideshow_meta_box() {
	$slideshow_meta_box = array(
		'id'          => 'slideshow_meta_box',
		'title'       => __( 'Slide Details', 'wpi-parent' ),
		'desc'        => '',
		'pages'       => array( 'home_page_slideshow' ),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			array(
				'label'       => __( 'Slide Link URL', 'mass-academy' ),
				'desc' 		  => __( 'This link will apply to the entire slide image', 'mass-academy'),
				'id'          => 'link_url',
				'type'        => 'text'
			),
		)
	);
	
	if ( function_exists( 'ot_register_meta_box' ) )
		ot_register_meta_box( $slideshow_meta_box );
}


// Our Team plugin display options.
// array of fields to remove from Our Team plugin
$fields_to_remove = array('url', 'tel', 'twitter', 'contact_email');
foreach ($fields_to_remove as $field) {
  add_filter( 'woothemes_our_team_member_' . $field, '__return_false' );
}

// Function to get information into the different sections of the footer.
function get_footer_content($footer_content) {
  global $site_name;
  $content = ot_get_option('footer_' . $footer_content);
  if (!empty($content)) {

    $class = str_replace('_', '-', $footer_content);

    $output = "<div class='footer-{$class}'>";
    if ($footer_content == 'email') {
      $output .= "<a href='mailto:{$content}' title='Email {$site_name}' class='footer-{$class}-link'>{$content}</a>";
    }
    elseif ($footer_content == 'phone_number') {
      $output .= '<span>Ph:</span> ' . $content;
    }
    elseif ($footer_content == 'address') {
      $output .= "<address>{$content}</address>";
    }
    else {
      $output .= $content;
    }
    $output .= "</div>";
    return print $output;
  }
  else {
    return NULL;
  }
}
add_filter('footer_content', 'get_footer_content', 1, 1);

// Get social media icons for the footer
function get_footer_social_links($social_network_url) {
  $social_link = ot_get_option('footer_' . $social_network_url . '_link');
  if(!empty($social_link)) {
    $social_network_name = get_social_network_name($social_network_url);
    $link_title = __('Link to ' . get_bloginfo() . ' ' . $social_network_name . ' Page');
    $class = 'footer-social-media-icon icon-' . strtolower($social_network_name);
    $output = "<a href='$social_link' title='$link_title' alt='$social_network_name Logo' class='$class' target='_blank'></a>";
    return print $output;
  }
  return NULL;
}

function get_social_network_name($social_network_url) {
  if (strpos($social_network_url, 'twitter') !== false) {
    return 'Twitter';
  }
  elseif (strpos($social_network_url, 'facebook') !== false) {
    return 'Facebook';
  }
  else {
    return NULL;
  }
}
add_filter('footer_social_media', 'get_footer_social_links', 1, 1);

// Get text links in the footer
function get_footer_text_links($link_name) {
  global $site_name;
  if ($link_name == 'contact_us' && ot_get_option('footer_contact_us')) {
    $link = ot_get_option('footer_contact_us');
    return print "<a href='$link' title='Contact " . $site_name . "'>Contact Us</a>";
  }
  elseif ($link_name == 'directions') {
    $link = ot_get_option('footer_directions');
    return print "<a href='$link' title='Directions to " . $site_name . "'>Directions</a>";
  }
  return NULL;
}
add_filter('footer_text_links', 'get_footer_text_links');

// Get bookstore link for the footer
function get_bookstore_link() {
  $link = ot_get_option('footer_bookstore_link');
  $output = "<div class='bookstore-link-wrapper'>";
  $output .= "<a title='Link to the Bookstore' href='$link'><img src='" . get_stylesheet_directory_uri() . "/img/bookstore-icon.svg' alt='Bookstore ribbon image' /></a>";
  $output .= "</div>";

  return print $output;
}
add_filter('bookstore_link', 'get_bookstore_link');

// =================================================
// :: Homepage Slideshow Meta Box ::gt
// -------------------------------------------------
add_theme_support( 'post-thumbnails' );
the_post_thumbnail( 200, 150 );


function get_blog_date($post_id){
  $start_date = get_the_time('F j, Y', $post_id);
  $end_date = get_post_meta($post_id, 'end-date', false);
  $output = "<div class='date-wrapper'>";

  if (!empty($start_date)) {
    $output .= '<span class="start-date">' . $start_date . '</span>';
    if (!empty($end_date[0])) {
      $output .= '<span class="date-separator"> - </span>';
      $output .= '<span class="end-date">' . $end_date[0] . '</span>';
    }
  } 
  else {
    $blog_post_date = get_the_date(get_option( 'date_format' ), $post_id);
    $output .= "<span class='blog-post-date'>$blog_post_date</span>";
  }

  $output .= "</div>";
  return print $output;

}

add_action('get_blog_date', 'get_blog_date');

function onetarek_prevent_future_type( $post_data ) {
  if ( $post_data['post_status'] == 'future' && $post_data['post_type'] == 'post' ) {
    $post_data['post_status'] = 'publish';
  }
  return $post_data;
}
add_filter('wp_insert_post_data', 'onetarek_prevent_future_type');
remove_action('future_post', '_future_post_hook');


