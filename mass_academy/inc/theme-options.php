<?php
/**
 * Initialize the custom Theme Options.
 */
add_action( 'admin_init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 *
 * @return    void
 * @since     2.0
 */
function custom_theme_options() {
  
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( ot_settings_id(), array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
   
	$custom_settings = array(

    'sections'        => array(
      array(
        'id'          => 'section_logo',
        'title'       => __( 'Logos', 'wpi-mpi' )
      ),
      array(
        'id'          => 'section_footer_options',
        'title'       => __( 'Footer Options', 'wpi-mpi' )
      ),
    ),
		
		'settings'        => array( 
			// Footer Options
      array(
        'id'     	=> 'footer_address',
        'label'  	=> __( 'Address', 'wpi-mpi' ),
        'type'   	=> 'textarea',
        'section'	=> 'section_footer_options',
      ),
      array(
        'id'     	=> 'footer_phone_number',
        'label'  	=> __( 'Phone Number', 'wpi-mpi' ),
        'type'   	=> 'text',
        'section'	=> 'section_footer_options',
      ),
      array(
        'id'     	=> 'footer_email',
        'label'  	=> __( 'Email', 'wpi-mpi' ),
        'type'   	=> 'text',
        'section'	=> 'section_footer_options',
      ),
      array(
        'id'     	=> 'footer_contact_us',
        'label'  	=> __( 'Contact Us', 'wpi-mpi' ),
        'type'   	=> 'text',
        'section'	=> 'section_footer_options',
      ),
      array(
        'id'     	=> 'footer_directions',
        'label'  	=> __( 'Directions', 'wpi-mpi' ),
        'type'   	=> 'text',
        'section'	=> 'section_footer_options',
      ),
      array(
        'id'     	=> 'footer_facebook_link',
        'label'  	=> __( 'Facebook Link', 'wpi-mpi' ),
        'type'   	=> 'text',
        'section'	=> 'section_footer_options',
      ),
      array(
        'id'     	=> 'footer_twitter_link',
        'label'  	=> __( 'Twitter Link', 'wpi-mpi' ),
        'type'   	=> 'text',
        'section'	=> 'section_footer_options',
      ),
      array(
        'id'     	=> 'footer_bookstore_link',
        'label'  	=> __( 'Bookstore Link', 'wpi-mpi' ),
        'type'   	=> 'text',
        'section'	=> 'section_footer_options',
      ),

      // Header Options
      array(
        'id'          => 'favicon',
        'label'       => __( 'Favicon', 'wpi-mpi' ),
        'type'        => 'upload',
        'section'     => 'section_logo',
      ),
      array(
        'id'          => 'header_logo',
        'label'       => __( 'Logo', 'wpi-mpi' ),
        'type'        => 'upload',
        'section'     => 'section_logo',
      ),
		)
	);
  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( ot_settings_id() . '_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( ot_settings_id(), $custom_settings ); 
  }
  
}
