<?php get_header();?>
	<section id="the-content" class="error404-page-content" role="contentinfo" aria-label="Page not found">
		<div id="content">
			<article class="main c12">
				<h2>404 Page Not Found</h2>
				<p>Sorry, but the page you were looking for is not here.</p>
        <gcse:search linktarget="_parent"></gcse:search>
      </article>
			<div class="clear"></div>
		</div>
		</div>
	</section>
<?php get_footer() ?>