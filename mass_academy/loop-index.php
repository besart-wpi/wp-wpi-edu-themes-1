<?php if(have_posts()):
	while(have_posts()): the_post(); ?>
		<h2 class="blog-title"><a href="<?= the_permalink() ?>" title="<?= the_title() ?>"><?= the_title() ?></a></h2>
		<div class="post-meta">
			<?php get_blog_date($post->ID); ?>
		</div>
		<?php 
			the_excerpt();
			$the_title = 'Read more about "' . wp_trim_words(get_the_title(), 3, '...'). '"';
		?>
		<p><a href="<?php the_permalink() ?>" title="<?= __('Read more about ' . get_the_title()) ?>"><?= __($the_title) ?></a>
		<br>
		<br>
	<?php endwhile;
else: ?>
	<h2>Posts are coming soon!</h2>
<?php endif; ?>
