<?php get_header() ?>
<?php $slides = get_posts(array(
  'post_type'	=> 'home_page_slideshow',
  'posts_per_page'	=> 0,
  'orderby'	=> 'menu_order',
  'order'		=> 'ASC'
)); ?>

<?php if(!empty($slides)): ?>
	<div class="front-page cycle-slideshow" data-cycle-slides="> div">

    <?php
		foreach($slides as $slide): 
			// =================================================
			// :: Display the thumbnail as a background ::
			// -------------------------------------------------
			$bg = get_post_thumbnail_id($slide->ID);
			$bg = get_post($bg);
			if($bg){
				$bg = 'style="background: url('. $bg->guid. '); background-position: center; background-repeat: no-repeat; background-size: cover;"';
			}
			// Get link URL
			$link_url = get_post_meta($slide->ID, 'link_url', true);
		?>
			<div <?php echo $bg; ?>>
				<section role="contentinfo" aria-label="Home page slider">
					<div class="content-wrap">
						<?php if($slide->post_title): ?><h1><span><?= $slide->post_title ?></span></h1><?php endif; ?>
						<?php if($slide->post_content): ?><div class="content"><?= apply_filters('the_content', $slide->post_content) ?></div><?php endif; ?>
					</div>
				</section>
			</div>
		<?php endforeach; ?>
		<?php if(count($slides) > 1): ?>
		    <span class="cycle-pager"></span>
		<?php endif; ?>
	</div>

  <?php endif; ?>
	<section id="the-content" class="font-page-content" role="contentinfo" aria-label="Home page content">
		<div id="content">
			<?php //- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
			// Intro block
			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
			if($post->post_content): ?>
				<div class="intro">
					<?= apply_filters('the_content', $post->post_content) ?>
				</div>
			<?php endif; ?>

			<?php

      /*
			 * Displays either content as full width or with sidebar
			*/

			$sidebar = get_post_meta($post->ID, 'right-side', true);
			$theContent = Oz::getField('content', 'the-content');
			if(!is_array($sidebar)) $sidebar = array();
			$sidebar = array_merge(array('sidebar'=>''), $sidebar);
			$contentWidth = $sidebar['sidebar'] ? 'c6' : 'c12';
			?>

      <?php if (!empty($theContent)): ?>
      <div class="font-page-content-wrapper cf">
        <article class="main <?= $contentWidth ?>">
          <?php while(have_posts()): the_post(); ?>
            <?= apply_filters('the_content', $theContent) ?>
          <?php endwhile; ?>
        </article>
        <?php if($sidebar['sidebar']): ?>
        <article class="main c6">
          <?= apply_filters('the_content', $sidebar['sidebar']) ?>
        </article>
        <?php endif; ?>
      </div>
      <?php endif; ?>
			<?php get_sidebar('home'); ?>            
            
		</div>
        
        <?php get_sidebar('homefooter'); ?>
	</section>
<?php get_footer() ?>