<?php get_header(); ?>
  <?php if($featuredImage): ?>
    <div class="hero">
      <header>
        <h1><?= ot_get_option('landing_pages_news_title') ?> <?= getArchiveLabel() ?></h1>
      </header>
    </div>
  <?php endif; ?>
  <?php 

  $title = wp_title('', 0);
  if (strpos('asdfadf', 'asd')) {
   	echo 'i exist';
   } ?>
	<section id="the-content" role="contentinfo" aria-label="<?= wp_title(''); ?>">
    <h1 class="page-title"><?php getPageTitle(); ?></h1>
    <div id="content">
			<?php //- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
			// Displays either content as full width or with sidebar
			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
			$sidebar = getSidebar('single');
      $hasSidebar = is_active_sidebar('global') || $sidebar;
      $fullWidth = $hasSidebar ? ' c8' : '';
			?>
      <article class="main<?= $fullWidth; ?>">
        <?php get_template_part('loop', 'index') ?>
				<?php posts_nav_link(); ?> 
			</article>
			<?php if($sidebar): ?>
				<aside class="main c4">
					<ul>
						<?= apply_filters('the_content', $sidebar) ?>
					</ul>
				</aside>
			<?php endif; ?>
			<div class="clear"></div>
		</div>
	</section>

<?php get_footer(); ?>