<?php

// custom theme functions

/**
 * Override the featured image function so we can get a larger sized image instead of a cropped version
 */
function wpi_solostream_feature_image_wide() {
    global $post;
    if (has_post_thumbnail()) {
        $img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large');
        $img = $img[0];
    } else {
        $img = false;
    }
    if ( $img && get_post_meta( $post->ID, 'remove_thumb', true ) != 'Yes' ) {
        ?><img src="<?php echo $img; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" /><?php
    } else {
        ?><img src="<?php echo get_stylesheet_directory_uri() . '/img/blank.gif'; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" /><?php
    }
}

/**
 * Overrides some of the Venture javascript so the homepage slideshow will work when there's only one slide.
 */
function wpi_custom_js() {

    // override some custom functions
    wp_register_script(
        'wpi-custom',
        get_stylesheet_directory_uri() . '/js/wpi-custom.js',
        array('jquery'), // depend on jquery so our scripts are loaded after it
        '1.0',
        true
    );
    wp_enqueue_script('wpi-custom');
    
}
add_action('wp_print_scripts', 'wpi_custom_js', 100);

function wpi_add_admin_styles() {
    wp_enqueue_style('wpi-admin-settings', get_stylesheet_directory_uri().'/css/settings.css', array('theme-settings'));
}
add_action('admin_menu', 'wpi_add_admin_styles');
