<?php
/*
Template Name: Strategic Plan Interior Page
*/

get_header();

global $post;
$args=array(
  'order'=>'ASC',
  'orderby'=> 'menu_order',
  'post_type' => 'page',
  'post_status' => 'publish',
  'posts_per_page' => 100,
  'post_parent' => $post->ID // Only get direct children of the current page
);
query_posts($args);
?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php get_template_part( 'partials/loop-section-block-sp-interior' ); ?>
<?php endwhile; endif; ?>

<?php get_footer(); ?>
