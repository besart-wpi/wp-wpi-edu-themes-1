<?php

//echo 'STYLESHEET DIRECTORY URI IS:';
//echo get_stylesheet_directory_uri();

$path = explode("/", get_stylesheet_directory_uri());
$directoryname = $path[3];
//echo "\nDIRECTORY NAME IS: $directoryname.";

global $siteIsSP;
$siteIsSP = ($directoryname == "strategicplan");

$bodyClasses = array();
if ($siteIsSP) {
    $bodyClasses[] = 'strategicplan';
}

?><!--[if IE]><![endif]-->
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js ie ie6"><![endif]-->
<!--[if IE 7]><html class="no-js ie ie7"><![endif]-->
<!--[if IE 8]><html class="no-js ie ie8"><![endif]-->
<!--[if IE 9]><html class="no-js ie ie9"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js"><!--<![endif]-->
<head>
    <title><?php wp_title(' | ', true, 'right'); ?></title>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <meta name="viewport" content="width=device-width" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" />

    <!--[if !lte IE 8]><!-->
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/flexnav.css" />
    <!--<![endif]-->

    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

<!--
/**
 * @license
 * MyFonts Webfont Build ID 2703593, 2013-12-07T12:46:37-0500
 *
 * The fonts listed in this notice are subject to the End User License
 * Agreement(s) entered into by the website owner. All other parties are
 * explicitly restricted from using the Licensed Webfonts(s).
 *
 * You may obtain a valid license at the URLs below.
 *
 * Webfont: Museo Sans 700 by exljbris
 * URL: http://www.myfonts.com/fonts/exljbris/museo-sans/700/
 * Copyright: Copyright (c) 2008 by Jos Buivenga. All rights reserved.
 *
 * Webfont: Museo Sans 300 by exljbris
 * URL: http://www.myfonts.com/fonts/exljbris/museo-sans/300/
 * Copyright: Copyright (c) 2008 by Jos Buivenga. All rights reserved.
 *
 * Webfont: Museo Sans 500 by exljbris
 * URL: http://www.myfonts.com/fonts/exljbris/museo-sans/500/
 * Copyright: Copyright (c) 2008 by Jos Buivenga. All rights reserved.
 *
 * Webfont: Museo Sans 300 Italic by exljbris
 * URL: http://www.myfonts.com/fonts/exljbris/museo-sans/300-italic/
 * Copyright: Copyright (c) 2008 by Jos Buivenga. All rights reserved.
 *
 * Webfont: Museo Slab 100 by exljbris
 * URL: http://www.myfonts.com/fonts/exljbris/museo-slab/100/
 * Copyright: Copyright (c) 2009 by Jos Buivenga. All rights reserved.
 *
 * Webfont: Museo Slab 300 by exljbris
 * URL: http://www.myfonts.com/fonts/exljbris/museo-slab/300/
 * Copyright: Copyright (c) 2009 by Jos Buivenga. All rights reserved.
 *
 * Webfont: Museo Slab 500 by exljbris
 * URL: http://www.myfonts.com/fonts/exljbris/museo-slab/500/
 * Copyright: Copyright (c) 2009 by Jos Buivenga. All rights reserved.
 *
 *
 * License: http://www.myfonts.com/viewlicense?type=web&buildid=2703593
 * Licensed pageviews: 10,000
 *
 * © 2013 MyFonts Inc
*/

-->

<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/webfonts.css" />
<?php wp_head(); ?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/animatescroll.js"></script>

<!--[if !lte IE 8]><!-->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.flexnav.min.js"></script>
<!--<![endif]-->

<script>
// add HTML5 support to IE
document.createElement('header');document.createElement('footer');document.createElement('section');document.createElement('aside');document.createElement('nav');document.createElement('article');
</script>

<style type="text/css">
    /* background-size polyfill for IE8 */
    .block-footer,
    .elem1,
    .elem2 {
        /* The url is relative to the document, not to the css file! */
        /* Prefer absolute urls to avoid confusion. */
        -ms-behavior: url(<?php echo get_stylesheet_directory_uri(); ?>/js/backgroundsize/backgroundsize.min.htc);
    }
</style>

<?php
global $interior;
if(!is_front_page()){ $interior = true; $bodyClasses[] = 'interior'; } ?>
</head>
<body <?php body_class($bodyClasses); ?>>
<div id="wrapper" class="hfeed">

<?php

if ($siteIsSP) {

?>

    <div id="wpibanner" <?php if ( is_admin_bar_showing() ) { echo ' style="top: 32px;"';   } ?>>
        <div class="elastic">
            <span>
                <a href="http://wp.wpi.edu/strategicplan"></a>
                <h1 class="site-title-header">STRATEGIC PLAN 2015</h1>
            </span>
        </div>
        <nav id="menu" class="sp-menu" role="navigation">
            <div class="menu-button sp-mobilemenu">Menu</div>
            <ul class="fl flexnav one-page" data-breakpoint="900">
                <?php
                wp_nav_menu(array(
                    'theme_location'  => 'main-menu',
                    'container'       => '',
                    'fallback_cb'     => '',
                    'items_wrap'      => '%3$s'
                )); ?>
            </ul>
        </nav>
    </div>

<?php  } else {  ?>

    <header id="header" role="banner"<?php if ( is_admin_bar_showing() ) { echo ' style="top: 32px;"';  } ?>>
        <nav id="menu" role="navigation">
            <div class="menu-button">Menu</div>
            <ul class="fl flexnav one-page" data-breakpoint="900">
                <?php
                wp_nav_menu(array(
                    'theme_location'  => 'main-menu',
                    'container'       => '',
                    'fallback_cb'     => '',
                    'items_wrap'      => '%3$s'
                )); ?>
            </ul>
        </nav>
    </header>
<?php  }  ?>


<div class="main" id="container">
