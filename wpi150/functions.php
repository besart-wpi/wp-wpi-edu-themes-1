<?php
// =================================================
//	:: Parent Theme Components ::
// ------------------------------------------------
define('WPI_OPTIONTREE', true); // OptionTree
define('WPI_EASY_TINYMCE', false); // Easy TinyMCE Class
define('WPI_POST_ORDER', true); // Post Ordering
define('WPI_VISUAL_SHORTCODES', false); // Visual Shortcodes
define('WPI_HIGHLIGHT_BOX_SHORTCODE', false); // Highlight Box Shortcode
define('WPI_VIDEO_GALLERY_SHORTCODE', false); // Video Gallery Shortcode


// =================================================
//	:: Theme Options ::
// -------------------------------------------------
add_filter( 'ot_use_theme_options', '__return_true' ); // Show the Theme Options page
add_filter( 'ot_override_forced_textarea_simple', '__return_true' );
load_template( trailingslashit( get_stylesheet_directory() ) . 'includes/custom-walker.php' );
load_template( trailingslashit( get_stylesheet_directory() ) . 'includes/custom-functions.php' );
load_template( trailingslashit( get_stylesheet_directory() ) . 'includes/theme-options.php' );
load_template( trailingslashit( get_stylesheet_directory() ) . 'includes/meta-boxes.php' );


// =================================================
// 	:: Post Order Whitelist ::
// -------------------------------------------------
// Add post types to the $post_order_whitelist array to enable manual sorting
global $post_order_whitelist;
$post_order_whitelist = array();


// =================================================
// 	:: Menus ::
// -------------------------------------------------
register_nav_menus(array(
	'main'	=> 'Main Navigation'
));


// =================================================
// :: Thumbnails ::
// -------------------------------------------------
add_image_size('event-thumbnail', 96, 96, true);
add_image_size('footer-graphic', 60, 57, true);
add_image_size('footer-graphic2x', 120, 114, true);
add_image_size('event-graphic', 580, 404, true);
add_image_size('event-graphic-thumb', 210, 80, true);
add_image_size('placeholder-size', 290, 202, true);
add_image_size('event-size', 683, 384, true);


// =================================================
// :: Enqueues ::
// -------------------------------------------------
add_action('wp_enqueue_scripts', 'enqueue_wpi150_scripts', 11);
function enqueue_wpi150_scripts(){

	wp_enqueue_style( 'theme-style', get_stylesheet_uri());
	wp_enqueue_style( 'wpi150-jscrollpane', get_stylesheet_directory_uri() . '/css/jquery.jscrollpane.css'); 
	wp_enqueue_style( 'wpi150-colorbox', get_stylesheet_directory_uri() . '/css/colorbox.css'); 
	wp_enqueue_style( 'wpi150-responsive', get_stylesheet_directory_uri() . '/css/responsive.css');
		
	wp_enqueue_script('jquery');
	wp_enqueue_script('jquery-cycle-2', get_template_directory_uri() . '/js/lib/cycle2/jquery.cycle2.min.js', array( 'jquery' ), true);
	wp_enqueue_script('jquery-cycle-2-carousel', get_template_directory_uri() . '/js/lib/cycle2/jquery.cycle2.carousel.min.js', array( 'jquery' ), true);
	wp_enqueue_script('jquery-mapster', get_stylesheet_directory_uri() . '/js/jquery.imagemapster.min.js', array( 'jquery' ), true);
	wp_enqueue_script('jquery-cycle-2-swipe', get_stylesheet_directory_uri() . '/js/jquery.cycle2.swipe.min.js', array( 'jquery' ), true);
	wp_enqueue_script('jquery-jscrollpane', get_stylesheet_directory_uri() . '/js/jquery.jscrollpane.min.js', array( 'jquery' ), true);
	wp_enqueue_script('jquery-mousewheel', get_stylesheet_directory_uri() . '/js/jquery.mousewheel.js', array( 'jquery' ), true);
	wp_enqueue_script('jquery-colorbox', get_stylesheet_directory_uri() . '/js/jquery.colorbox-min.js', array( 'jquery' ), true);
	wp_enqueue_script('srcset', get_stylesheet_directory_uri() . '/js/srcset.min.js', array(), true);
	wp_enqueue_script('theme-functions', get_stylesheet_directory_uri() . '/js/main.js', array( 'jquery' ), true);
	
	
}

add_action('admin_enqueue_scripts', 'enqueue_wpi150_admin_scripts');   
function enqueue_wpi150_admin_scripts(){
	global $post_type;
    if( 'timeline_event' == $post_type ){
		wp_enqueue_script('jquery-validate', get_stylesheet_directory_uri() . '/js/jquery.validate.min.js', array('jquery'));
		wp_enqueue_script('theme-admin-js', get_stylesheet_directory_uri() . '/js/admin.js');
		
		wp_enqueue_style( 'wpi150-admin-styles', get_stylesheet_directory_uri() . '/css/admin.css'); 
	}
}

// =================================================
// :: Radio Buttons for Taxonomies ::
// -------------------------------------------------
add_action('after_setup_theme', 'load_radio_buttons_for_taxonomies_plugin');
function load_radio_buttons_for_taxonomies_plugin() {
	if (!class_exists('Radio_Buttons_for_Taxonomies')) {
		// load Radio Buttons for Taxonomies if not already loaded
		include_once(get_stylesheet_directory() . '/plugins/radio-buttons-for-taxonomies/radio-buttons-for-taxonomies.php');
	}
}


// =================================================
// ::  Sidebars ::
// -------------------------------------------------
/*
register_sidebar(array(
	'name'	=> 'Global',
	'id'	=> 'global',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
));
*/

// =================================================
//	:: IE Fix ::
// -------------------------------------------------	
	add_filter( 'wp_head' , 'add_shiv_for_ie' );
	function add_shiv_for_ie() { ?>
		<!--[if lt IE 9]>			
			<script src="<?php bloginfo('stylesheet_directory'); ?>/js/html5shiv.js"></script>			
			<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/ie.css" />
		<![endif]-->
	<?php
	}

// =================================================
// ::  Post Type: Timeline Event ::
// -------------------------------------------------
add_action('init', 'timeline_event_init');
function timeline_event_init() {
	$labels = array(
			'name' => _x('Timeline Event', 'post type general name'),
			'singular_name' => _x('Timeline Event', 'post type singular name'),
			'add_new' => _x('Add New', 'timeline_event'),
			'add_new_item' => __('Add Event'),
			'edit_item' => __('Edit Event'),
			'new_item' => __('New Event'),
			'all_items' => __('All Events'),
			'view_item' => __('View Event'),
			'search_items' => __('Search Events'),
			'not_found' =>  __('No Events found'),
			'not_found_in_trash' => __('No Events found in Trash'), 
			'parent_item_colon' => '',
			'menu_name' => 'Timeline Event'
	);
	
	$args = array(
			'labels' => $labels,
			'public' => true,
			'show_ui' => true,
			'_builtin' =>  false,
			'show_in_nav_menus' => true,
			'capability_type' => 'post',			
			'hierarchical' => false,
			'exclude_from_search' => false,
			'rewrite' => array("slug" => "timeline-event"),
			'menu_icon' => 'dashicons-calendar',
			'supports' => array('title','editor','thumbnail')
	);
	register_post_type( 'timeline_event' , $args );
}

add_action( 'init', 'wpi_event_tax' );
function wpi_event_tax() {
	register_taxonomy(
		'periods',
		'timeline_event',
		array(
			'label' => __( 'Time Periods' ),
			'rewrite' => array( 'slug' => 'periods' ),
			'hierarchical' => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true
		)
	);
	
	register_taxonomy(
		'timeline_event_tags',
		'timeline_event',
		array(
			'label' => __( 'Tags' ),
			'rewrite' => array( 'slug' => 'timeline-event-tags' ),
			'hierarchical' => false,
			'show_admin_column' => true,
			'show_in_nav_menus' => false
		)
	);
}