	<footer id="footer">
		<div class="footer-column-wrap">
			<?php
			for($i = 1; $i <= 3; $i++):
				$heading = ot_get_option('block_'.$i.'_heading');
				$subheading = ot_get_option('block_'.$i.'_subheading');
				$before = '';
				$after = '';
				if($i != 2 && $url = ot_get_option('block_'.$i.'_url')){
					$before = '<a href="'.esc_url($url).'" class="fc-url">';
					$after = '</a>';
				}
				
				echo $before;
				echo '<div class="footer-column col'.$i.'">';
				echo 	'<img src="'.get_bloginfo('stylesheet_directory').'/images/arrow_footer@2x.png" alt="" width="11" height="5">';
				if($heading)
					echo '<h4 class="fc-heading">'.$heading.'</h4>';
				if($subheading)
					echo '<h5 class="fc-sub-heading">'.$subheading.'</h5>';
				if($i != 2){
					if($graphic = ot_get_option('block_'.$i.'_graphic')){
						$graphic2x = $graphic;
						if(is_numeric($graphic)){
							$graphic = theme_get_image($graphic, 'footer-graphic', false);
							$graphic2x = theme_get_image($graphic2x, 'footer-graphic2x', false);
						}
						echo '<img src="'.$graphic.'" srcset="'.$graphic2x.' 2x" alt="" class="fc-image" width="60" height="57">';
					}
				} else {
					if($facebook_url = ot_get_option('block_'.$i.'_facebook'))
						echo '<a href="'.esc_url($facebook_url).'" target="_blank"><img src="'.get_bloginfo('stylesheet_directory').'/images/icon_facebook.png" srcset="'.get_bloginfo('stylesheet_directory').'/images/icon_facebook@2x.png 2x" alt="" class="fc-image" width="35" height="35"></a>';
					if($twitter_url = ot_get_option('block_'.$i.'_twitter'))
						echo '<a href="'.esc_url($twitter_url).'" target="_blank"><img src="'.get_bloginfo('stylesheet_directory').'/images/icon_twitter.png" srcset="'.get_bloginfo('stylesheet_directory').'/images/icon_twitter@2x.png 2x" alt="" class="fc-image" width="35" height="35"></a>';
					if($instagram_url = ot_get_option('block_'.$i.'_instagram'))
						echo '<a href="'.esc_url($instagram_url).'" target="_blank"><img src="'.get_bloginfo('stylesheet_directory').'/images/icon_instagram.png" srcset="'.get_bloginfo('stylesheet_directory').'/images/icon_instagram@2x.png 2x" alt="" class="fc-image" width="35" height="35"></a>';
				}				
				echo '</div>';
				echo $after;
			endfor;				
			?>			
			<div class="clear"></div>
		</div>
	</footer>
	
	</div><!-- end .wrap -->
	<?php wp_footer(); ?>
	</body>
</html>