<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
?>

<?php get_header() ?>	
	<div id="main">	
		
		<div class="interior">

			<h1 class="page-title">Upcoming Events</h1>
			
			<?php
			$events = tribe_get_events(array('eventDisplay' => 'upcoming'));
			$current_event_id = is_singular('tribe_events') ? get_the_ID() : '';
			//$events = tribe_get_events(array('eventDisplay' => 'upcoming', 'posts_per_page'   => 4));
			if(!empty($events)):
			?>
			
				<div class="event-calendar-container">
					<?php
					$ctr = 1;
					foreach ( $events as $post ) :
						setup_postdata( $post );
					?>
						<a class="event-calendar calendar-anchor <?php if($ctr == 1 && !$current_event_id) { echo 'active-event'; } elseif($current_event_id == $post->ID){ echo 'active-event'; } ?>" href="#event-<?php echo $ctr; ?>">
							<div class="ec-date">								
								<span class="ec-date-month"><?php echo tribe_get_start_date(NULL, 'true', 'M'); ?></span>
								<span class="ec-date-day"><?php echo tribe_get_start_date(NULL, 'true', 'd'); ?></span>
							</div>
							<div class="ec-title">
								<h3><?php the_title(); ?></h3>
								<?php
								$sub_heading = get_post_meta($post->ID, '_sub_heading', true);
								if($sub_heading){
									echo '<h4>'.$sub_heading.'</h4>';
								}
								?>
							</div>
							<div class="clear"></div>
						</a>
					<?php
						$ctr++;
					endforeach;
					wp_reset_postdata();
					?>					
				</div>
				
				<div class="event-details-wrap" id="event-scroll">
					
					<?php
					$ctr = 1;
					foreach ( $events as $post ) :
						setup_postdata( $post );
					?>
						<div id="event-<?php echo $ctr; ?>" class="event-detail-container <?php if($ctr == 1) { echo 'active-event-detail'; } ?>">
							<div class="main-content the-content events-detail">
								<div class="event-detail-top">
									<h2><?php the_title(); ?></h2>
									<?php echo tribe_events_event_schedule_details( get_the_ID(), '<p class="event-time">', '</p>' ); ?>
									<address class="event-info"><?php echo tribe_get_venue(); ?><br>
									<?php echo tribe_get_full_address(); ?>
									</address>
								</div>
								<?php the_content(); ?>
							</div>
							
							<div class="events-image">
                            	<?php 
								// Used for events with no thumbnail
								$placeholder_image = get_post_meta(get_option('page_on_front'), '_placeholder_image', true); 
								
								if(has_post_thumbnail()){
									the_post_thumbnail('event-graphic');
								} elseif($placeholder_image){
									// The field contains a URL, try to get the image ID instead
									if(!is_numeric($placeholder_image)){
										$placeholder_image = get_attachment_id_from_url( $placeholder_image );
									}
									
									// Get the image
									if(is_numeric($placeholder_image)){
										$placeholder_image = theme_get_image($placeholder_image, 'event-graphic');
										echo $placeholder_image;
									} else {
										// Something went wrong, show nothing
									}
								} ?>
							</div>
							<div class="clear"></div>
						</div>
					<?php
						$ctr++;
					endforeach;
					wp_reset_postdata();
					?>	
				
				</div>
			
			<?php else: ?>			
				<div class="main-content the-content">
					<p>There are no upcoming events at this time.</p>
				</div>
			<?php endif; ?>			
			
			<div class="clear"></div>
			
		</div>
		
	</div>    
<?php get_footer() ?>