jQuery(function($){
	
	// Events Listing
	// ------------------------------------------
	$('.calendar-anchor').click(function(event){
		$('.calendar-anchor').removeClass('active-event');
		$(this).addClass('active-event');
		
		var target = $(this).attr('href');
		
		if(!$(target).is(":visible")){
			$('.event-detail-container').hide();
			$(target).stop().fadeIn();
			
			if($('#show-menu').is(":visible")){ // Mobile only
				// Scroll to top of even details
				$('html, body').animate({
					scrollTop: $('#event-scroll').offset().top
				}, 600);
			}
		}
		
		event.preventDefault();
	});
	
	
	// jScrollPane
	// ------------------------------------------
	$('.il-content').jScrollPane({
		verticalDragMinHeight: 60,
		verticalDragMaxHeight: 60,
		verticalGutter: 10,
		autoReinitialise: true
	});


	// Colorbox
	// ------------------------------------------
	var cboxOptions = {
	  maxWidth: '75%',
	  maxHeight: '75%'
	}
	
	$(".ilc-slide a").colorbox(cboxOptions);
	
	// ColorBox resize function
	var resizeTimer;
	function resizeColorBox()
	{
		if (resizeTimer) clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function() {
				if (jQuery('#cboxOverlay').is(':visible')) {
						jQuery.colorbox.resize();
				}
		}, 300);
	}

	// Resize ColorBox when resizing window or changing mobile device orientation
	jQuery(window).resize(resizeColorBox);
	window.addEventListener("orientationchange", resizeColorBox, false);
	
	$('#show-menu').click(function(){
		$(this).toggleClass('menu-shown');
		$('#nav').slideToggle();
		return false;
	});
	
	
	// Logo SVG fallback to PNG for ie
	// ------------------------------------------
	if( $("html").hasClass("ie") ) {
		var src = $('#logo a img').attr("src").replace(".svg", ".png");
		$('#logo a img').attr("src", src);
	};	
	
	
	// Timeline
	// ------------------------------------------
	$( window ).resize(function() {
		if($( window ).width() <= 1006) {
			$('.timeline').cycle('destroy');
			$('.timeline').cycle({
				allowWrap: true
			});
		} else {
			$('.timeline').cycle('reinit');
		}
	});
	
	$( window ).resize();
	
	var totalSlide = $('.timeline-slide').length;

	$('.timeline-slide').each(function(i){
		
		if($(this).hasClass('active-ts') && totalSlide > 10) {			
			var goTo = i;
			if((totalSlide - i ) < 10){
				goTo = totalSlide - 10;
			}			
			$('.timeline').cycle('goto', goTo);
		}
	});		
	
	// Swap to resized logo image for IE due to lack of bicubic image resizing
	var ua = window.navigator.userAgent;
	var msie = ua.indexOf("MSIE ");

	if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {     // If Internet Explorer, return version number
		var src = $('#logo img').attr("src");
		src = src.replace("logo.png", "logo-ie.png");
		$('#logo img.desktop').attr("src", src);
	}
	
});