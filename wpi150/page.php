<?php get_header() ?>	
	<div id="main">	
		
		<div class="interior">
			<?php if(have_posts()): while(have_posts()): the_post(); ?>
			
				<h1 class="page-title"><?php the_title(); ?></h1>				
				
				<div class="main-content the-content">
					<?php the_content(); ?>
					<div class="clear"></div>
				</div>
			
			<?php endwhile; endif; ?>
		</div>
		
	</div>    
<?php get_footer() ?>