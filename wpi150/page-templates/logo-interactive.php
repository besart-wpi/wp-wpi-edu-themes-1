<?php /* Template Name: Logo Interactive */ ?>
<?php get_header() ?>	
	<div id="main">	
		
		<div class="interior interior-logo">
			<?php if(have_posts()): while(have_posts()): the_post(); ?>
			
				<div class="interactive-left the-content">
				
					<div class="il-intro">
						<div class="il-heading">
							<h1><?php the_title(); ?></h1>
						</div>
                        <div class="il-content">
							<?php the_content(); ?>
                            <div class="clear"></div>
                        </div>
						<div class="clear"></div>
					</div>
					
					<?php for($i = 1; $i <= 10; $i++): ?>
					
						<?php
						$icon = get_post_meta($post->ID, '_section_'.$i.'_icon', true); 
						$headline = get_post_meta($post->ID, '_section_'.$i.'_headline', true); 
						$subhead = get_post_meta($post->ID, '_section_'.$i.'_subhead', true); 
						$content = get_post_meta($post->ID, '_section_'.$i.'_content', true); 
						$gallery = get_post_meta($post->ID, '_section_'.$i.'_gallery', true); 
						?>
					
						<div class="il-desc" data-map-index="<?php echo $i; ?>">
							<div class="il-heading">
								<h2><?php echo substr($headline, 0, 26); ?></h2>
								<h3><?php echo substr($subhead, 0, 26); ?></h3>
								<?php
								if($icon){
									if(is_numeric($icon)){
										$icon = theme_get_image($icon, 'event-thumbnail', false);
									}
									echo '<div class="il-icon" style="background-image: url('.$icon.');"></div>';
								}
								?>
								<a href="#" class="il-close"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/x.png" alt="Close"></a>
							</div>
							<div class="il-content">
								<?php echo wpautop(stripslashes($content)); ?>
								<div class="clear"></div>
							</div>
							<div class="il-carousel">
								<?php if($gallery): $gallery = explode(',', $gallery); ?>
									
									<div class="il-carousel-slider cycle-slideshow"
									data-cycle-fx="carousel"
									data-cycle-slides="> div"
									data-cycle-timeout="0"
									data-cycle-prev="#carousel_<?php echo $i; ?>_prev"
									data-cycle-next="#carousel_<?php echo $i; ?>_next"
									>
										<?php foreach($gallery as $gal): ?>
											<div class="ilc-slide">
												<?php
												$gal_thumbnail = $gal;
												$gal_full = $gal;
												if(is_numeric($gal)){
													$gal_full = theme_get_image($gal, 'full', false);
													$gal_thumbnail = theme_get_image($gal, 'event-thumbnail', false);
												}
												?>
												<a rel="group-<?php echo $i; ?>" href="<?php echo $gal_full; ?>"><img src="<?php echo $gal_thumbnail; ?>" alt=""></a>
											</div>
										<?php endforeach; ?>
									</div>
									
									<a id="carousel_<?php echo $i; ?>_prev" class="ilc-prev" href="#">Prev</a>
									<a id="carousel_<?php echo $i; ?>_next" class="ilc-next" href="#">Next</a>
								
								<?php endif; ?>
							</div>
						</div>
					
					<?php endfor; ?>					
					
				</div>
				
				<div class="interactive-logo-wrap">
				
					<div class="interactive-logo">
					
						<img class="ie-placeholder" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo_interactive.png">
						
						<img id="wpi-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo_interactive.png" class="interactive-map" usemap="#map" alt="">
						
						<map name="map" id="map">
							<area shape="poly" href="#" data-map-index="1" coords="0,0,52,0,89,72,115,23,145,81,89,181,0,15" />
							<area shape="poly" href="#" data-map-index="2" coords="202,183,105,0,164,0,226,131,221,147" />
							<area shape="poly" href="#" data-map-index="3" coords="200,78,228,133,295,0,242,0" />
							<area shape="poly" href="#" data-map-index="4" coords="317,179,355,179,350,49,355,42,356,22,353,7,345,0,324,0,318,8,315,21,320,33,316,35,315,44,319,47" />
							<area shape="poly" href="#" data-map-index="5" coords="355,44,386,43,393,43,401,48,402,53,402,60,399,67,393,70,351,71,352,114,404,113,424,104,441,86,448,61,445,34,430,14,411,3,401,0,353,0" />
							<area shape="poly" href="#" data-map-index="6" coords="471,178,521,178,520,52,525,52,525,43,522,43,522,27,516,27,508,0,482,0,477,25,471,28,470,42,466,46,466,53,469,52" />
							<area shape="poly" href="#" data-map-index="7" coords="60,197,81,197,75,231,85,249,84,273,89,274,88,283,95,308,100,309,100,394,104,394,102,462,34,462,34,396,37,396,37,309,43,308,50,281,51,246,61,231" />
							<area shape="poly" href="#" data-map-index="8" coords="140,463,182,463,182,414,141,414,142,347,182,348,181,274,207,272,244,271,265,273,265,277,265,277,308,277,308,219,303,219,287,210,271,218,264,219,264,224,229,221,183,224,182,219,178,219,161,210,144,219,139,219" />
							<area shape="poly" href="#" data-map-index="9" coords="182,461,181,419,247,418,260,410,272,390,272,371,265,356,252,346,241,343,182,344,182,300,254,302,287,316,302,335,314,360,316,389,309,414,296,436,282,448,259,459" />
							<area shape="poly" href="#" data-map-index="10" coords="422,211,461,210,461,217,542,259,542,270,532,268,532,284,535,286,535,289,542,290,542,301,535,303,535,392,529,407,514,435,495,450,471,464,413,464,390,451,372,436,357,415,349,394,347,358,348,304,341,299,342,286,347,285,347,271,340,272,336,261,421,216" />
						</map>

						
						
					</div>
				</div>
				
				<div class="clear"></div>

				<script>
					jQuery(function($){
						if( !$("html").hasClass("ie") ) {
							$('#wpi-logo').mapster({ 
								fillOpacity: 1,
								// mapKey: 'data-map-index',
								// listKey: 'data-map-index',
								// boundList: $('.interactive-left .il-desc'),
								// listSelectedClass: 'active-il-desc',
								singleSelect: true,
								 render_highlight: {
									fillOpacity: 1,
									stroke: false,
									altImage: '<?php bloginfo('stylesheet_directory'); ?>/images/logo_interactive_red.png'
								 },
								render_select: {
									stroke: false,
									altImage: '<?php bloginfo('stylesheet_directory'); ?>/images/logo_interactive_red.png'
								}
							});
						} else {
							$('#wpi-logo').attr('src', '<?php bloginfo('stylesheet_directory'); ?>/images/logo_interactive_blank.png');
						}
						
						$('#map area').on('click', function(){							
							$(this).toggleClass('active-map');
							$('#map area').not(this).removeClass('active-map');
							
							mapIndex = $(this).data('map-index');
							var descTarget = $('.interactive-left div[data-map-index="'+mapIndex+'"]');
							
							if($(this).hasClass('active-map')){
								$('.interactive-left .il-desc').hide();
								$('.interactive-left .il-intro').hide();
								$(descTarget).fadeIn();
								
								if( $("html").hasClass("ie") ) {
									$('#wpi-logo').attr('src', '<?php bloginfo('stylesheet_directory'); ?>/images/piece'+mapIndex+'.png');
								}
							} else {
								$('.interactive-left .il-intro').fadeIn();
								$(descTarget).hide();
								var descTarget = $('.interactive-left .il-intro');
								
								if( $("html").hasClass("ie") ) {
									$('#wpi-logo').attr('src', '<?php bloginfo('stylesheet_directory'); ?>/images/logo_interactive_blank.png');
								}
							}
							
							// Make sure the interior-logo wrap is tall enough to display the entire description for the active logo part
							$('.interior-logo').css('min-height', '300px');
							if( $('.interior-logo').height() < $(descTarget).outerHeight() ){
								$('.interior-logo').css('min-height', $(descTarget).outerHeight() );
							}
							
							return false;
						});
						
						$('.il-close').on('click', function(){
							$('area').removeClass('active-map').mapster('deselect');
							$('.interactive-left .il-desc').hide();
							$('.interactive-left .il-intro').fadeIn();
							if( $("html").hasClass("ie") ) {
								$('#wpi-logo').attr('src', '<?php bloginfo('stylesheet_directory'); ?>/images/logo_interactive_blank.png');
							}
							return false;
						});
						
					}); // END document.ready
				</script>
			
			<?php endwhile; endif; ?>
		</div>
		
	</div>    
<?php get_footer() ?>