<div class="single-timeline-desc the-content">
	<div class="il-heading">
		<h1><?php the_title(); ?></h1>
		<?php
		$story_date = get_post_meta($post->ID, '_story_date', true);
		$start_date = get_post_meta($post->ID, '_start_date', true);
		$end_date = get_post_meta($post->ID, '_end_date', true);
		$event_image = get_post_meta($post->ID, '_event_image', true);
		$youtube_url = get_post_meta($post->ID, '_youtube_url', true);
		$soundcloud_url = get_post_meta($post->ID, '_soundcloud_url', true);
		
		$st_date  = '';
		if($story_date){
			$st_date = substr($story_date, 0, 22);
		} elseif($start_date) {
			$start_timestamp = strtotime($start_date);
			if(!$end_date){
				$st_date = date('m/d/Y', $start_timestamp);
			} else {
				$end_timestamp = strtotime($end_date);
				
				$start_d = date('d', $start_timestamp);
				$start_m = date('m', $start_timestamp);
				$start_y = date('Y', $start_timestamp);
				
				$end_d = date('d', $end_timestamp);
				$end_m = date('m', $end_timestamp);
				$end_y = date('Y', $end_timestamp);
				
				$st_date = date('m/d/Y', $start_timestamp).'&#8211;'.date('m/d/Y', $end_timestamp);
				
				if($start_y == $end_y){
					$st_date = date('F j', $start_timestamp).'&#8211;'.date('F j', $end_timestamp).', '.$start_y;
					
					if($start_m == $end_m){
						if($start_d == $end_d){
							$st_date = date('F j', $start_timestamp).' '.$start_y;
						} else {
							$st_date = date('F j', $start_timestamp).'&#8211;'.date('j', $end_timestamp).', '.$start_y;
						}
					}									
					
					if(($start_m != $end_m) && $start_d == '01' && date('t', $end_timestamp) == $end_d ){
						$st_date = date('F', $start_timestamp).'&#8211;'.date('F', $end_timestamp).' '.$start_y;
					}
					
					if($start_m == '01' && $start_date == '01' && $end_m == '12' && $end_d == '31'){
						$st_date = $start_y;
					}									
				}
			
			}
		}
		if($st_date){
			echo '<p class="st-date">'.$st_date.'</p>';
		}
		?>
	</div>
	<div class="st-content">
		<?php the_content(); ?>
	</div>
</div>

<?php
if($event_image){
	if(is_numeric($event_image)){
		$event_image = theme_get_image($event_image, 'event-size', false);
	}
}

$embed_code = '';
if($soundcloud_url){
	$embed_code = wp_oembed_get($soundcloud_url, array('width'=>'683', 'height'=>'384'));
}
if($youtube_url){
	$embed_code = wp_oembed_get($youtube_url, array('width'=>'683', 'height'=>'384'));
}
?>

<div class="single-timeline-right<?php echo $embed_code ? ' has-embed' : ' no-embed'; ?>" <?php echo $event_image ? 'style="background-image: url('.$event_image.')"' : ''; ?>>
	<?php
	
	echo '<img src="'.$event_image.'" alt="" class="event-image">';
	echo $embed_code;
	?>
</div>

<div class="clear"></div>