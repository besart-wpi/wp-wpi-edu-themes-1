jQuery(function($){
	var $portfolio 	= $('#portfolio-wrap')
	var $projects 	= $('.portfolio-thumb-wrap')
	var $cats 		= $('#project-categories a')
	var $content 	= $('article', $projects)

	/**
	 * Equalize heights
	 */
	 
	var resizeThumbs = function() {
		var tallest = 0;
		$('article', $thumbs).height('auto');
		$('article', $thumbs).each(function(){
			tallest = Math.max($(this).height(), tallest)
		}).height(tallest);
	}
	 
	$thumbs = $('.portfolio-thumb-wrap'); 
	if($thumbs.length){
		resizeThumbs();
		$(window).resize(function(){
			resizeThumbs();
		});
	}

	$cats.click(function(){
		var $this = $(this);

		$projects.show()
			.not($('.' + $this.attr('href').replace('#', ''))).hide()
		// $portfolio.masonry('reloadItems')
		$cats.removeClass('selected')
		$this.addClass('selected')

		return false;
	})
})

jQuery('window').ready(function(){
	var $ = jQuery

	setTimeout(function(){
		// var $portfolio 	= $('#portfolio-wrap')
		// $portfolio.masonry({
			// itemSelector: '.portfolio-thumb-wrap'
		// })
	}, 1)
})