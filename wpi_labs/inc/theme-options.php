<?php
/**
 * Initialize the custom Theme Options.
 */
add_action( 'admin_init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 *
 * @return    void
 * @since     2.0
 */
function custom_theme_options() {
  
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( ot_settings_id(), array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
   
	$custom_settings = array( 
	
		'sections'        => array( 
			array(
				'id'          => 'section_color_options',
				'title'       => __( 'Color Options', 'wpi-mpi' )
			),
			array(
				'id'          => 'section_header_options',
				'title'       => __( 'Header Options', 'wpi-mpi' )
			),
			array(
				'id'          => 'section_footer_options',
				'title'       => __( 'Footer Options', 'wpi-mpi' )
			),
      array(
        'id'          => 'social_options',
        'title'       => __( 'Social Media Options', 'wpi-mpi' )
      )
		),
		
		'settings'        => array( 
			// Color Options
			array(
				'id'          => 'color_heading',
				'label'       => __( 'Heading Color', 'wpi-mpi' ),
				'type'        => 'colorpicker',
				'section'     => 'section_color_options',
			),
			array(
				'id'          => 'color_titles',
				'label'       => __( 'Interior Page Title Color', 'wpi-mpi' ),
				'type'        => 'colorpicker',
				'section'     => 'section_color_options',
			),
			array(
				'id'          => 'color_links',
				'label'       => __( 'Link Colors', 'wpi-mpi' ),
				'type'        => 'colorpicker',
				'section'     => 'section_color_options',
			),
			array(
				'id'          => 'color_menu_bg',
				'label'       => __( 'Menu Background Color', 'wpi-mpi' ),
				'type'        => 'colorpicker',
				'section'     => 'section_color_options',
			),
			array(
				'id'          => 'color_menu_active_bg',
				'label'       => __( 'Menu Background Color (Active)', 'wpi-mpi' ),
				'type'        => 'colorpicker',
				'section'     => 'section_color_options',
			),
			array(
				'id'          => 'color_slide_bg',
				'label'       => __( 'Slide Title Background', 'wpi-mpi' ),
				'desc'		  => __( 'This color will have a slight transparency applied.'),
				'type'        => 'colorpicker',
				'section'     => 'section_color_options',
			),
			array(
				'id'          => 'color_border_top',
				'label'       => __( 'Content Top Border Color', 'wpi-mpi' ),
				'type'        => 'colorpicker',
				'section'     => 'section_color_options',
			),
			array(
				'id'          => 'color_border',
				'label'       => __( 'Content Border Highlight Color', 'wpi-mpi' ),
				'type'        => 'colorpicker',
				'section'     => 'section_color_options',
			),
			array(
				'id'          => 'color_quote',
				'label'       => __( 'Testimonial Background Color', 'wpi-mpi' ),
				'type'        => 'colorpicker',
				'section'     => 'section_color_options',
			),

			// Meeting Options
			array(
				'id'          => 'header_sub_logos',
				'label'       => __( 'Sub Logos', 'theme-text-domain' ),
				'std'         => '',
				'type'        => 'list-item',
				'section'     => 'section_header_options',
				'operator'    => 'and',
				'settings'    => array( 
					array(
						'id'          => 'header_sub_logos_icon',	//Sorry, don't know what else to call it :P
						'label'       => __( 'Logo', 'theme-text-domain' ),
						'type'        => 'upload',
						'operator'    => 'and'
					),
					array(
						'id'          => 'header_sub_logos_link',	//Sorry, don't know what else to call it :P
						'label'       => __( 'URL', 'theme-text-domain' ),
						'type'        => 'text',
						'operator'    => 'and'
					)				  
				)
			),

			// Header Options
			array(
				'id'          => 'favicon',
				'label'       => __( 'Favicon', 'wpi-mpi' ),
				'type'        => 'upload',
				'section'     => 'section_header_options',
			),
			array(
				'id'          => 'header_logo',
				'label'       => __( 'Logo', 'wpi-mpi' ),
				'type'        => 'upload',
				'section'     => 'section_header_options',
			),
			array(
				'id'          => 'header_logo_offset',
				'label'       => __( 'Logo Vertical Offset', 'wpi-mpi' ),
				'type'        => 'text',
				'desc'		  => 'Vertical offset in pixels',
				'section'     => 'section_header_options',
			),
			array(
				'id'          => 'header_sub_logos',
				'label'       => __( 'Sub Logos', 'theme-text-domain' ),
				'std'         => '',
				'type'        => 'list-item',
				'section'     => 'section_header_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and',
				'settings'    => array( 
				  array(
				    'id'          => 'header_sub_logos_icon',	//Sorry, don't know what else to call it :P
				    'label'       => __( 'Logo', 'theme-text-domain' ),
				    'desc'        => '',
				    'std'         => '',
				    'type'        => 'upload',
				    'post_type'   => '',
				    'taxonomy'    => '',
				    'min_max_step'=> '',
				    'class'       => '',
				    'condition'   => '',
				    'operator'    => 'and'
				  ),
				  array(
				    'id'          => 'header_sub_logos_link',	//Sorry, don't know what else to call it :P
				    'label'       => __( 'URL', 'theme-text-domain' ),
				    'desc'        => '',
				    'std'         => '',
				    'type'        => 'text',
				    'post_type'   => '',
				    'taxonomy'    => '',
				    'min_max_step'=> '',
				    'class'       => '',
				    'condition'   => '',
				    'operator'    => 'and'
				  )				  
				)
			),

			// Footer Options
			array(
				'id'     	=> 'footer_copyright',
				'label'  	=> __( 'Footer Copyright', 'wpi-mpi' ),
				'type'   	=> 'textarea',
				'section'	=> 'section_footer_options',
			),
			array(
				'id'     	=> 'footer_contact',
				'label'  	=> __( 'Footer Contact', 'wpi-mpi' ),
				'type'   	=> 'textarea',
				'section'	=> 'section_footer_options',
			),


      /* ---- Social Media Options ------------------------------ */
      array(
        'id'          => 'twitter_url',
        'label'       => __( 'Twitter URL', 'wpi-mpi' ),
        'type'        => 'text',
        'section'     => 'social_options',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'facebook_url',
        'label'       => __( 'Facebook URL', 'wpi-mpi' ),
        'type'        => 'text',
        'section'     => 'social_options',
        'operator'    => 'and'
      ),      
      array(
        'id'          => 'linkedin_url',
        'label'       => __( 'LinkedIn URL', 'wpi-mpi' ),
        'type'        => 'text',
        'section'     => 'social_options',
        'operator'    => 'and'
      )
		)
	);
  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( ot_settings_id() . '_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( ot_settings_id(), $custom_settings ); 
  }
  
}
