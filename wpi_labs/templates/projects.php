<?php // Template Name: Projects Landing
get_header();

// =================================================
// :: Get the list of project categories ::
// ------------------------------------------------
$cats = get_terms('project-categories');
$catHTML = '';
foreach($cats as $cat)
	$catHTML .= '<li><a href="#cat-'.$cat->slug.'">'.$cat->name.'</a>';

$posts = get_posts(array(
	'post_type'		=> 'project_portfolio',
	'numberposts'=> -1,
	'orderby' => 'menu_order',
	'order' => 'ASC'
));

// =================================================
// :: Gets the .hero background ::
// -------------------------------------------------
if($featuredImage = get_post_thumbnail_id()){
	$featuredImage = get_post($featuredImage)->guid;
}

?>
	<div class="hero <?php if($featuredImage) echo 'imaged" style="background: url('. $featuredImage . '); background-position: center; background-repeat: no-repeat; background-size: cover; -ms-behavior: url('.get_bloginfo('template_url').'/js/backgroundsize.min.htc);'; ?>">
		<header>
			<h1>
				<?php if($featuredImage) echo '<span>' ?>
					<?php the_title() ?>
				<?php if($featuredImage) echo '</span>' ?>
			</h1>
		</header>
	</div>

	<section id="the-content">
		<div id="content">
			<article class="main c12" style="padding-right: 0">
				<div>
					<?php
						global $post;
						echo apply_filters('the_content', $post->post_content);
					?>
					<div class="clear"></div>
					<br>
				</div>
				<div class="project-thumbnails-wrapper">
					<?php echo do_shortcode("[projects_listing hide_title]") ?>
				</div>

			</article>
			<div class="clear"></div>
		</div>
	</section>

	<?php // =================================================
	// :: Scripts ::
	// -------------------------------------------------
	// js('masonry.js');
	js('portfolio.js');
	get_footer();
?>