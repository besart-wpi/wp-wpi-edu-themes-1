    	</div><!-- / #main -->
	</div><!-- / #inner-wrap -->
	
	<div id="footer">

			<div class="widgets">
				<div class="wide">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer: Wide') ) : ?> <?php endif; ?>
				</div><!-- / .column -->
			</div>
		 
 		<div class="wrap">

 			<?php if ( is_active_sidebar( 'footer_1' ) ||  is_active_sidebar( 'footer_2'  )  ||  is_active_sidebar( 'footer_3'  ) ) : ?>
  				<div class="widgets">
 			<?php endif; ?>

				<?php if ( is_active_sidebar( 'footer_1'  ) ) { ?>
 	 				<div class="column">
						<?php dynamic_sidebar('footer_1'); ?> 
					</div><!-- / .column -->
				<?php } ?>

				<?php if ( is_active_sidebar( 'footer_2'  ) ) { ?>
 	 				<div class="column">
						<?php dynamic_sidebar('footer_2'); ?> 
					</div><!-- / .column -->
				<?php } ?>

				<?php if ( is_active_sidebar( 'footer_3'  ) ) { ?>
 	 				<div class="column last">
						<?php dynamic_sidebar('footer_3'); ?> 
					</div><!-- / .column -->
				<?php } ?>

			<?php if ( is_active_sidebar( 'footer_1'  ) ||  is_active_sidebar( 'footer_2'  )  ||  is_active_sidebar( 'footer_3'  ) ) : ?>
  				<div class="cleaner">&nbsp;</div>
				</div>
 			<?php endif; ?>
  		  
			<div class="inner">
				<div class="left">
					<div class="menu">
						<?php if (has_nav_menu( 'secondary' )) {
							wp_nav_menu( array(
								'container' => 'menu',
								'container_class' => '',
								'menu_id' => 'footmenu',
								'sort_column' => 'menu_order',
								'depth' => 1,
								'theme_location' => 'secondary'
							) );
						} ?>
					</div>

					<div class="copyright">
					 <p>Copyright © 1995 - <?php echo date('Y'); ?> Worcester Polytechnic Institute | 100 Institute Road, Worcester, MA 01609-2280 | +1-508-831-5000</p>
					</div>
				</div>

				<div class="right">
					<div class="widgets">
						<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer: Right of Copyright') ) : ?> <?php endif; ?>
						<div class="cleaner">&nbsp;</div>
					</div>
				</div>

				<div class="cleaner">&nbsp;</div>
			</div>
		</div>
	</div><!-- / #footer -->
</div><!-- / #wrapper -->


<script type="text/javascript">
jQuery(document).ready(function($) {
	
	<?php if (is_home() && option::get('featured_posts_show') == 'on') { ?>
 	jQuery("#slider").flexslider({
 		controlNav: true,
		directionNav:false,
		animationLoop: true,
   		animation: "<?php if (option::get('slideshow_effect') == 'Slide') { ?>slide<?php } else { ?>fade<?php } ?>",
		useCSS: true,
		smoothHeight: true,
		touch: false,
		slideshow: <?php if (option::get('slideshow_auto') == 'on') { echo "true"; } else { echo "false"; } ?>,
		<?php if (option::get('slideshow_auto') == 'on') { ?>slideshowSpeed:<?php echo option::get('slideshow_speed'); ?>,<?php } ?>
		pauseOnAction: true,
		animationSpeed: 600,
		start: wpz_sliderTextAnimate,
		before: wpz_sliderTextAnimate
 	});
	jQuery('#slider .slides h3').css({'margin-top':0,'opacity':0});
	jQuery('#slider .slides .excerpt').css({'left':'-680px','opacity':0});
	<?php } ?>
	
	<?php wp_reset_query(); if (is_singular('portfolio')) { ?>
 	jQuery("#portfolio-slider").flexslider({
 		controlNav: false,
		directionNav:true,
		animationLoop: true,
		slideshow: false,
  		animation: "slide",
		useCSS: false,
		touch: false,
		smoothHeight: true,
		slideshow: false,
 		animationSpeed: 300
 	});	
	<?php } ?>
 
});
</script>
 
<?php wp_footer(); ?>
</body>
</html>