<div id="menu" class="fix">
						<div class="divide"><img src="http://wp.wpi.edu/capetown-2013/wp-content/themes/venture-capetown/images/orangeLine.png"></div>	
 						<div class="menuDiv">
						<ul class="dropdown sf-js-enabled" id="mainmenu"><li class="topNav menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-19501" id="menu-item-19501"><a href="http://wp.wpi.edu/capetown-2013/">Home</a></li>
<li class="topNav menu-item menu-item-type-custom menu-item-object-custom menu-item-18539" id="menu-item-18539"><a href="http://wp.wpi.edu/capetown-2013/homepage/about/">About CTPC</a></li>
<li class="topNav menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor menu-item-18540" id="menu-item-18540"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/" class="sf-with-ul">Projects<span class="sf-sub-indicator"> »</span></a>
<ul class="sub-menu" style="display: none; visibility: hidden;">
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18547" id="menu-item-18547"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/2013-2/">2013</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18541" id="menu-item-18541"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2012/" class="sf-with-ul">2012<span class="sf-sub-indicator"> »</span></a>
	<ul class="sub-menu" style="display: none; visibility: hidden;">
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18542" id="menu-item-18542"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2012/mgv/">Capacity Building of a Community Based Organisation in Maitland Garden Village</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18543" id="menu-item-18543"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2012/pathway/">Envisioning a Black River Pathway: Creating a Heritage Destination through Social Development</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18544" id="menu-item-18544"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2012/mtshini-wam/">Supporting Reblocking and Community Development in Mtshini Wam</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18545" id="menu-item-18545"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2012/langrug/">Laying the Foundation for a Resilient Partnership: Innovative Upgrading in the Informal Settlement of Langrug</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18546" id="menu-item-18546"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2012/rooftop/">Rooftop Gardens for Sustainable Livelihoods in Cape Town</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-18784" id="menu-item-18784"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2011/" class="sf-with-ul">2011<span class="sf-sub-indicator"> »</span></a>
	<ul class="sub-menu" style="display: none; visibility: hidden;">
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18788" id="menu-item-18788"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2011/sustainable-livelihoods-through-beekeeping/">Sustainable Livelihoods Through Beekeeping</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18790" id="menu-item-18790"><a href="http://wp.wpi.edu/capetown-2013/projects/p2011/river/">Black River Corridor: Visions for Restoration and Recreational Use</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-11668 current_page_item menu-item-18791" id="menu-item-18791"><a href="http://wp.wpi.edu/capetown-2013/projects/p2011/mgv/">Supporting Asset Based Community Development in Maitland Garden Village</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18792" id="menu-item-18792"><a href="http://wp.wpi.edu/capetown-2013/projects/p2011/omev/">Envisioning the Future Development of Oude Molen Eco Village</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18793" id="menu-item-18793"><a href="http://wp.wpi.edu/capetown-2013/projects/p2011/langrug-greywater/">Addressing Greywater Management Issues in Langrug Using a Sustainable Reiterative Process</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18787" id="menu-item-18787"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2011/wash/">WaSHUp: Innovating Water Sanitation and Hygiene Upgrading in Langrug</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18548" id="menu-item-18548"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2010/" class="sf-with-ul">2010<span class="sf-sub-indicator"> »</span></a>
	<ul class="sub-menu" style="display: none; visibility: hidden;">
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18549" id="menu-item-18549"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2010/profiling-community-assets/">Profiling Community Assets</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18550" id="menu-item-18550"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2010/collaborative-construction/">Collaborative Construction</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18551" id="menu-item-18551"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2010/stormwater-management/">Stormwater Management</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18552" id="menu-item-18552"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2010/spaza/">Strengthening Spaza Shops</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18553" id="menu-item-18553"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2010/ecd/">Supporting Early Childhood Development</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18554" id="menu-item-18554"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2010/culture/">Initiation Site Development</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18555" id="menu-item-18555"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2010/co-researchers/">Co-Researchers</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18556" id="menu-item-18556"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2009/" class="sf-with-ul">2009<span class="sf-sub-indicator"> »</span></a>
	<ul class="sub-menu" style="display: none; visibility: hidden;">
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18557" id="menu-item-18557"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2009/mapping-planning/">Mapping and Planning</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18558" id="menu-item-18558"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2009/water-sanitation/">Water &amp; Sanitation</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18559" id="menu-item-18559"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2009/economy/">Economy</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18560" id="menu-item-18560"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2009/communications/">Communications</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18561" id="menu-item-18561"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2009/energy/">Energy</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18562" id="menu-item-18562"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2009/gardens/">Gardens</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18563" id="menu-item-18563"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2009/buildings/">Buildings</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18564" id="menu-item-18564"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2008/" class="sf-with-ul">2008<span class="sf-sub-indicator"> »</span></a>
	<ul class="sub-menu" style="display: none; visibility: hidden;">
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18565" id="menu-item-18565"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2008/buildings/">Buildings</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18566" id="menu-item-18566"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2008/water-sanitation/">Water &amp; Sanitation</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18567" id="menu-item-18567"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2008/communications/">Communications</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18568" id="menu-item-18568"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2008/energy/">Energy</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18569" id="menu-item-18569"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2008/mapping-planning/">Mapping &amp; Planning</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18570" id="menu-item-18570"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2008/economy/">Economy</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18571" id="menu-item-18571"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2007/" class="sf-with-ul">2007<span class="sf-sub-indicator"> »</span></a>
	<ul class="sub-menu" style="display: none; visibility: hidden;">
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18572" id="menu-item-18572"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2007/laundry-centre/">Laundry Centre</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18573" id="menu-item-18573"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2007/sustainable-housing/">Sustainable Housing</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18574" id="menu-item-18574"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2007/flood-risk-management/">Flood Risk Management</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18575" id="menu-item-18575"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2007/informal-trading/">Informal Trading</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18576" id="menu-item-18576"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2007/transportation/">Transportation</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18577" id="menu-item-18577"><a href="http://wp.wpi.edu/capetown-2013/homepage/projects/p2007/recreational-facilities/">Recreational Facilities</a></li>
	</ul>
</li>
</ul>
</li>
<li class="topNav menu-item menu-item-type-custom menu-item-object-custom menu-item-18578" id="menu-item-18578"><a href="http://wp.wpi.edu/capetown-2013/homepage/student-life/" class="sf-with-ul">For Students<span class="sf-sub-indicator"> »</span></a>
<ul class="sub-menu" style="display: none; visibility: hidden;">
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18579" id="menu-item-18579"><a href="http://wp.wpi.edu/capetown-2013/homepage/student-life/new-approach-to-project-website-development/" class="sf-with-ul">Shared Action Learning Project Development<span class="sf-sub-indicator"> »</span></a>
	<ul class="sub-menu" style="display: none; visibility: hidden;">
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18581" id="menu-item-18581"><a href="http://wp.wpi.edu/capetown-2013/homepage/student-life/new-approach-to-project-website-development/how-will-we-build-our-project-website/">How Will We Build Our Project Website?</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18582" id="menu-item-18582"><a href="http://wp.wpi.edu/capetown-2013/homepage/student-life/new-approach-to-project-website-development/resources-for-website-development/">Resources for Project &amp; Website Development</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18580" id="menu-item-18580"><a href="http://wp.wpi.edu/capetown-2013/homepage/student-life/new-approach-to-project-website-development/introduction-what-is-a-shared-action-learning-approach-to-iqps/">What is Shared Action Learning?</a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18583" id="menu-item-18583"><a href="http://wp.wpi.edu/capetown-2013/homepage/student-life/dining-in-sa/">Dining in Cape Town</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18584" id="menu-item-18584"><a href="http://wp.wpi.edu/capetown-2013/homepage/student-life/student-page/" class="sf-with-ul">Student Page<span class="sf-sub-indicator"> »</span></a>
	<ul class="sub-menu" style="display: none; visibility: hidden;">
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18585" id="menu-item-18585"><a href="http://wp.wpi.edu/capetown-2013/homepage/student-life/student-page/student-forum/">Student Forum</a></li>
		<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18586" id="menu-item-18586"><a href="http://wp.wpi.edu/capetown-2013/homepage/student-life/student-page/contact-list/">Contact List</a></li>
	</ul>
</li>
</ul>
</li>
<li class="topNav menu-item menu-item-type-custom menu-item-object-custom menu-item-18587" id="menu-item-18587"><a href="http://wp.wpi.edu/capetown-2013/homepage/support-ctpc/">Acknowledgements</a></li>
<li class="topNav menu-item menu-item-type-custom menu-item-object-custom menu-item-18588" id="menu-item-18588"><a href="http://wp.wpi.edu/capetown-2013/homepage/contact-us/">Contact Us</a></li>
</ul>						</div>
						<div class="menuDivide"><img src="http://wp.wpi.edu/capetown-2013/wp-content/themes/venture-capetown/images/blueLine.png"></div>
  						
					</div>