
/**
 * Overrides the WPZOOM function of the same name.
 * Fixes a bug that prevents the slideshow from having only one slide.
 */
function wpz_sliderTextAnimate(slider) {

    var $slideLength = jQuery('#slider .slides li').length;

    if ($slideLength > 1) {
        
        // for multiple slides, animate all headers and excerpts
        var $currentSlide = jQuery('#slider .slides li').not('.clone').eq(slider.animatingTo),
        $otherSlides = jQuery('#slider .slides li').not($currentSlide);

        $otherSlides.find('h3').animate({'margin-top': 0, 'opacity': 0}, 800);
        $otherSlides.find('.excerpt').animate({'left': '-680px', 'opacity': 0}, 800);
        $currentSlide.find('h3').animate({'margin-top': '130px', 'opacity': 1}, 800);
        $currentSlide.find('.excerpt').animate({'left': 0, 'opacity': 1}, 800);

    } else if ($slideLength == 1) {

        // for a single slide, just show the header and excerpt, but don't animate it off screen
        var $singleSlide = jQuery('#slider .slides li');
        $singleSlide.find('h3').animate({'margin-top': '130px', 'opacity': 1}, 800);
        $singleSlide.find('.excerpt').animate({'left': 0, 'opacity': 1}, 800);

    }

}

/**
 * Pins the horizontal navigation to the top when scrolling down the page.
 */
jQuery(document).ready(function($) { 
  
    $(window).scroll(function() {
      if ($(this).scrollTop() > 145) { // 158
           $('#menu').addClass('fix');
      } else {
           $('#menu').removeClass('fix');
      }
    });

    $('.transImg').height($('.sidebar').height());
 
});
