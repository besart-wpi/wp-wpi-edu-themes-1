<?php

/* Custom functions for WPI Venture Capetown child theme */

/* 	Register Custom Menu  */
register_nav_menu('submenu', 'Sub Menu');

/**
 * Overrides some of the Venture javascript so the homepage slideshow will work when there's only one slide.
 * 
 */
function wpi_override_venture_scripts() {

    // override some custom functions
    wp_register_script(
        'wpi-custom',
        get_stylesheet_directory_uri() . '/js/wpi-custom.js',
        array('wpzoom-custom'), // depend on wpzoom-custom so this loads after it to override its functions
        '1.0',
        true
    );
    wp_enqueue_script('wpi-custom');
    
}
add_action('wp_print_scripts', 'wpi_override_venture_scripts', 100);


/**
 * Adds a header image to the interior pages. 
 */
register_sidebar( array(
    'name' => __( 'Interior Header Image', 'venture' ),
    'id' => 'interior-image',
    'description' => __( 'Adds a header image to the interior pages.', 'venture' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
) );


/**
 * Adds a widget that displays the navigation based on the page structure.
 */
function wpi_surrounding_nav_widget($args, $instance=null) {
    echo $args['before_widget'];
    wp_page_menu('show_home=0');
    echo '<script type="text/javascript">';
    echo 'jQuery(document).ready(function($) { ';
    
    // first, hide all pages in the nav
    echo 'jQuery(".wpi_surrounding_nav_widget .page_item").hide();' . "\n";

    // now, show only the items we want to see
    
    // show all ancestors above the current page
    echo 'jQuery(".wpi_surrounding_nav_widget .current_page_ancestor").show();' . "\n";
    // show the current page
    echo 'jQuery(".wpi_surrounding_nav_widget .current_page_item").show();' . "\n";
    // show the siblings of the current page's ancestors
    echo 'jQuery(".wpi_surrounding_nav_widget .current_page_ancestor").siblings().show();' . "\n";
    // show the current page's siblings
    echo 'jQuery(".wpi_surrounding_nav_widget .current_page_item").siblings().show();' . "\n";
    // show the current page's direct children
    echo 'jQuery(".wpi_surrounding_nav_widget .current_page_item > ul.children > li.page_item").show();' . "\n";
    
    // echo 'jQuery(".wpi_surrounding_nav_widget .current_page_item > ul ul.children > li.page_item").show();' . "\n";
    
    echo ' });</script>';
    echo $args['after_widget'];
}
function register_wpi_surrounding_nav_widget() {
    register_sidebar_widget(array('WPI Surrounding Nav Widget', 'widgets'), 'wpi_surrounding_nav_widget');
}
add_action('widgets_init', 'register_wpi_surrounding_nav_widget');


/**
 * Clean up the admin interface a little by removing unecessary custom post types from the menu
 */
function wpi_hide_wpzoom_menu_items() {
    remove_menu_page('edit.php?post_type=testimonial');
    remove_menu_page('edit.php?post_type=client');
    remove_menu_page('edit.php?post_type=portfolio');
}
add_action('admin_menu', 'wpi_hide_wpzoom_menu_items');


/**
 * Remove the WPZOOM News box from the dashboard
 */
function wpi_remove_wpzoom_dashboard_news() {
    remove_meta_box('dashboard_wpzoom', 'dashboard', 'normal');
}
add_action('wp_dashboard_setup', 'wpi_remove_wpzoom_dashboard_news' );


/**
 * Remove the Top Button meta box on Page edit screens
 * See: http://wordpress.stackexchange.com/questions/59607/removing-custom-meta-box-added-in-parent-theme
 */
function wpi_remove_top_buttom_meta_box() {
    remove_meta_box('wpzoom_top_button' , 'page' , 'side');
}
add_action('add_meta_boxes_page' , 'wpi_remove_top_buttom_meta_box');

/**
 * Register widgetized area and update sidebar with default widgets
 */
function magazino_widgets_init() {
    register_sidebar(array(
        'name' => 'Blog: Sidebar',
        'id' => 'blog-sidebar',
        'description' => 'Sidebar that appears only on pages using the Blog template.',
        'before_widget' => '<div class="widget %2$s" id="%1$s">',
        'after_widget' => "</div>",
        'before_title' => '<h3 class="title"><span>',
        'after_title' => '</span></h3>',
    ));
}
add_action( 'widgets_init', 'magazino_widgets_init' );
