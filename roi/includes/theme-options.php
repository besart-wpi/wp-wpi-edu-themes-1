<?php
/**
 * Initialize the custom Theme Options.
 */
add_action( 'admin_init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 *
 * @return    void
 * @since     2.0
 */
function custom_theme_options() {
  
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( ot_settings_id(), array() );
  
  
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
	$custom_settings = array( 
		'contextual_help' => array(
			'content'       => array( 
			array(
			'id'        => 'option_types_help',
			'title'     => __( 'Option Types', 'roi-backed' ),
			'content'   => '<p>' . __( 'Help content goes here!', 'roi-backed' ) . '</p>'
			)
			),
			'sidebar'       => '<p>' . __( 'Sidebar content goes here!', 'roi-backed' ) . '</p>'
		),
		
		/* ======== Section Settings ========= */
		'sections'        => array(
			
			array(
				'id'          => 'footer_options',
				'title'       => __( 'Footer Options', 'roi-backed' )
			),			
			
		),
		
		/* ======== Fields Settings ========= */
		'settings'        => array(
		
			/* ---- Footer Options ------------------------------ */
			array(
				'id'          => 'footer_tab',
				'label'       => __( 'Footer Left Options', 'roi-backend' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'tab',
				'section'     => 'footer_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
			array(
				'id'          => 'footer_address',
				'label'       => __( 'Address', 'roi-backend' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'textarea',
				'section'     => 'footer_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
			array(
				'id'          => 'footer_contact_url',
				'label'       => __( 'Contact Link URL', 'roi-backend' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'footer_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
			array(
				'id'          => 'footer_directions_url',
				'label'       => __( 'Directions Link URL', 'roi-backend' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'footer_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
			array(
				'id'          => 'connect_tab',
				'label'       => __( 'Connect With Us Icons', 'roi-backend' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'tab',
				'section'     => 'footer_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
			array(
				'id'          => 'instagram_url',
				'label'       => __( 'Instagram URL', 'roi-backend' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'footer_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
			array(
				'id'          => 'twitter_url',
				'label'       => __( 'Twitter URL', 'roi-backend' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'footer_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
			array(
				'id'          => 'facebook_url',
				'label'       => __( 'Facebook URL', 'roi-backend' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'footer_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
			array(
				'id'          => 'youtube_url',
				'label'       => __( 'YouTube URL', 'roi-backend' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'footer_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
			array(
				'id'          => 'inquire_url',
				'label'       => __( 'Inquire URL', 'roi-backend' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'footer_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
			array(
				'id'          => 'visit_url',
				'label'       => __( 'Visit URL', 'roi-backend' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'footer_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
			array(
				'id'          => 'apply_url',
				'label'       => __( 'Apply URL', 'roi-backend' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'footer_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
			
			
			
		) /* --- End Fields Settings --- */
	);
  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( ot_settings_id() . '_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( ot_settings_id(), $custom_settings ); 
  }
  
}