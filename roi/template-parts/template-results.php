<div id="<?php echo $post->post_name; ?>" class="section-results section-page"><div class="section-page-border"></div><div class="section-page-inner">

	<?php get_template_part('template-parts/content');  ?>

	<div class="statistic-boxes"><div class="container clearfix">
	
		<a href="#" id="stat-prev" class="cycle-prev mobile-view"><img src="<?php bloginfo('template_url'); ?>/images/prev_white.png" alt=""></a>
		<a href="#" id="stat-next" class="cycle-next mobile-view"><img src="<?php bloginfo('template_url'); ?>/images/next_white.png" alt=""></a>
			
		<div class="container-inner statistic-wrap"
			data-cycle-fx="carousel"
			data-cycle-timeout="0"
			data-cycle-carousel-visible="2"
			data-cycle-carousel-fluid="true"
			data-cycle-slides="> .statistic-box-outer"
			data-cycle-prev="#stat-prev"
			data-cycle-next="#stat-next"
			data-cycle-swipe="true"
			>
		
			<?php for($i = 1; $i <= 10; $i++): ?>
				<?php
				$box_heading = trim(get_post_meta($post->ID, '_box_'.$i.'_heading', true));
				$box_sub_heading = trim(get_post_meta($post->ID, '_box_'.$i.'_sub_heading', true));
				$box_text = trim(get_post_meta($post->ID, '_box_'.$i.'_text', true));
				$box_url = esc_url(get_post_meta($post->ID, '_box_'.$i.'_url', true));
				?>
				<div class="statistic-box-outer"><div class="statistic-box-outer-inner">
					<?php if($box_url): ?>
						<a class="box-<?php echo $i; ?> statistic-box" href="<?php echo $box_url; ?>">
					<?php else: ?>
						<div class="box-<?php echo $i; ?> statistic-box">
					<?php endif; ?>
							<div class="box-inner"><div class="box-inner-inner">
								<div class="box-heading-wrap">
									<?php if($i == 5 || $i == 6 || $i == 7 || $i == 9): ?>
										<span class="box-sub-heading"><?php echo $box_sub_heading; ?></span>
										<span class="box-heading"><?php echo $box_heading; ?></span>						
									<?php else: ?>
										<span class="box-heading"><?php echo $box_heading; ?></span><span class="box-sub-heading"><?php echo $box_sub_heading; ?></span>
									<?php endif; ?>
								</div><div class="box-text">
									<?php echo $box_text; ?>
								</div>
							</div></div>
					<?php if($box_url): ?>
						</a>
					<?php else: ?>
						</div>
					<?php endif; ?>
				</div></div>
			<?php endfor; ?>	
			
	</div></div></div><!-- .statistic-boxes -->

	<?php get_template_part('template-parts/quotes'); ?>

	<?php get_template_part('template-parts/video'); ?>

	<?php
	$workplaces_heading = trim(get_post_meta($post->ID, '_workplaces_heading', true));
	$workplaces = get_post_meta($post->ID, '_workplaces', true);
	?>

	<?php if(!empty($workplaces)): ?>
		<div class="workplaces-section">
			<div class="container clearfix">
				<div class="workplaces-heading"><div class="workplaces-heading-inner">
					<h3><?php echo $workplaces_heading; ?></h3>
				</div></div>
				<div class="workplaces-entries">
					<a href="#" id="we-prev" class="cycle-prev mobile-view"><img src="<?php bloginfo('template_url'); ?>/images/btn_prev_purple.png" alt=""></a>
					<a href="#" id="we-next" class="cycle-next mobile-view"><img src="<?php bloginfo('template_url'); ?>/images/btn_next_purple.png" alt=""></a>
					
					<div class="clearfix mobile-cycle-flex-visible"
					data-cycle-fx="carousel"
					data-cycle-timeout="0"
					data-cycle-carousel-visible="2"
					data-cycle-auto-height="calc"
					data-cycle-carousel-fluid="true"
					data-cycle-slides="> .workplaces-column"				
					data-cycle-swipe="true"
					data-cycle-prev="#we-prev"
					data-cycle-next="#we-next"
					>
					
						<?php 
						$count = count($workplaces);
						$ctr = 1;
						$end = ceil($count/5);
						?>
						<ul class="workplaces-column">
							<?php foreach($workplaces as $workplace): ?>
								<li><?php echo $workplace['title']; ?></li>
								<?php
								if((0 == $ctr % $end) && $ctr != $count){
									echo '</ul><ul class="workplaces-column">';
								}
								$ctr++;
								?>
							<?php endforeach; ?>
						</ul>
					
					</div>
				</div>
			</div>
		</div>

	<?php endif; ?>
	
</div></div>