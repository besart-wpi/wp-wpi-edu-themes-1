<div id="<?php echo $post->post_name; ?>" class="section-innovate section-page"><div class="section-page-border"></div><div class="section-page-inner">

	<?php get_template_part('template-parts/content');  ?>

	<div class="quotes-grid"><div class="container clearfix">
		
		<h3 class="quote-grid-heading mobile-view"><?php echo trim(get_post_meta($post->ID, '_quotes_grid_heading', true)); ?></h3>
		
		<div class="quote-grid-wrap mobile-cycle"
		data-cycle-fx="scrollHorz"
		data-cycle-timeout="0"		
		data-cycle-auto-height="calc"
		data-cycle-slides="> div.quote-grid-cycle"
		data-cycle-swipe="true"
		>

			<a href="#" class="cycle-prev mobile-view"><img src="<?php bloginfo('template_url'); ?>/images/slide_mobile_prev.png" alt=""></a>
			<a href="#" class="cycle-next mobile-view"><img src="<?php bloginfo('template_url'); ?>/images/slide_mobile_next.png" alt=""></a>			

			<div class="quote-grid desktop-view">
				<h3 class="quote-grid-heading"><?php echo trim(get_post_meta($post->ID, '_quotes_grid_heading', true)); ?></h3>
			</div>
			
			<?php $quotes = get_post_meta($post->ID, '_quotes', true); ?>
			<?php $ctr = 1; ?>
			<?php if(!empty($quotes)): foreach($quotes as $quote): ?>
				
				<div class="quote-grid quote-grid-cycle quote-grid-<?php echo $ctr; ?> clearfix"><div class="quote-grid-inner clearfix">
					<div class="quote-photo-wrap"><div class="quote-photo">
						<?php if($quote['photo']): ?>
							<img srcset="<?php echo theme_get_scrset($quote['photo'], 'photo-size'); ?>" alt="<?php echo $quote['title']; ?>">
						<?php endif; ?>
					</div></div>
					<div class="quote-grid-text-wrap"><div class="qgt-inner"><div class="qgt-inner-inner">
						<div class="quote-grid-text">
							<?php echo wpautop(stripslashes($quote['quote'])); ?>
						</div>
						<p class="quote-grid-author">
							<?php echo $quote['title']; ?>, 
							<?php echo $quote['class']; ?>, 
							<?php echo $quote['major']; ?>
						</p>
					</div></div></div>
				</div></div>
				
				<?php
				$ctr++;
				if($ctr == 8){
					$ctr = 1;
				}
				?>
			<?php endforeach; endif; ?>
		
		
		</div>
		
	</div></div>

	<?php get_template_part('template-parts/video'); ?>	

</div></div>