<?php
$video_youtube_id = trim(get_post_meta($post->ID, '_video_youtube_id', true));
$video_caption = get_post_meta($post->ID, '_video_caption', true);
$video_image = get_post_meta($post->ID, '_video_image', true);

if($video_youtube_id):
?>
	<div class="video-section"><div class="container">
		<div class="video-section-inner">
			<?php
			if(!$video_image){
				$video_image_srcset = 'http://img.youtube.com/vi/'.$video_youtube_id.'/0.jpg';
			} else {
				$video_image_srcset = theme_get_scrset($video_image, 'video-size');
			}
			?>
			<div class="desktop-view">
				<a href="https://www.youtube.com/watch?v=<?php echo $video_youtube_id; ?>" class="lightbox-media">
					<img src="<?php bloginfo('template_url'); ?>/images/play.png" class="btn-play" alt="">
					<img srcset="<?php echo $video_image_srcset; ?>" alt="" class="video-pic">
					<?php if($video_caption): ?>
						<div class="video-caption">
							<?php echo wpautop(stripslashes($video_caption)); ?>
						</div>
					<?php endif; ?>
				</a>
			</div>
			<div class="mobile-view clearfix">
				<?php if($video_caption): ?>
					<div class="video-caption">
						<div class="video-caption-inner"><div class="video-caption-inner-inner">
							<?php echo wpautop(stripslashes($video_caption)); ?>
						</div></div>
					</div>
				<?php endif; ?>
					<div class="video-wrap">
						<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $video_youtube_id; ?>" frameborder="0" allowfullscreen></iframe>
					</div>
			</div>
		</div>
	</div></div>
<?php
endif;