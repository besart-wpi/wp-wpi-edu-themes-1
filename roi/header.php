<!--[if IE]><![endif]-->
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta http-equiv="x-ua-compatible" content="ie=edge" />
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />	
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_url'); ?>/favicon.ico" />
	<script src="//use.typekit.net/vec1sni.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	<?php wp_head(); ?>	
</head>
<body <?php body_class(); ?>>
	<div id="top"></div>
	<header id="header"><div id="header-inner">
		<div class="container clearfix">
			<div id="logo-wrap">
				<?php $heading_tag = ( is_home() || is_front_page() ) ? 'h1' : 'div'; ?>
				<<?php echo $heading_tag; ?> id="logo">
					<a href="http://www.wpi.edu/admissions/undergraduate.html"><img srcset="<?php bloginfo('template_url'); ?>/images/logo.png 1x, <?php bloginfo('template_url'); ?>/images/logo2x.png 2x" alt=""><span class="site-name"><?php bloginfo('name'); ?></span></a>
				</<?php echo $heading_tag; ?>>
			</div>
			<a href="#" id="show-menu" class="mobile-view">
				<span class="show-menu-text">Menu</span>
				<span class="menu-icon">
					<span></span>
					<span></span>
					<span></span>
				</span>
			</a>
			<nav id="nav">
				<?php
				$args = array(
					'post_type' => 'page',
					'orderby' => 'menu_order',
					'order' => 'ASC',
					'posts_per_page'=> -1
				);
				
				$all_pages = get_posts($args);
				if(!empty($all_pages)):
				$ctr = 1;
				?>
				<ul class="clearfix">
					<?php foreach($all_pages as $the_page): ?>
					<li class="<?php echo ($ctr==1) ? 'current-menu-item' : ''; $ctr++; ?>"><a href="<?php echo esc_url(home_url('/')); ?>#<?php echo $the_page->post_name; ?>"><?php echo $the_page->post_title; ?></a></li>
					<?php endforeach; ?>									
				</ul>
				<?php endif; ?>
			</nav>
		</div>
		
		<?php if(is_front_page()): ?>
			<div id="find-out-more">
				<a href="#" class="find-out-more-close"><img src="<?php bloginfo('template_url'); ?>/images/x.png" alt=""></a>
				<a href="#learn-more">Find Out More<br><img class="fom-arrow" src="<?php bloginfo('template_url'); ?>/images/arrow_fom.png" alt=""></a>
			</div>
		<?php endif; ?>
		
	</div></header>