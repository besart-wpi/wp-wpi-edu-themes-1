<?php get_header(); ?>

<?php 
// Home Page Slideshow
$home_page_slides = new WP_Query( array(
	'post_type' => 'home_page_slideshow',
	'posts_per_page' => -1,
	'orderby' => 'menu_order',
	'order' => 'DESC'
) );

if ( $home_page_slides->have_posts() ): ?>
<div id="slider">
	<div class="cycle-slideshow cycle-slider" 
		data-cycle-fx="fade" 
		data-cycle-slides="> div"
		data-cycle-timeout="<?php echo ot_get_option('home_page_slideshow_pause_time') ? ot_get_option('home_page_slideshow_pause_time') : '5000'; ?>"
		data-cycle-speed="<?php echo ot_get_option('home_page_slideshow_transition_time') ? ot_get_option('home_page_slideshow_transition_time') : '1000'; ?>"
		data-cycle-pause-on-hover="true"
		>

        <?php while ( $home_page_slides->have_posts() ): $home_page_slides->the_post(); 
			$image = get_post_meta($post->ID, 'image', true); // Expects an attachment ID
			$image_attributes = wp_get_attachment_image_src( $image, 'home-page-slide' );
			$caption = get_post_meta($post->ID, 'caption', true);
			$url = get_post_meta($post->ID, 'url', true);
			
			$before_caption = $url ? '<a href="'.esc_url($url).'">' : '';
			$after_caption = $url ? ' <img src="'.get_stylesheet_directory_uri().'/images/icon-slide.png" alt=""/></a>' : ''; 
			
			if( $image_attributes[0] ): ?>
            <div class="slide-item" style="background-image:url(<?php echo $image_attributes[0]; ?>)">
                <div class="wrap">
                	<img class="image-mobile" src="<?php echo $image_attributes[0]; ?>" alt="">
                	<?php if($caption): ?>
                    <div class="caption">
                        <h1><?php echo $before_caption; ?>Dictumst habitant. Primis ultrices quae <br/>possimus atque gravida justo, veritatis<?php echo $after_caption; ?></h1>
                    </div>
                    <?php endif; ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/blank-slide.gif" class="blank" alt=""/>
                </div>
            </div>
        <?php endif; endwhile; ?>
	</div><!-- .cycle-slideshow -->
</div><!-- #slider -->      
<?php endif;
/* Restore original Post Data */
wp_reset_postdata();
// END Home Page Slidshow ?>


<?php 
// 2-Column Content
$column1 = get_post_meta($post->ID, 'column1', true);
$column2 = get_post_meta($post->ID, 'column2', true);
$layout_variation = get_post_meta($post->ID, 'layout_variation', true);

// Reverse the array sort so the expiration date loop (below) starts looking from the bottom of the list and works up
// This shouldn't matter if an expiration date is always set, but in the case that one isn't, it will give preference to items at the top of the list
if(is_array($column1)){ array_reverse( $column1 ); }
if(is_array($column2)){ array_reverse( $column2 ); }

// Find Active Column Content based on "expiration_date"
$column_content = array();
for($i=1;$i<=2;$i++){
	$last_date = 0;
	foreach( ${'column'.$i} as $key => $column ){
		// Get expiration date
		$expiration_date = strtotime($column['expiration_date']); // field should be in format "2015-07-08"
		
		// Set active content for column if: 
		// 1. has a higher expiration date 
		// 2. empty expiration date
		if( $expiration_date >= $last_date || empty($expiration_date) ){
			// echo '<p>Column ' . $i . ' key ' . $key . ' date of ' . $expiration_date . ' is greater than last date of ' . $last_date . '</p>';
			$column_content[$i] = $column;	
			if($expiration_date >= $last_date){
				$last_date = $expiration_date;
			}
		}
	}
}
?>
<div id="home-row" <?php if( $layout_variation == 'without_headings' ){ echo 'class="col-one"'; } ?>>
	<div class="wrap">
		<?php if( $layout_variation == 'without_headings' ){ echo '<div class="row_wrap"><div class="row">'; } ?>
			<div class="col">
				<div class="inner">
					<?php 
						// Heading Top
						if( $layout_variation != 'without_headings' && $column_content[1]['heading_top'] ){
							echo '<h3 class="tab-head">'.$column_content[1]['heading_top'].'</h3>';
						}
						// Title
						echo $column_content[1]['title'] ? '<h2>'.$column_content[1]['title'].'</h2>' : ''; 
						// Content
						echo apply_filters('the_content', strip_p($column_content[1]['content']));
					?>
					<div class="clear"></div>
				</div>
			</div>
			<div class="col last">
				<div class="inner">
                	<?php 
						// Heading Top
						if( $layout_variation != 'without_headings' && $column_content[2]['heading_top'] ){
							echo '<h3 class="tab-head">'.$column_content[2]['heading_top'].'</h3>';
						}
						// Title
						echo $column_content[2]['title'] ? '<h2>'.$column_content[1]['title'].'</h2>' : ''; 
						// Content
						echo apply_filters('the_content', strip_p($column_content[2]['content']));
					?>
                	<div class="clear"></div>
                </div>
			</div>
			<div class="clear"></div>
		<?php if( $layout_variation == 'without_headings' ){ echo '</div></div><!-- .row_wrap and .row -->'; } ?>
	</div>
</div>
<?php // END 2-Column Content ?>


<?php 
// Lower Callout
$lower_callout_heading = get_post_meta($post->ID, 'lower_callout_heading', true);
$lower_callout_text = get_post_meta($post->ID, 'lower_callout_text', true);
$lower_callout_image = get_post_meta($post->ID, 'lower_callout_image', true);
$lower_callout_image_attributes = wp_get_attachment_image_src( $lower_callout_image, 'full' );
?>
<div id="home-footer">
	<div class="wrap">
		
		<div class="col col-img">
			<img src="<?php echo $lower_callout_image_attributes[0]; ?>" alt="">
		</div>
		<div class="col last">
			<?php 
			// Heading
			echo $lower_callout_heading ? '<h2>'.$lower_callout_heading.'</h2>' : ''; 
			// Text
			echo apply_filters('the_content', $lower_callout_text);
			?>
		</div>
		
	</div>
</div>

<?php get_footer(); ?>