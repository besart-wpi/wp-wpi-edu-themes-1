<?php get_header(); ?>

<div id="content" class="content">
	<div class="wrap">
    
    	<?php if( have_posts() ): while( have_posts() ): the_post(); ?>
			<h1 class="main-title"><?php the_title(); ?></h1>
			<?php the_content(); ?>
        <?php endwhile; endif; ?>

	</div><!-- .wrap -->
</div><!-- #content -->

<?php get_footer(); ?>