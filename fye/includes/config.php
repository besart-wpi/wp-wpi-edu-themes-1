<?php
// =================================================
//	:: Admin Favicon ::
// -------------------------------------------------
function add_favicon() {
	$favicon_url = get_stylesheet_directory_uri() . '/admin-favicon.ico';
	echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';
}
add_action('login_head', 'add_favicon');
add_action('admin_head', 'add_favicon');
add_action('wp_head', 'add_favicon');
	
	
// =================================================
//	:: Enqueue Scripts ::
// -------------------------------------------------
	function theme_enqueue_scripts(){
		/* -- Enqueue CSS File -- */			
		wp_enqueue_style( 'theme-normalize', get_template_directory_uri() . '/css/normalize.min.css');
		wp_enqueue_style( 'theme-reset', get_template_directory_uri() . '/css/reset.css');
		wp_enqueue_style( 'theme-typography', get_template_directory_uri() . '/css/typography.css');
		wp_enqueue_style( 'theme-style', get_stylesheet_uri());
		wp_enqueue_style( 'theme-responsive', get_template_directory_uri() . '/css/responsive.css');
		wp_enqueue_style( 'fontawesome', '//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
		
		/* -- Enqueue JS File -- */
		// Header
		wp_enqueue_script('jquery');
		wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/modernizr-2.6.2-respond-1.1.0.min.js', array(), '1.1.0');
		wp_enqueue_script( 'jquery-galleria', get_template_directory_uri() . '/js/galleria/galleria-1.4.2.min.js', array( 'jquery' ), '1.4.2');
		
		// Footer
		wp_enqueue_script( 'jquery-hoverintent', get_template_directory_uri() . '/js/hoverIntent.js', array( 'jquery' ), 'r7', true);
		wp_enqueue_script( 'jquery-superfish', get_template_directory_uri() . '/js/superfish.js', array( 'jquery' ), '1.7.5', true);
		wp_enqueue_script( 'jquery-cycle2', get_template_directory_uri() . '/js/jquery.cycle2.min.js', array( 'jquery' ), '2.1.6', true);
		wp_enqueue_script( 'jquery-fitvids', get_template_directory_uri() . '/js/jquery.fitvids.js', array( 'jquery' ), '1.1', true);
		
		wp_register_script('theme-main', get_template_directory_uri() . '/js/main.js', 'jquery', '1.0', TRUE);
		wp_localize_script('theme-main', 'ajax_var', array( 'url' => admin_url( 'admin-ajax.php' ), 'nonce' => wp_create_nonce( 'fye-ajax-nonce' ) ) );
		wp_enqueue_script( 'theme-main');
	}
	add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );
	
		
// =================================================
//	:: Theme Support ::
// -------------------------------------------------
add_theme_support( 'title-tag' );
add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );


// =================================================
//	:: Register Thumbnails ::
// -------------------------------------------------
	add_theme_support( 'post-thumbnails', array('post') );
	set_post_thumbnail_size( 200, 150, true );
	add_image_size( 'home-page-slide', 1400, 579, true );


// =================================================
//	:: Register Menus ::
// -------------------------------------------------
	if ( function_exists( 'register_nav_menus' ) ) {
		register_nav_menus( array(
			'main_menu' => 'Main Menu'
		));
	}
	
	
// =================================================
//	:: IE Fix ::
// -------------------------------------------------	
	add_filter( 'wp_head' , 'add_shiv_for_ie' );
	function add_shiv_for_ie() { ?>
		<!--[if lt IE 9]>			
			<script src="<?php bloginfo('template_url'); ?>/js/html5shiv.js"></script>			
			<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/ie.css" />
		<![endif]-->
	<?php
	}
	
	
// =================================================
//	:: TinyMCE Plugins / Config ::
// -------------------------------------------------
	// TinyMCE Options
	add_filter('tiny_mce_before_init', 'custom_tiny_mce_before_init');
	function custom_tiny_mce_before_init( $options ) {
		
		// Custom Style Formats
		$style_formats = array(  
			// Each array child is a format with it's own settings
			array(  
				'title' => 'Sub Title',  
				'block' => 'h2',  
				'classes' => 'sub-title'
				
			), 
			array(  
				'title' => 'Heading Red',  
				'block' => 'h4',  
				'classes' => 'red'
				
			), 
			array(  
				'title' => 'Indented List',  
				'block' => 'ul',  
				'classes' => 'indent'
				
			), 
			array(  
				'title' => 'Gray Box',  
				'block' => 'div',  
				'classes' => 'gray',
				'wrapper' => true
			),
			array(  
				'title' => 'Playlist Caption',  
				'block' => 'h4',  
				'classes' => 'playlist_caption'
			),
			array(  
				'title' => 'Icon - Exclamation Triangle',  
				'block' => 'h4',  
				'classes' => 'alert icon-attention'
			),
			array(  
				'title' => 'Icon - Circle Checked',  
				'block' => 'h5',  
				'classes' => 'icon-ok-circled2'
			),
			array(  
				'title' => 'Bold Link',  
				'inline' => 'a',  
				'classes' => 'boldlink'
			),
			array(  
				'title' => 'Large Icon (Fontawesome)',  
				'inline' => 'span',  
				'classes' => 'iconlarge'
			),
			array(  
				'title' => 'Suitcase Icon (Fontawesome)',  
				'inline' => 'span',  
				'classes' => 'faicon-suitcase'
			),
		);  
		// Insert the array, JSON ENCODED, into 'style_formats'
		$options['style_formats'] = json_encode( $style_formats );  
		
		// For Fontawesome plugin
		if ( ! isset( $options['extended_valid_elements'] ) ) {
			$options['extended_valid_elements'] = '';
		} else {
			$options['extended_valid_elements'] .= ',';
		}
	 
		$options['extended_valid_elements'] .= 'span[class]';
		return $options;
	}
	
	// Add new buttons
	add_filter('mce_buttons', 'custom_mce_register_buttons');
	function custom_mce_register_buttons($buttons) {
	   array_push($buttons, 'separator', 'fontawesome', 'noneditable', 'styleselect');
	   return $buttons;
	}
	 
	// Load the TinyMCE plugin : fontawesome
	add_filter('mce_external_plugins', 'fontawesome_register_tinymce_javascript');
	function fontawesome_register_tinymce_javascript($plugin_array) {
	   $plugin_array['fontawesome'] = get_template_directory_uri() . '/includes/tinymce_plugins/TinyMCE-FontAwesome-Plugin/fontawesome/plugin.js';
	   return $plugin_array;
	}
	// Add Fontawesome CSS to editor
	function fontawesome_add_editor_styles() {
		add_editor_style( '//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' );
		add_editor_style( get_template_directory_uri() . '/css/typography.css' );
	}
	add_action( 'admin_init', 'fontawesome_add_editor_styles' );
	
	
// =================================================
//	:: Hide editor on specific pages ::
// -------------------------------------------------
add_action( 'admin_init', 'hide_editor' );

function hide_editor() {
  // Get the Post ID.
  $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
  if( !isset( $post_id ) ) return;

  // Hide the editor on the page titled 'Homepage'
  if($post_id == get_option('page_on_front')){ 
    remove_post_type_support('page', 'editor');
  }

  // Hide the editor on a page with a specific page template
  // Get the name of the Page Template file.
  $template_file = get_post_meta($post_id, '_wp_page_template', true);

  if($template_file == 'my-page-template.php'){ // the filename of the page template
    // remove_post_type_support('page', 'editor');
  }
}

// =================================================
//	:: Custom Contact Form 7 AJAX Loading Gif  ::
// -------------------------------------------------
add_filter('wpcf7_ajax_loader', 'my_wpcf7_ajax_loader');
function my_wpcf7_ajax_loader () {
	return  get_bloginfo('stylesheet_directory') . '/images/ajax-loader.gif';
}