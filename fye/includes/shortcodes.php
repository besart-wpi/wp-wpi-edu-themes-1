<?php 
add_filter( 'the_content', 'tgm_io_shortcode_empty_paragraph_fix' );
/**
 * Filters the content to remove any extra paragraph or break tags
 * caused by shortcodes.
 *
 * @since 1.0.0
 *
 * @param string $content  String of HTML content.
 * @return string $content Amended string of HTML content.
 */
function tgm_io_shortcode_empty_paragraph_fix( $content ) {
 
    $array = array(
        '<p>['    => '[',
        ']</p>'   => ']',
        ']<br />' => ']',
		'<p>&nbsp;</p>' => ''
    );
    return strtr( $content, $array );
 
}


/*
 * Column Shortcodes
 ---------------------------------- */
function shortcode_column_half( $atts, $content = null ) {
	return '<div class="col half">' . apply_filters('the_content', $content ) . '</div>';
}
add_shortcode('half', 'shortcode_column_half');

function shortcode_column_half_last( $atts, $content = null ) {
	return '<div class="col half last">' . apply_filters('the_content', $content ) . '</div><div class="clear"></div>';
}
add_shortcode('half_last', 'shortcode_column_half_last');


/*
 * Callout Box Shortcode
 ---------------------------------- */
function shortcode_callout_box( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'color' => ''
		), $atts, 'callout_box' );
	return '<div class="feedback clearfix '.$atts['color'].'">' . apply_filters('the_content', $content ) . '</div>';
}
add_shortcode('callout_box', 'shortcode_callout_box');


/*
 * Border Box Shortcode
 ---------------------------------- */
function shortcode_border_box( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'align' => ''
		), $atts, 'border_box' );
		
		$alignClass = '';
		if($atts['align']){
			$alignClass = ' align' . $atts['align'];
		}
		
	return '<div class="border_box'.$alignClass.'">' . apply_filters('the_content', $content ) . '</div>';
}
add_shortcode('border_box', 'shortcode_border_box');


/*
 * Step Note Shortcode
 ---------------------------------- */
function shortcode_step_note( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'color' => 'blue',
			'label' => 'See Next',
			'url'   => '',
		), $atts, 'step_note' );
		
	$shortcode = '<div class="steps-note">
			<h3>'.strip_tags($content).'</h3>
			<a href="'.esc_url($atts['url']).'" class="btn-colord btn-'.$atts['color'].' alignright">'.$atts['label'].'</a>	
			<div class="clear"></div>
		</div>';
	return $shortcode;
}
add_shortcode('step_note', 'shortcode_step_note');


/*
 * Accordion Shortcode
 ---------------------------------- */
function shortcode_accordion( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'label' => 'Expand to learn more'
		), $atts, 'shortcode_accordion' );
	
	$shortcode = '<div class="accord">
				<h4 class="ac-tab icon-expand">'.$atts['label'].'</h4>
				<div class="ac-content">'.apply_filters('the_content', strip_p($content) ).'<div class="clear"></div></div>
			</div>';
	return $shortcode;
}
add_shortcode('accordion', 'shortcode_accordion');


/*
 * Galleria Photo Gallery Shortcode
 ---------------------------------- */
function shortcode_galleria_gallery( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'id' => ''
		), $atts, 'galleria' );

	if($atts['id']=='')
		return;
		
	// Get gallery images
	$images = get_post_meta($atts['id'], 'images', true); // comma sep string of attachment IDs
	// Convert to array
	$images = explode(",", $images);
			
	if( empty( $images ) )
		return;
	
	$shortcode = '<div class="galleria">';
	foreach($images as $image_id){
		$image_attributes = wp_get_attachment_image_src( $image_id, 'full' );
		$image = get_post( $image_id );

		$shortcode .= '<img src="'.$image_attributes[0].'" data-title="'.$image->post_title.'" data-description="'.$image->post_excerpt.'">';
	}
	$shortcode .= '</div>';
	
	$shortcode .= "<script>
					Galleria.loadTheme('".get_stylesheet_directory_uri()."/js/galleria/themes/classic/galleria.classic.min.js');
					Galleria.configure({
						_toggleInfo: false,
						responsive: true
					});
					Galleria.run('.galleria', {
						height: 0.5625
					});
				</script>";
		
	return $shortcode;
}
add_shortcode('galleria', 'shortcode_galleria_gallery');

/*
 * Carousel Photo Gallery Shortcode
 ---------------------------------- */
function shortcode_carousel_gallery( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'id' => ''
		), $atts, 'carousel' );

	if($atts['id']=='')
		return;
		
	// Get gallery images
	$images = get_post_meta($atts['id'], 'images', true); // comma sep string of attachment IDs
	// Convert to array
	$images = explode(",", $images);
			
	if( empty( $images ) )
		return;
	
	$shortcode = '<div class="carousel">
	<div class="cycle-slideshow" 
	data-cycle-fx="scrollHorz"
    data-cycle-pause-on-hover="true"
    data-cycle-speed="500"
	data-cycle-timeout="5000" 
	data-cycle-prev="#prev-'.$atts['id'].'"
    data-cycle-next="#next-'.$atts['id'].'">';
	foreach($images as $image_id){
		$image_attributes = wp_get_attachment_image_src( $image_id, 'full' );
		$image = get_post( $image_id );

		$shortcode .= '<img src="'.$image_attributes[0].'" alt="">';
	}
	$shortcode .= '</div> <!-- .cycle-slideshow -->';
	$shortcode .= '<a href=# class="carousel-nav carousel-nav-prev" id="prev-'.$atts['id'].'">Prev</a>';
    $shortcode .= '<a href=# class="carousel-nav carousel-nav-next" id="next-'.$atts['id'].'">Next</a>';
	$shortcode .= '</div>';
		
	return $shortcode;
}
add_shortcode('carousel', 'shortcode_carousel_gallery');