<?php 
/*
Template Name: New Student Orientation
*/

get_header();
the_post(); 

// Get post meta
$splash_image = get_post_meta($post->ID, 'splash_image', true);
$splash_image_attributes = wp_get_attachment_image_src( $splash_image, 'home-page-slide' );
$splash_caption = get_post_meta($post->ID, 'splash_caption', true);

$button_yellow_label = get_post_meta($post->ID, 'button_yellow_label', true);
$button_yellow_url = get_post_meta($post->ID, 'button_yellow_url', true);

$button_green_label = get_post_meta($post->ID, 'button_green_label', true);
$button_green_url = get_post_meta($post->ID, 'button_green_url', true);

$button_blue_label = get_post_meta($post->ID, 'button_blue_label', true);
$button_blue_url = get_post_meta($post->ID, 'button_blue_url', true);

$button_purple_label = get_post_meta($post->ID, 'button_purple_label', true);
$button_purple_url = get_post_meta($post->ID, 'button_purple_url', true);

$button_black_label = get_post_meta($post->ID, 'button_black_label', true);
$button_black_url = get_post_meta($post->ID, 'button_black_url', true);
?>

<div id="banner" style="background-image:url(<?php echo $splash_image_attributes[0]; ?>)">
	<div class="wrap">
    	<img class="image-mobile" src="<?php echo $splash_image_attributes[0]; ?>" alt="">
		<?php echo $splash_caption ? '<h1 class="main-title">'.$splash_caption.'</h1>' : ''; ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/blank-slide.gif" alt="" class="blank" />
        <h1 class="main-title"><?php the_title(); ?></h1>
	</div>
</div>

<div id="content" class="content nso">
	<div class="wrap">
		
        <div class="row_wrap">        
		
            <div class="row">
                <div class="col">
                    <?php the_content(); ?>
                </div>
            </div>

		    <div class="buttonrow">
			
                <div class="col">
    				<?php echo $button_yellow_label ? '<a href="'.esc_url($button_yellow_url).'" class="btn-colord btn-yellow">'.$button_yellow_label.'</a>' : ''; ?>	
                </div>
                <div class="col last">
    				<?php echo $button_green_label ? '<a href="'.esc_url($button_green_url).'" class="btn-colord btn-green alignright">'.$button_green_label.'</a>' : ''; ?>
                </div>
    			<div class="clear"></div>

                <div class="col">
    				<?php echo $button_blue_label ? '<a href="'.esc_url($button_blue_url).'" class="btn-colord btn-blue">'.$button_blue_label.'</a>' : ''; ?>
                </div>
                <div class="col last">
    				<?php echo $button_purple_label ? '<a href="'.esc_url($button_purple_url).'" class="btn-colord btn-purple alignright">'.$button_purple_label.'</a>' : ''; ?>
                </div>
                <div class="clear"></div>

    			<?php echo $button_black_label ? '<a href="'.esc_url($button_black_url).'" class="btn-colord btn-black btn-wide">'.$button_black_label.'</a>' : ''; ?>

            </div>

		</div>

	</div>
        
</div>

<?php get_footer(); ?>