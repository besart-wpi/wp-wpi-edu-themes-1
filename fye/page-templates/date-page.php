<?php 
/*
Template Name: Date Page
*/

get_header(); ?>

<div id="content" class="content">
	<div class="wrap">
    
    	<?php if( have_posts() ): while( have_posts() ): the_post(); ?>
			<h1 class="main-title"><?php the_title(); ?></h1>
            
            <?php // Icon Content
			$icon_contents = array();
			$has_active_icons = false;
			$icon_labels = array(
				4 => 'Get Connected',
				1 => 'Academics',
				2 => 'Housing &amp; Dining',
				7 => 'Financial',
				3 => 'Health Services',
				5 => 'Student Life',
				6 => 'Additional Info'
			);
			
			// Get post meta
			$icon_content_introduction = get_post_meta($post->ID, 'icon_content_introduction', true);
			foreach( $icon_labels as $key => $icon ){
				if( get_post_meta($post->ID, 'icon_content_content'.$key, true) != '' ){
					$has_active_icons = true;
					$icon_contents[$key] = array(
						'heading' => get_post_meta($post->ID, 'icon_content_heading'.$key, true),
						'content' => get_post_meta($post->ID, 'icon_content_content'.$key, true),
					);
				}
			}
			
			// Display Introduction
			echo apply_filters('the_content', strip_p( $icon_content_introduction) ); 
			
			if( $has_active_icons ){
				
				// Display Active Icon Links
				echo '<div class="btn-circles">';
				$icon_count = 0;
				foreach( $icon_contents as $key => $icon_content ){
					$icon_count++;
					$item_class = array();
					if( !empty($icon_content['content']) ): 
						$item_class[] = 'item-'.$key;
						if( count($icon_contents) == $icon_count) {
							$item_class[] = 'last';
						}
						if( ($icon_count - 1) % 3 == 0 ){
							$item_class[] = 'clear3';	
						}
						$item_class = implode( ' ', $item_class );
						?>
						<a href="#info-<?php echo $key; ?>" class="item <?php echo $item_class ?>">
                            <span class="icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-<?php echo $key; ?>.png" alt=""/></span>
                            <?php echo $icon_labels[$key]; ?>
                        </a>
					<?php endif;
				}
				echo '<div class="clear"></div></div><!-- .btn-circles --><hr/><br/>';
				
				
				// Display the Icon Contents
				foreach( $icon_contents as $key => $icon_content ){
					if( !empty($icon_content['content']) ): ?>
						<div class="infos" id="info-<?php echo $key; ?>">
                            <h2 class="info-title"><span class="icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-<?php echo $key; ?>.png" alt=""/></span> <?php echo $icon_content['heading'] ? $icon_content['heading'] : $icon_labels[$key]; ?></h2>
                            <div class="icon_content_wrap">
                    		<?php echo apply_filters('the_content', strip_p( $icon_content['content'] )); ?>                            
                            </div>
                        </div>
					<?php endif;
				}

			} // END $has_active_icons
			
			// END Icon Content ?>
            
            
			<?php the_content(); ?>
            
        <?php endwhile; endif; ?>
        
        <div class="sked-buttons">
        	<?php
			$args = array(
				'container'		=> '',
				'theme_location'=> 'main_menu',
				'depth'			=> 2,
				'menu_class'	=> '',
				'link_before'   => '<span>',
				'link_after'    => '</span>'
			);
			wp_nav_menu($args);
			?>
			<div class="clear"></div>
		</div>

	</div><!-- .wrap -->
</div><!-- #content -->

<?php get_footer(); ?>