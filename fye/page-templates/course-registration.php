<?php 
/*
Template Name: Course Registration
*/

get_header(); ?>

<div id="content" class="content">
	<div class="wrap">
    
    	<?php if( have_posts() ): while( have_posts() ): the_post(); ?>
			<h1 class="main-title"><?php the_title(); ?></h1>
            
            <?php the_content(); ?>
            
            <?php // Step Content
			$step_contents = array();
			$has_active_steps = false;
			
			// Get post meta
			for($i=1;$i<=6;$i++){
				if( get_post_meta($post->ID, 'step_content'.$i, true) != '' ){
					$has_active_steps = true;
					$step_contents[$i] = array(
						'heading' => get_post_meta($post->ID, 'step_heading'.$i, true),
						'content' => get_post_meta($post->ID, 'step_content'.$i, true),
					);
				}
			}
			
			if( $has_active_steps ){
				
				// Display Active Step Links
				echo '<div class="btn-steps">';
				foreach( $step_contents as $key => $step_content ){
					if( !empty($step_content['content']) ): ?>
						<a href="#step-<?php echo $key; ?>" class="item item-<?php echo $key; echo $key==1 ? ' active' : ''; echo $key==6 ? ' last' : '';?>">
                            Step <span class="icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/blank-number.gif" alt=""/><span class="num"><?php echo $key; ?></span></span>
                        </a>
					<?php endif;
				}
				echo '<div class="clear"></div></div><!-- .btn-steps --><hr/><br/>';
				
				echo '<div id="steps_top" name="steps_top"></div>';
				
				// Display the Step Contents
				foreach( $step_contents as $key => $step_content ){
					if( !empty($step_content['content']) ): ?>
                    
                    	<div class="step-info step-<?php echo $key; ?>" id="step-<?php echo $key; ?>" <?php echo $key > 1 ? ' style="display:none;"' : ''; ?>>
                            <h2 class="step-title"><span class="num"><?php echo $key; ?></span> <?php echo $step_content['heading'] ? $step_content['heading'] : 'Step ' . $key; ?></h2>
                            <div class="step-content">
                            <?php 
								// Strip paragraph tags
								$content = str_replace( '<p>', '', $step_content['content']);
								$content = str_replace( '</p>', "\n", $content);
								echo apply_filters('the_content', $content); 
							?>
                            </div>
                        </div>

					<?php endif;
				}

			} // END $has_active_steps
			
			// END Step Content ?>
            
        <?php endwhile; endif; ?>

	</div><!-- .wrap -->
</div><!-- #content -->

<?php get_footer(); ?>