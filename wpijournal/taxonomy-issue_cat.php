<?php get_header(); ?>

    <section class="section archive-issue">
        <div class="wrap">
            <div class="upper">
                <h1><?php single_term_title(); ?></h1>
            </div>
            <?php if( have_posts() ): ?>
                <div class="list">
                    <?php while( have_posts() ) : the_post();
                        $post_id        = $post->ID;
                        $subheading     = $post->post_excerpt ? $post->post_excerpt : wp_trim_words( $post->post_content, 16, '...' );
                        $image = false;
                        $youtube_video_id = false;
                        
                        $hero_slides    = get_field( 'hero_slides', $post->ID );

                        $departmentxx    = get_field( 'department', $post->ID );

                        if( $hero_slides ){
                            $image              = $hero_slides[0][ 'image' ];
                            $youtube_video_id   = $hero_slides[0][ 'youtube_video_id' ];
                        }
                        include( get_stylesheet_directory() . '/parts/item-article.php' );
                    endwhile; ?>
                </div>
                <?php if (function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
            <?php else: ?>
                <h3>No articles found.</h3>
            <?php endif; ?>
        </div>
    </section>

<?php get_footer(); ?>