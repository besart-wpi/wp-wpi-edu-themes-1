<?php

/* Template Name: Contact */

get_header(); ?>

<?php the_post(); ?>

<section class="section content">
        <div class="wrap">
            <main class="center">
                <?php
                $intro_image    = get_field( 'intro_image' );
                $introduction   = get_field( 'introduction' );
                $cta_button     = get_field( 'cta_button' );
                $form_heading   = get_field( 'form_heading' );
                $form_shortcode = get_field( 'form_shortcode' ); ?>

                <div class="item-head">
                    <h1><?php the_title(); ?></h1>
                </div>

                <?php if( $intro_image || $introduction ){ ?>
                    <div class="post-item image-text intro">
                        <?php if( $intro_image ){ ?>
                            <figure class="image">
                                <?php echo wp_get_attachment_image( $intro_image['ID'], 'thumb-faculty' ); ?>
                            </figure>
                        <?php } ?>
                        <div class="text">
                            <?php echo $introduction; ?>
                        </div>
                    </div>
                <?php } ?>

                <?php if( $cta_button  ){ ?>
                    <div class="post-item intro-btn">
                        <p><a href="<?php echo $cta_button[ 'url' ]; ?>" class="btn" <?php if( $cta_button[ 'target' ] == '_blank' ){ ?>target="_blank" rel="noopener noreferrer"<?php } ?> title="<?php echo $cta_button[ 'title' ] ?>"><?php echo $cta_button[ 'title' ] ?></a></p>
                    </div>
                <?php } ?>

                <?php if( $form_shortcode  ){ ?>
                    <div class="post-item class-form">
                        <?php echo $form_heading ? sprintf( '<h2>%s</h2>', $form_heading ) : ''; ?>
                        <div class="form">
                            <?php echo do_shortcode( $form_shortcode ); ?>
                        </div>
                    </div>
                <?php } ?>

                <?php the_content(); ?>
            </main>
        </div>
    </section>

<?php get_footer(); ?>