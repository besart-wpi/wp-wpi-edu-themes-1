<style>
<?php
$departments = get_terms( array( 'taxonomy' => 'department_cat' ) ); 
if( $departments ){
    foreach( $departments as $dept ){
        $colorDay   = get_field( 'accent_color_day', 'department_cat_' . $dept->term_id );
        $colorNight = get_field( 'accent_color_night', 'department_cat_' . $dept->term_id );
        $termSlug = sanitize_title( $dept->slug );
        if( $colorDay ){ ?>
            .section .meta .term-dept-<?php echo $termSlug; ?> { color: <?php echo $colorDay; ?>; }
            .section .meta .term-dept-<?php echo $termSlug; ?> svg path { fill: <?php echo $colorDay; ?>; }
            .department-<?php echo $termSlug; ?> .section.content main a:not(.issue-term) { color: <?php echo $colorDay; ?>; }
            .department-<?php echo $termSlug; ?> .section.content main a:not(.issue-term):hover { color: <?php echo $colorNight; ?>; }
            .department-<?php echo $termSlug; ?> .section.content main ::selection { background-color: <?php echo $colorDay; ?>; }
            .department-<?php echo $termSlug; ?> .section.content main .drop-caps p .dropcap { color: <?php echo $colorDay; ?>; }
            .department-<?php echo $termSlug; ?> .section.content main .drop-caps p .dropcap:after { background-color: <?php echo $colorDay; ?>; }
            .department-<?php echo $termSlug; ?> .section.content main .wp-block-quote { background-color: <?php echo $colorDay; ?>; }
            .department-<?php echo $termSlug; ?> .section.content main .quote-custom blockquote p { color: <?php echo $colorDay; ?>; }

            .theme-night .section .meta .term-dept-<?php echo $termSlug; ?> { color: <?php echo $colorNight; ?>; }
            .theme-night .section .meta .term-dept-<?php echo $termSlug; ?> svg path { fill: <?php echo $colorNight; ?>; }
            .theme-night.department-<?php echo $termSlug; ?> .section.content main a:not(.issue-term) { color: <?php echo $colorNight; ?>; }
            .theme-night.department-<?php echo $termSlug; ?> .section.content main a:not(.issue-term):hover { color: <?php echo $colorDay; ?>; }
            .theme-night.department-<?php echo $termSlug; ?> .section.content main ::selection { background-color: <?php echo $colorNight; ?>; }
            .theme-night.department-<?php echo $termSlug; ?> .section.content main .drop-caps p .dropcap { color: <?php echo $colorNight; ?>; }
            .theme-night.department-<?php echo $termSlug; ?> .section.content main .drop-caps p .dropcap:after { background-color: <?php echo $colorNight; ?>; }
            .theme-night.department-<?php echo $termSlug; ?> .section.content main .wp-block-quote { background-color: <?php echo $colorNight; ?>; }
            .theme-night.department-<?php echo $termSlug; ?> .section.content main .quote-custom blockquote p { color: #ffffff; }
        <?php }
    }
} ?>
</style>