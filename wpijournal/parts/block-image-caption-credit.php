<?php
/**
 * Block Name: Image with Caption and Credit Block
 *
 * This is the template that displays the image with caption and credit block
 */

// create id attribute for specific styling
$id = 'image-caption-credit-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? $block['align'] : '';

$image      = get_field( 'image' );
$caption    = get_field( 'caption' );
$credit     = get_field( 'credit' ); ?>

            <?php if( $image ){ ?>
                <div class="post-item image-caption <?php echo $align_class; ?>" id="<?php echo $id; ?>">
                    <?php echo $image ? sprintf( '<figure>%s</figure>', wp_get_attachment_image( $image['ID'], 'image-credit' ) ) : ''; ?>
                    <?php if( $caption ){ ?>
						<p class="caption">
							<?php echo $caption; ?>
							<?php if( $credit ){ ?>
								<span class="credits">Photography: <?php echo $credit; ?> <svg width="18px" height="16px" viewBox="0 0 18 16" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" class="svg replaced-svg"><path d="M6.7957 0L11.4012 0C11.9883 0 12.5121 0.362109 12.716 0.914063L13.2188 2.25L16.3125 2.25C17.2441 2.25 18 3.00586 18 3.9375L18 14.0625C18 14.9941 17.2441 15.75 16.3125 15.75L1.6875 15.75C0.755859 15.75 0 14.9941 0 14.0625L0 3.9375C0 3.00586 0.755859 2.25 1.6875 2.25L4.78125 2.25L5.21719 1.09336C5.46328 0.435937 6.09258 0 6.7957 0ZM12.048 3.9375L16.3125 3.9375L16.3125 14.0625L1.6875 14.0625L1.6875 3.9375L5.95195 3.9375L6.7957 1.6875L11.2078 1.6875L12.048 3.9375ZM9 13.2188C6.67266 13.2188 4.78125 11.3273 4.78125 9C4.78125 6.67266 6.67266 4.78125 9 4.78125C11.3273 4.78125 13.2188 6.67266 13.2188 9C13.2188 11.3273 11.3273 13.2188 9 13.2188ZM6.46875 9C6.46875 7.6043 7.6043 6.46875 9 6.46875C10.3957 6.46875 11.5313 7.6043 11.5313 9C11.5313 10.3957 10.3957 11.5313 9 11.5313C7.6043 11.5313 6.46875 10.3957 6.46875 9Z" id="Shape" fill="#000000" fill-rule="evenodd" stroke="none"></path></svg></span>
							<?php } ?>
						</p>
					<?php } ?>
                </div>
            <?php } ?>