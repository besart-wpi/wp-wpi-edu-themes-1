<?php
    $other_stories = get_field( 'other_stories' );
    if( $other_stories ){ ?>
        <section class="section stories">
            <div class="wrap">
                <h2>Other Stories</h2>
                <div class="slider-wrap">
                    <div class="story-arrows"></div>
                    <div class="story-slider">
                        <?php foreach( $other_stories as $story ){
                            $heading            = $story->post_title;
                            $subheading         = $story->post_excerpt ? $story->post_excerpt : wp_trim_words( $story->post_content, 16, '...' );
                            $link               = get_permalink( $story->ID );
                            $image              = false;
                            $youtube_video_id   = false;
                            
                            $hero_slides        = get_field( 'hero_slides', $story->ID );

                            if( $hero_slides ){
                                $image              = $hero_slides[0][ 'image' ];
                                $youtube_video_id   = $hero_slides[0][ 'youtube_video_id' ];
                            } ?>
                            <div class="item">
                                <div>
                                    <figure>
                                        <a href="<?php echo $link; ?>" title="<?php echo $heading; ?>"><span class="sr"><?php echo $heading; ?></span></a>
                                        <?php echo $image ? wp_get_attachment_image( $image[ 'ID' ], 'thumb-highlight' ) : ''; ?>
                                    </figure>
                                    <div class="text">
                                        <?php
                                        $department = get_the_terms( $story->ID, 'department_cat' );
                                        $issue = get_the_terms( $story->ID, 'issue_cat' );
                                        if( $issue || $department ){ ?>
                                            <div class="meta">
                                                <?php if( $department ){ ?>
                                                    <a href="<?php echo get_term_link( $department[0]->slug, 'department_cat' ) ?>" title="<?php echo $department[0]->name; ?>" class="bracket term-dept-<?php echo sanitize_key( $department[0]->slug ) ?>">
                                                        <span><?php echo $department[0]->name; ?></span>
                                                        <svg width="4px" height="26px" viewBox="0 0 4 26" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg"><path d="M4 25L4 26L0 26L0 0L4 0L4 2L2 2L2 24L4 24L4 25L4 25Z" id="Vector" fill="#000000" stroke="none" /></svg><svg width="4px" height="26px" viewBox="0 0 4 26" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg"><path d="M0 0L4 0L4 26L0 26L0 24L2 24L2 2L0 2L0 0L0 0Z" id="Vector" fill="#000000" stroke="none" /></svg>
                                                    </a>
                                                <?php } ?>

                                                <?php $currentIssue = wpi_journal_current_issue();
                                                if( $issue ){
                                                    if( $issue[0]->term_id == $currentIssue->term_id )
                                                        $issueTitle = 'Current Issue';
                                                    else
                                                        $issueTitle = $issue[0]->name; ?>
                                                    <a href="<?php echo get_term_link( $issue[0]->term_id, 'issue_cat' ); ?>" title="<?php echo $issueTitle; ?>"><?php echo $issueTitle; ?></a>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                        <?php echo $heading ? sprintf( '<h3><a href="%s" title="%s">%s</a></h3>', $link, $heading, $heading ) : ''; ?>
                                        <?php echo $subheading ? sprintf( '<p>%s</p>', $subheading ) : ''; ?>
                                        <a href="<?php echo $link; ?>" class="link-arrow" title="<?php echo $heading; ?>">Read Story</a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    <?php } ?>