    <?php
    $toggle_switch_tooltip = get_field( 'toggle_switch_tooltip', 'option' ); ?>
        <?php if( $toggle_switch_tooltip ){ ?>
            <div class="info">
                <span class="btn-info"><svg width="12px" height="12px" viewBox="0 0 12 12" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg"><path d="M12 6C12 2.68629 9.31371 0 6 0C2.68629 0 0 2.68629 0 6C0 9.31371 2.68629 12 6 12C9.31371 12 12 9.31371 12 6ZM4.75 3.625C4.75 3.00368 5.25368 2.5 5.875 2.5C6.49632 2.5 7 3.00368 7 3.625C7 4.24632 6.49632 4.75 5.875 4.75C5.25368 4.75 4.75 4.24632 4.75 3.625ZM6.625 9.25L6.625 5.5L5.125 5.5L5.125 9.25L6.625 9.25Z" id="Shape" fill="#323237" fill-rule="evenodd" stroke="none" /></svg></span>
                <div class="tip"><?php echo $toggle_switch_tooltip; ?></div>
            </div>
        <?php } ?>
        <div class="toggler-slider">
            <input type="checkbox" value="None" id="toggler-slider" name="toggler-slider" />
            <label for="toggler-slider"><span class="sr">Change Theme</span></label>
            <span></span>
        </div>