<?php get_header(); 
    
    $heading = get_field( '404_heading', 'option' );
    $content = get_field( '404_content', 'option' );
    $image = get_field( '404_image', 'option' ); // attachment ID
    ?>

    <section class="section not-found">
        <div class="wrap">
            <?php if($image): ?>
            <figure><?php echo wp_get_attachment_image( $image, array('579', '464') );  ?></figure>
            <?php endif; ?>
            <div class="text">
                <?php echo $heading ? '<h2>'.esc_html($heading).'</h2>' : ''; ?>
                <?php echo $content; ?>
                <div class="buttons">
                    <a href="#" class="btn pop-search" title="Search WPI Journal">Search WPI Journal</a>
                    <a href="<?php bloginfo( 'url' ); ?>" class="link-arrow">WPI Journal Home</a>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>