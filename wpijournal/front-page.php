<?php get_header(); ?>

    <?php the_post(); ?>
    
    <h1 class="sr-only"><?php the_title(); ?></h1>

<?php
$slides = get_field( 'slides' );
if( $slides ){ ?>
    <section class="section slider">
        <div class="hero-slider">
            <?php foreach( $slides as $slide ){
                
                $image = false;
                $youtube_video_id = false;
                
                $slide_type = $slide[ 'slide_type' ];
                
                if( $slide_type == 'content' ){
                    $department         = $slide[ 'department' ];
                    $heading            = $slide[ 'heading' ];
                    $subheading         = $slide[ 'sub-heading' ];
                    $image              = $slide[ 'image' ];
                    $youtube_video_id   = $slide[ 'youtube_video_id' ];
                    $button             = $slide[ 'button' ];
                    $text_alignment     = $slide[ 'text_alignment' ];
                }else{
                    $article            = $slide[ 'article' ];
                    $heading            = $article->post_title;
                    $subheading         = $article->post_excerpt ? $article->post_excerpt : wp_trim_words( $article->post_content, 16, '...' );
                    $hero_slides        = get_field( 'hero_slides', $article->ID );
                    $text_alignment     = $slide[ 'text_alignment' ] ? $slide[ 'text_alignment' ] : 'left';

                    if( $hero_slides ){
                        $image              = $hero_slides[0][ 'image' ];
                        $youtube_video_id   = $hero_slides[0][ 'youtube_video_id' ];
                    }
                }
                ?>
                <div class="item <?php echo $text_alignment; ?>">
                    <div>
                        <div class="wrap">
                            <div class="caption">
                                <?php if( $slide_type != 'content' ){
                                    $department = get_the_terms( $article->ID, 'department_cat' );
                                    $issue = get_the_terms( $article->ID, 'issue_cat' );
                                    if( $issue || $department ){ ?>
                                        <div class="meta">
                                            <?php if( $department ){ ?>
                                                <a href="<?php echo get_term_link( $department[0]->slug, 'department_cat' ) ?>" title="<?php echo $department[0]->name; ?>" class="bracket term-dept-<?php echo sanitize_key( $department[0]->slug ) ?>">
                                                    <span><?php echo $department[0]->name; ?></span>
                                                    <svg width="4px" height="26px" viewBox="0 0 4 26" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg"><path d="M4 25L4 26L0 26L0 0L4 0L4 2L2 2L2 24L4 24L4 25L4 25Z" id="Vector" fill="#000000" stroke="none" /></svg>
                                                    <svg width="4px" height="26px" viewBox="0 0 4 26" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg"><path d="M0 0L4 0L4 26L0 26L0 24L2 24L2 2L0 2L0 0L0 0Z" id="Vector" fill="#000000" stroke="none" /></svg>
                                                </a>
                                            <?php } ?>

                                            <?php $currentIssue = wpi_journal_current_issue();
                                            if( $issue ){
                                                if( $issue[0]->term_id == $currentIssue->term_id )
                                                    $issueTitle = 'Current Issue';
                                                else
                                                    $issueTitle = $issue[0]->name; ?>
                                                <a href="<?php echo get_term_link( $issue[0]->term_id, 'issue_cat' ); ?>" title="<?php echo $issueTitle; ?>"><?php echo $issueTitle; ?></a>
                                            <?php } ?>
                                        </div>
                                    <?php }
                                } ?>
                                <?php if( $slide_type == 'content' && $department ){ ?>
                                    <div class="meta">
                                       <a href="<?php echo get_term_link( $department->slug, 'department_cat' ) ?>" title="<?php echo $department->name; ?>" class="bracket term-dept-<?php echo sanitize_key( $department->slug ) ?>">
                                            <span><?php echo $department->name; ?></span>
                                            <svg width="4px" height="26px" viewBox="0 0 4 26" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg"><path d="M4 25L4 26L0 26L0 0L4 0L4 2L2 2L2 24L4 24L4 25L4 25Z" id="Vector" fill="#000000" stroke="none" /></svg>
                                            <svg width="4px" height="26px" viewBox="0 0 4 26" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg"><path d="M0 0L4 0L4 26L0 26L0 24L2 24L2 2L0 2L0 0L0 0Z" id="Vector" fill="#000000" stroke="none" /></svg>
                                        </a>
                                    </div>
                                <?php } ?>
                                <?php echo $heading ? sprintf( '<h2>%s</h2>', $heading ) : ''; ?>
                                <?php echo $subheading ? sprintf( '<p>%s</p>', $subheading ) : ''; ?>
                                <?php if( ($article && ! $youtube_video_id) || ($button && ! $youtube_video_id) ){ ?>
                                    <?php if( $slide_type == 'content' ){ ?>
                                        <a href="<?php echo $button[ 'url' ]; ?>" class="btn arrow light" <?php if( $button[ 'target' ] == '_blank' ){ ?>target="_blank" rel="noopener noreferrer"<?php } ?>><?php echo $button[ 'title' ] ?></a>
                                    <?php }elseif( $article ){ ?>
                                        <a href="<?php echo get_permalink($article); ?>" class="btn arrow light" >Read Story</a>
                                    <?php } ?>
                                <?php }elseif( $youtube_video_id ){ ?>
                                    <a data-fancybox="" href="//www.youtube.com/watch?v=<?php echo $youtube_video_id; ?>" class="play-video"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/play.svg" class="svg" alt="play"></a>
                                <?php } ?>
                            </div>
                        </div>
                        <figure>
                            <?php echo $image ? wp_get_attachment_image( $image['ID'], 'thumb-slider' ) : ''; ?>
                        </figure>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="hero-arrows"><div></div></div>
    </section>
    <section class="section mobile-slider"></section>
<?php } ?>

<?php
$counter = 1;
$highlight_boxes = get_field( 'highlight_boxes' );
if( $highlight_boxes ){
    foreach( $highlight_boxes as $item ){ 
        
        $image = false;
        $youtube_video_id = false;
        
        $box_type = $item[ 'box_type' ];
        
        if( $box_type == 'content' ){
            $department         = $item[ 'department' ];
            $heading            = $item[ 'heading' ];
            $subheading         = $item[ 'sub-heading' ];
            $image              = $item[ 'image' ];
            $youtube_video_id   = $item[ 'youtube_video_id' ];
            $link               = $item[ 'link' ];
        }elseif( $box_type == 'article' ){
            $article            = $item[ 'article' ];
            $heading            = $article->post_title;
            $subheading         = $article->post_excerpt ? $article->post_excerpt : wp_trim_words( $article->post_content, 16, '...' );
            $link               = get_permalink( $article->ID );
            $hero_slides        = get_field( 'hero_slides', $article->ID );

            if( $hero_slides ){
                $image              = $hero_slides[0][ 'image' ];
                $youtube_video_id   = $hero_slides[0][ 'youtube_video_id' ];
            }
        }
        ?>
        <section class="section highlight <?php echo ( $counter % 2 == 0 ) ? 'right' : 'left'; ?>">
            <div class="wrap">
                <?php if( $link && $box_type == 'content' ){
                    $link_url   = $link[ 'url' ];
                    $link_title = $link[ 'title' ];
                }elseif( $link && $box_type == 'article' ){
                    $link_url   = $link;
                    $link_title = $heading;
                } ?>

                <figure>
                    <?php if( $youtube_video_id ){ ?>
                        <a data-fancybox="" href="//www.youtube.com/watch?v=<?php echo $youtube_video_id; ?>" class="play-video"><span class="icon"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/play.svg" class="svg" alt=""></span></a>
                    <?php }elseif( $link ){ ?>
                        <a href="<?php echo $link_url; ?>" title="<?php echo $link_title; ?>"><span class="sr"><?php echo $link_title; ?></span></a>
                    <?php } ?>
                    <?php echo $image ? wp_get_attachment_image( $image[ 'ID' ], 'thumb-highlight' ) : ''; ?>
                </figure>
                <div class="text">
                    <?php
                    if( $box_type == 'article' ){
                        $department = get_the_terms( $article->ID, 'department_cat' );
                        $issue = get_the_terms( $article->ID, 'issue_cat' );
                        if( $issue || $department ){ ?>
                            <div class="meta">
                                <?php if( $department ){ ?>
                                    <a href="<?php echo get_term_link( $department[0]->slug, 'department_cat' ) ?>" title="<?php echo $department[0]->name; ?>" class="bracket term-dept-<?php echo sanitize_key( $department[0]->slug ) ?>">
                                        <span><?php echo $department[0]->name; ?></span>
                                        <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bracket-left.svg" alt="left bracket" class="svg"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bracket-right.svg" alt="right bracket" class="svg">
                                    </a>
                                <?php } ?>

                                <?php $currentIssue = wpi_journal_current_issue();
                                if( $issue ){
                                    if( $issue[0]->term_id == $currentIssue->term_id )
                                        $issueTitle = 'Current Issue';
                                    else
                                        $issueTitle = $issue[0]->name; ?>
                                    <a href="<?php echo get_term_link( $issue[0]->term_id, 'issue_cat' ); ?>" title="<?php echo $issueTitle; ?>"><?php echo $issueTitle; ?></a>
                                <?php } ?>
                            </div>
                        <?php }
                    } ?>
                    
                    <?php if( $box_type == 'content' && $department ){ ?>
                        <div class="meta">
                            <a href="<?php echo get_term_link( $department->slug, 'department_cat' ) ?>" title="<?php echo $department->name; ?>" class="bracket term-dept-<?php echo sanitize_key( $department->slug ) ?>">
                                <span><?php echo $department->name; ?></span>
                                <img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bracket-left.svg" alt="left bracket" class="svg"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/bracket-right.svg" alt="right bracket" class="svg">
                            </a>
                        </div>
                    <?php } ?>
                    <?php echo $heading ? sprintf( '<h2><a href="%s" title="%s">%s</a></h2>', $link_url, $link_title, $heading ) : ''; ?>
                    <?php echo $subheading ? sprintf( '<p>%s</p>', $subheading ) : ''; ?>
                    <?php if( $link ){ ?>
                        <a href="<?php echo $link_url; ?>" title="<?php echo $link_title; ?>" class="link-arrow"><?php echo ( $box_type == 'content' ) ? $link_title : 'Read Story'; ?></a>
                    <?php } ?>
                </div>
            </div>
        </section>
    <?php
        $counter ++;
    }
} ?>

<?php
$photos         = get_field( 'photos' );
$autoplay       = get_field( 'autoplay' );
$autoplay_speed = get_field( 'autoplay_speed' );
if( $photos ){ ?>
    <section class="section gallery">
        <div class="wrap">
            <div class="gal-arrows"></div>
            <div class="gal-slider" data-autoplay="<?php echo $autoplay ? 'true' : 'false'; ?>" data-speed="<?php echo $autoplay_speed; ?>">
                <?php foreach( $photos as $photo ){ ?>
                    <div class="item">
                        <div>
                            <figure>
                                <?php echo $photo[ 'link_url' ] ? sprintf( '<a href="%s" title="%s"><span class="sr">%s</span></a>', $photo[ 'link_url' ], esc_attr( strip_tags( $photo[ 'caption' ] ) ), esc_attr( $photo[ 'caption' ] ) ) : ''; ?>
                                <?php echo $photo[ 'photo' ] ? wp_get_attachment_image( $photo[ 'photo' ][ 'ID' ], 'thumb-gallery' ) : ''; ?>
                            </figure>
                            <div class="caption">
                                <?php echo $photo[ 'caption' ] ? sprintf( '<p>%s</p>', $photo[ 'caption' ] ) : ''; ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
    <section class="section mobile-gallery"></section>
<?php } ?>

    <section class="section socials">
        <div class="wrap share-btn" data-url="<?php bloginfo( 'url' ); ?>" data-title="<?php bloginfo( 'name' ); ?>">
            <a href="<?php the_permalink(); ?>#Share on Facebook" class="btn-facebook" data-id="fb" title="Share on Facebook"><img class="svg" src="<?php bloginfo( 'template_url' ); ?>/assets/images/facebook.svg" alt="Share on Facebook"><span class="sr">Share on Facebook</span></a>
            <a href="<?php the_permalink(); ?>#Share on Twitter" class="btn-twitter" data-id="tw" title="Share on Twitter"><img class="svg" src="<?php bloginfo( 'template_url' ); ?>/assets/images/twitter.svg" alt="Share on Twitter"><span class="sr">Share on Twitter</span></a>
            <a href="<?php the_permalink(); ?>#Copy page URL" class="btn-copy-url" data-clipboard-text="<?php bloginfo( 'url' ); ?>" title="Copy page URL"><span>Copied!</span><img class="svg" src="<?php bloginfo( 'template_url' ); ?>/assets/images/share.svg" alt=""><span class="sr">Copy page URL</span></a>
        </div>
    </section>

<?php get_footer(); ?>