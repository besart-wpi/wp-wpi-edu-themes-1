<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php get_template_part("template-parts/_single-activity-hero");?>

<?php if( trim($post->post_content) !== '' ): ?>
<div class="intro-text bg-color-white">
 	<div class="container relative">
		<?php the_content(); ?>
	</div>
</div>
<?php endif; ?>

<?php 
if( have_rows('flexible_content') ){
  while ( have_rows('flexible_content') ) { 
    the_row();
    $layout = get_row_layout();
    get_template_part("flexible-content/_".$layout);
  }
}
?>

<?php endwhile; endif; ?>

<?php get_footer(); ?>