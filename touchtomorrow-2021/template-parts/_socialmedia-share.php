<ul class="socialmedia-share">
  <li class="label">Share</li>
  <li>
    <a href="javascript:void window.open('http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php echo urlencode(get_the_title());?>', 'facebook_share', 'height=320, width=640, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no');">
      <span class="sr-only">Share on Facebook</span>
      <span class="the-text">Facebook</span>
      <svg
        alt="Share on Facebook"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 50 50"
      >
        <g data-name="Group 9" transform="translate(1 1)">
          <circle
            cx="24"
            cy="24"
            r="24"
            fill="none"
            stroke="#fff"
            stroke-miterlimit="10"
            stroke-width="2"
            opacity=".6"
          />
          <path
            d="M21.747 33v-8.051H19.04V21.75h2.707v-2.531a4.168 4.168 0 011.125-3.112A4.075 4.075 0 0125.86 15a17.413 17.413 0 012.461.141v2.847h-1.687a1.632 1.632 0 00-1.3.422 1.8 1.8 0 00-.281 1.125v2.215h2.987l-.422 3.2h-2.566V33z"
            fill="#fff"
          />
        </g></svg
    ></a>
  </li>
  <li>
    <a href="javascript:void window.open('http://twitter.com/share?url=<?php the_permalink();?>', 'twitter_share', 'height=320, width=640, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no');"
      ><span class="sr-only">Share on Twitter</span>
      <span class="the-text">Twitter</span>
      <svg
        alt="Share on Twitter"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 50 50"
      >
        <g data-name="Group 10" transform="translate(1 1)">
          <circle
            cx="24"
            cy="24"
            r="24"
            fill="none"
            stroke="#fff"
            stroke-miterlimit="10"
            stroke-width="2"
            opacity=".6"
          />
          <path
            d="M31.137 20.621a2.094 2.094 0 01.035.457 10.424 10.424 0 01-1.231 4.887 10.04 10.04 0 01-3.551 3.973 10.143 10.143 0 01-5.73 1.652A10.242 10.242 0 0115 29.938q.387.035.879.035a7.319 7.319 0 004.605-1.582 3.434 3.434 0 01-2.162-.721 3.671 3.671 0 01-1.283-1.81q.352.035.668.035a4.686 4.686 0 00.984-.105 3.719 3.719 0 01-2.109-1.3 3.539 3.539 0 01-.844-2.325v-.07a3.475 3.475 0 001.652.492 4.1 4.1 0 01-1.19-1.333 3.581 3.581 0 01.066-3.621 10.179 10.179 0 003.357 2.742 10.219 10.219 0 004.236 1.125 5.124 5.124 0 01-.07-.844 3.66 3.66 0 01.492-1.846 3.613 3.613 0 011.336-1.353 3.565 3.565 0 011.828-.492 3.522 3.522 0 011.494.316 4.175 4.175 0 011.213.844 7.51 7.51 0 002.356-.879 3.663 3.663 0 01-1.617 2.039A7.93 7.93 0 0033 18.688a8.114 8.114 0 01-1.863 1.933z"
            fill="#fff"
          />
        </g></svg
    ></a>
  </li>
  <li>
    <a class="share-link" data-url="<?php the_permalink();?>" href="#"
      ><span class="sr-only">Share Link</span>
      <span class="the-text">Share Link</span>
      <svg
        alt="Share Link"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 50 50"
      >
        <g data-name="Group 11" transform="translate(1 1)">
          <circle
            cx="24"
            cy="24"
            r="24"
            fill="none"
            stroke="#fff"
            stroke-miterlimit="10"
            stroke-width="2"
            opacity=".6"
          />
          <path
            d="M28.444 26.4a3.585 3.585 0 013.6 3.6 3.585 3.585 0 01-3.6 3.6 3.585 3.585 0 01-3.6-3.6 4.009 4.009 0 01.075-.788l-3.825-2.4a3.478 3.478 0 01-2.25.788 3.472 3.472 0 01-2.55-1.05 3.472 3.472 0 01-1.05-2.55 3.472 3.472 0 011.05-2.55 3.472 3.472 0 012.55-1.05 3.478 3.478 0 012.25.787l3.825-2.4a4.009 4.009 0 01-.075-.787 3.472 3.472 0 011.05-2.55 3.621 3.621 0 015.1 0 3.472 3.472 0 011.05 2.55 3.472 3.472 0 01-1.05 2.55 3.615 3.615 0 01-4.8.262l-3.825 2.4a4.172 4.172 0 010 1.575l3.825 2.4a3.478 3.478 0 012.25-.787z"
            fill="#fff"
          />
        </g></svg
    ></a>
  </li>
</ul>