<?php 
$image = get_field('hero_image');
$title = get_the_title();
$text = get_field('hero_description');
$youtube_url = get_field('youtube_url');
?>
<div class="home-banner overflow-hidden <?php echo (!$image)?'no-hero-image':'';?>">
<?php if($image):?>  
  <div class="gradient-shade"></div>
  <div class="hero-image-wrapper">
    <?php echo wp_get_attachment_image( $image['ID'], 'hero-image-d', false, array( 'class' => 'bg d-none d-xl-block' ) ); ?>
    <?php echo wp_get_attachment_image( $image['ID'], 'hero-image-t', false, array( 'class' => 'bg d-xl-none' ) ); ?>
  </div>
<?php endif;?>

  <div class="caption">
    <div class="container">
      <div class="wrapper">
        <h1 class="the-title"><?php the_title();?></h1>
        <?php if( $text ): ?>
        <p class="the-desc">
          <?php echo $text;?>
        </p>
        <?php endif; ?>

        <?php $terms = get_the_terms( $post->ID, 'age-group' );?>
        <?php if($terms):?>
        <div class="post-meta font2">
          <span class="text-uppercase font-weight-bold">Ages:</span> <?php echo get_terms_links($terms);?>
        </div>
        <?php endif;?>
        
        <?php $terms = get_the_terms( $post->ID, 'activity-category' );?>
        <?php if($terms):?>
        <div class="post-meta font2">
          <span class="text-uppercase font-weight-bold">Category:</span> <?php echo get_terms_links($terms);?>
        </div>
        <?php endif;?>

        <?php $terms = get_the_terms( $post->ID, 'discipline' );?>
        <?php if($terms):?>
        <div class="post-meta font2">
          <span class="text-uppercase font-weight-bold">Disciplines:</span> <?php echo get_terms_links($terms);?>
        </div>
        <?php endif;?>
				
        <?php 
        $sponsor_link = get_field('sponsor_link');
        $sponsor_link_label = get_field('sponsor_link_label') ? get_field('sponsor_link_label') : 'Sponsored by';
				
        if($sponsor_link):?>
          <div class="post-meta font2 spsr-box">
            <span class="text-uppercase font-weight-bold"><?php echo $sponsor_link_label; ?>:</span> <a href="<?php echo $sponsor_link['url'];?>" target="<?php echo $sponsor_link['target'];?>"><?php echo $sponsor_link['title'];?></a>
          </div>
        <?php endif;?>

        <?php if(!empty($youtube_url)):?>
        <a
          href="<?php echo $youtube_url['url'];?>"
          data-fancybox
          class="the-button yellow"
          ><span class="icon-play-triangle"></span> <?php echo $youtube_url['title'];?></a
        >
        <?php endif;?>

        <?php get_template_part("template-parts/_socialmedia-share");?>
      </div>
    </div>
  </div>
  <!-- .caption -->
</div>
<!-- .home-banner -->