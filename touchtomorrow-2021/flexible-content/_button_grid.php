<?php 
$background_color = get_sub_field('background_color');
$buttons = get_sub_field('buttons');
$background_image = get_sub_field('background_image');
$background_image_alignment = get_sub_field('background_image_alignment');
$heading_rule_color = get_sub_field('heading_rule_color');
$heading = get_sub_field('heading');
$content = get_sub_field('content');
$foreground_color = get_sub_field('foreground_color');

if( have_rows('buttons') ): ?>
<div class="home-buttons overflow bg-<?php echo $background_color;?> text-<?php echo $foreground_color;?>">
  <div class="container relative">
    <?php 
    if($background_image){  
      echo wp_get_attachment_image( $background_image['ID'], 'full', false, array( 'class' => 'background-image align-'.$background_image_alignment ) );
    }
    ?>
    <div class="row justify-content-center">
      <div class="col-md-10 col-lg-8 text-center">
        <?php if(!empty($heading_rule_color) && $heading_rule_color!=='none'):?>
          <div class="heading-rule bg-<?php echo $heading_rule_color;?>"></div>
        <?php endif;?>

        <?php if(!empty($heading) && $heading!=='none'):?>
        <h2>
          <?php echo $heading;?>
        </h2>
        <?php endif;?>
      </div>
    </div>
    
    <div class="row">
      <div class="col-12">
        <?php if($content):?>
        <div class="the-content text-center">
          <?php echo $content?>
        </div>
        <?php endif;?>
      </div>
    </div>

    <div class="row justify-content-center">
    <?php while( have_rows('buttons') ): the_row(); ?>
      <?php 
      $link = get_sub_field('link'); 
      $foreground_color = get_sub_field('foreground_color'); 
      $background_color = get_sub_field('background_color'); 
      ?>
      <div class="col-button col-md-4">
        <a href="<?php echo $link['url'];?>" target="<?php echo $link['target'];?>" class="the-button <?php echo $background_color;?> solid fg-<?php echo $foreground_color;?>"
          ><?php echo $link['title'];?></a
        >
      </div>
    <?php endwhile; ?>
    </div>
  </div>
</div>
<!-- .home-buttons -->
<?php endif; ?>