<?php 
$slides = get_sub_field('slides');
$background_color = get_sub_field('background_color');
$foreground_color = get_sub_field('foreground_color');
$background_style = get_sub_field('background_style');
if($slides):
?>
<div class="home-carousel overflow <?php echo( $background_style=='single-color' ) ? 'bg-'.$background_color : '';?> <?php echo( $foreground_color=='white' ) ? 'fg-white' : '';?>">
  <span class="bg bg-<?php echo $background_color;?>"></span>

  <button type="button" class="slick-prev-slide">
    <span class="sr-only">Previous Slide</span>
    <span class="icon-angle-left"></span>
  </button>
  <button type="button" class="slick-next-slide">
    <span class="sr-only">Next Slide</span>
    <span class="icon-angle-right"></span>
  </button>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-7 order-md-2 image-side">
        <div class="the-carousel carousel-thumb">
        <?php foreach($slides as $slide):?>
          <?php 
          extract($slide);
          ?>
          <div class="the-slide">
            <?php if(!empty($youtube_url)):?>
            <a
              href="<?php echo $youtube_url;?>"
              data-fancybox
              class="the-thumb"
            >
              <?php echo wp_get_attachment_image( $image['ID'], 'carousel-image-d', false, array( 'class' => 'bg' ) ); ?>
              <span
                class="overlay d-flex align-items-center justify-content-center"
              >
                <span class="play-button"
                  ><span class="icon-play-triangle"></span
                ></span>
              </span>
            </a>
            <?php else:?>
            <div
              class="the-thumb"
            >
              <?php echo wp_get_attachment_image( $image['ID'], 'carousel-image-d', false, array( 'class' => 'bg' ) ); ?>
            </div>
            <?php endif;?>
          </div>
        <?php endforeach;?>
        </div>
        <!-- the-carousel carousel-thumb -->
      </div>
      <div class="col-lg-4 col-md-5 order-md-1 text-side">
        <div
          class="text-carousel-wrapper d-md-flex align-items-md-center flex-md-wrap"
        >
          <div class="the-carousel carousel-text">
          <?php foreach($slides as $slide):?>
            <?php 
            extract($slide);
            ?>
            <div class="the-slide">
              <div class="the-title"><?php echo $heading;?></div>
              <div class="the-desc"><?php echo $text;?></div>
              <?php if($links):?>
              <ul class="links">
              <?php foreach($links as $link):?>
                <li>
                  <a href="<?php echo $link['link']['url'];?>" target="<?php echo $link['link']['target'];?>"
                    ><span class="the-text"><?php echo $link['link']['title'];?></span>
                    <span class="icon-angle-right"></span
                  ></a>
                </li>
              <?php endforeach;?>
              </ul>
              <?php endif;?>
            </div>
            <!-- .the-slide -->
          <?php endforeach;?>
          </div>
          <!-- the-carousel carousel-text -->
        </div>
        <!-- .text-carousel-wrapper -->
      </div>
    </div>
  </div>
</div>
<!-- .home-carousel -->
<?php 
endif;?>