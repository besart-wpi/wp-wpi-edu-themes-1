<?php 
$columns = get_sub_field('columns');
$heading = get_sub_field('heading');
$heading_rule_color = get_sub_field('heading_rule_color');
$background_color = get_sub_field('background_color');
if(empty($background_color)) $background_color = 'white';
$background_image = get_sub_field('background_image');

$column_1 = get_sub_field('column_1');
if($columns>=2){
  $column_2 = get_sub_field('column_2');
}
if($columns>=3){
  $column_3 = get_sub_field('column_3');
}
?>
<div class="intro-text overflow bg-color-<?php echo $background_color;?>">
  <div class="container relative">
    <?php 
    if($background_image){  
      $background_image_alignment = get_sub_field('background_image_alignment');
      echo wp_get_attachment_image( $background_image['ID'], 'full', false, array( 'class' => 'background-image align-'.$background_image_alignment ) );
    }
    ?>
    <div class="row justify-content-center">
      <div class="col-md-10 col-lg-8 text-center">
        <?php if(!empty($heading_rule_color) && $heading_rule_color!=='none'):?>
          <div class="heading-rule bg-<?php echo $heading_rule_color;?>"></div>
        <?php endif;?>
        <?php if(!empty($heading)):?>
					<h2><?php echo $heading;?></h2>
        <?php endif; ?>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <?php if($columns==1):?>
        <div class="the-content column-<?php echo $columns;?> text-center">
          <?php echo $column_1?>
        </div>
        <?php endif;?>

        <?php if($columns==2):?>
        <div class="the-content">
          <div class="row">
            <div class="col-content col-md-6"><?php echo remove_final_nbsp($column_1);?></div>
            <div class="col-content col-md-6"><?php echo remove_final_nbsp($column_2);?></div>
          </div>
        </div>
        <?php endif;?>

        <?php if($columns==3):?>
        <div class="the-content">
          <div class="row">
            <div class="col-content col-md-4"><?php echo remove_final_nbsp($column_1);?></div>
            <div class="col-content col-md-4"><?php echo remove_final_nbsp($column_2);?></div>
            <div class="col-content col-md-4"><?php echo remove_final_nbsp($column_3);?></div>
          </div>
        </div>
        <?php endif;?>        
      </div>
    </div>
  </div>
</div>
<!-- .intro-text -->