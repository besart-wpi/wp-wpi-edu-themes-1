<?php 
$foreground_color = get_sub_field('foreground_color');
$background_color = get_sub_field('background_color');
if( have_rows('social_media_icons') ): ?>
<div class="socialmedia-links bg-<?php echo $background_color;?>">
  <div class="container">
    <ul>
    <?php while( have_rows('social_media_icons') ): the_row(); ?>
      <?php 
      $link = get_sub_field('link');
      $icon = get_sub_field('icon');
      ?>
      <li>
        <a class="text-navy-blue text-<?php echo $foreground_color;?>" href="<?php echo $link['url'];?>" target="<?php echo $link['target'];?>"><?php echo wp_get_attachment_image( $icon['ID'], 'full', false, array( 'class' => '' ) ); ?><?php echo $link['title'];?></a>
      </li>
    <?php endwhile;?>
    </ul>
  </div>
</div>
<!-- .socialmedia-links -->
<?php endif; ?>