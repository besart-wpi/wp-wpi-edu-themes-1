<?php 
if( have_rows('flexible_content') ){
  if(!has_subhero()){
    get_template_part("template-parts/_page-hero");  
  }

  while ( have_rows('flexible_content') ) { 
    the_row();
    $layout = get_row_layout();
    get_template_part("flexible-content/_".$layout);
  }
}
else{
  get_template_part("template-parts/_page-hero");
}
?>
