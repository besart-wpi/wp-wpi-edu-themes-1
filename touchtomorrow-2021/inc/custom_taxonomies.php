<?php 
// Register Custom Taxonomy
function custom_taxonomy() {
  // Age group taxonomy
	$age_group_labels = array(
		'name'                       => _x( 'Age Groups', 'Age Group', 'wpmix' ),
		'singular_name'              => _x( 'Age Group', 'Age Group', 'wpmix' ),
		'menu_name'                  => __( 'Age Group', 'wpmix' ),
		'all_items'                  => __( 'All Age Groups', 'wpmix' ),
		'parent_item'                => __( 'Parent Age Group', 'wpmix' ),
		'parent_item_colon'          => __( 'Parent Age Group:', 'wpmix' ),
		'new_item_name'              => __( 'New Age Group Name', 'wpmix' ),
		'add_new_item'               => __( 'Add New Age Group', 'wpmix' ),
		'edit_item'                  => __( 'Edit Age Group', 'wpmix' ),
		'update_item'                => __( 'Update Age Group', 'wpmix' ),
		'view_item'                  => __( 'View Age Group', 'wpmix' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'wpmix' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'wpmix' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'wpmix' ),
		'popular_items'              => __( 'Popular Age Groups', 'wpmix' ),
		'search_items'               => __( 'Search Age Groups', 'wpmix' ),
		'not_found'                  => __( 'Not Found', 'wpmix' ),
		'no_terms'                   => __( 'No items', 'wpmix' ),
		'items_list'                 => __( 'Age Groups list', 'wpmix' ),
		'items_list_navigation'      => __( 'Age Groups list', 'wpmix' ),
	);
	$age_group_args = array(
		'labels'                     => $age_group_labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'age-group', array( 'activity' ), $age_group_args );
  
  
  // Activity Category taxonomy
	$activity_category_labels = array(
		'name'                       => _x( 'Activity Categories', 'Activity Category', 'wpmix' ),
		'singular_name'              => _x( 'Activity Category', 'Activity Category', 'wpmix' ),
		'menu_name'                  => __( 'Activity Category', 'wpmix' ),
		'all_items'                  => __( 'All Activity Categories', 'wpmix' ),
		'parent_item'                => __( 'Parent Activity Category', 'wpmix' ),
		'parent_item_colon'          => __( 'Parent Activity Category:', 'wpmix' ),
		'new_item_name'              => __( 'New Activity Category Name', 'wpmix' ),
		'add_new_item'               => __( 'Add New Activity Category', 'wpmix' ),
		'edit_item'                  => __( 'Edit Activity Category', 'wpmix' ),
		'update_item'                => __( 'Update Activity Category', 'wpmix' ),
		'view_item'                  => __( 'View Activity Category', 'wpmix' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'wpmix' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'wpmix' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'wpmix' ),
		'popular_items'              => __( 'Popular Activity Categories', 'wpmix' ),
		'search_items'               => __( 'Search Activity Categories', 'wpmix' ),
		'not_found'                  => __( 'Not Found', 'wpmix' ),
		'no_terms'                   => __( 'No items', 'wpmix' ),
		'items_list'                 => __( 'Activity Categories list', 'wpmix' ),
		'items_list_navigation'      => __( 'Activity Categories list', 'wpmix' ),
	);
	$activity_category_args = array(
		'labels'                     => $activity_category_labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'activity-category', array( 'activity' ), $activity_category_args );
  
  
  // Discipline taxonomy
	$discipline_labels = array(
		'name'                       => _x( 'Disciplines', 'Discipline', 'wpmix' ),
		'singular_name'              => _x( 'Discipline', 'Discipline', 'wpmix' ),
		'menu_name'                  => __( 'Discipline', 'wpmix' ),
		'all_items'                  => __( 'All Disciplines', 'wpmix' ),
		'parent_item'                => __( 'Parent Discipline', 'wpmix' ),
		'parent_item_colon'          => __( 'Parent Discipline:', 'wpmix' ),
		'new_item_name'              => __( 'New Discipline Name', 'wpmix' ),
		'add_new_item'               => __( 'Add New Discipline', 'wpmix' ),
		'edit_item'                  => __( 'Edit Discipline', 'wpmix' ),
		'update_item'                => __( 'Update Discipline', 'wpmix' ),
		'view_item'                  => __( 'View Discipline', 'wpmix' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'wpmix' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'wpmix' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'wpmix' ),
		'popular_items'              => __( 'Popular Disciplines', 'wpmix' ),
		'search_items'               => __( 'Search Disciplines', 'wpmix' ),
		'not_found'                  => __( 'Not Found', 'wpmix' ),
		'no_terms'                   => __( 'No items', 'wpmix' ),
		'items_list'                 => __( 'Disciplines list', 'wpmix' ),
		'items_list_navigation'      => __( 'Disciplines list', 'wpmix' ),
	);
	$discipline_args = array(
		'labels'                     => $discipline_labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'discipline', array( 'activity' ), $discipline_args );
  
  
  // Section taxonomy
	$section_labels = array(
		'name'                       => _x( 'Sections', 'Section', 'wpmix' ),
		'singular_name'              => _x( 'Section', 'Section', 'wpmix' ),
		'menu_name'                  => __( 'Section', 'wpmix' ),
		'all_items'                  => __( 'All Sections', 'wpmix' ),
		'parent_item'                => __( 'Parent Section', 'wpmix' ),
		'parent_item_colon'          => __( 'Parent Section:', 'wpmix' ),
		'new_item_name'              => __( 'New Section Name', 'wpmix' ),
		'add_new_item'               => __( 'Add New Section', 'wpmix' ),
		'edit_item'                  => __( 'Edit Section', 'wpmix' ),
		'update_item'                => __( 'Update Section', 'wpmix' ),
		'view_item'                  => __( 'View Section', 'wpmix' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'wpmix' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'wpmix' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'wpmix' ),
		'popular_items'              => __( 'Popular Sections', 'wpmix' ),
		'search_items'               => __( 'Search Sections', 'wpmix' ),
		'not_found'                  => __( 'Not Found', 'wpmix' ),
		'no_terms'                   => __( 'No items', 'wpmix' ),
		'items_list'                 => __( 'Sections list', 'wpmix' ),
		'items_list_navigation'      => __( 'Sections list', 'wpmix' ),
	);
	$section_args = array(
		'labels'                     => $section_labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_quick_edit'				 => false,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
		'meta_box_cb'								 => false,
	);
	register_taxonomy( 'section', array( 'activity' ), $section_args );

}
add_action( 'init', 'custom_taxonomy', 0 );