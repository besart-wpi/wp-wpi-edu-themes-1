<?php
// ========== Run shortcodes from widget ==========
add_filter( 'widget_text', 'do_shortcode' );



// ========== Format page title ==========
function wpmix_format_page_title( $title, $sep ) {
    global $paged, $page;

    if ( is_feed() )
        return $title;

    // Add the site name.
    $title .= get_bloginfo( 'name' );

    // Add the site description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) )
        $title = "$title $sep $site_description";

    // Add a page number if necessary.
    if ( $paged >= 2 || $page >= 2 )
        $title = "$title $sep " . sprintf( __( 'Page %s', 'wpmix' ), max( $paged, $page ) );

    return $title;
}
add_filter( 'wp_title', 'wpmix_format_page_title', 10, 2 );



// ========== Sort Activity Taxonomies Manually (menu_order) ==========
add_filter('pre_get_posts', 'tt_sort_activities_taxonomy_archives');
function tt_sort_activities_taxonomy_archives($query){
    if ( !is_admin() && $query->is_tax( array ( 'age-group', 'activity-category', 'discipline', 'section' ) ) && $query->is_main_query() ) {
        $query->set('orderby', 'menu_order');
        $query->set('order', 'ASC');
    }
    return $query;
}



// ========== Remove empty paragraphs from shortcode string ==========
function wpmix_shortcode_empty_paragraph_fix( $content ) {
    $array = array (
        '<p>[' => '[',
        ']</p>' => ']',
        ']<br />' => ']'
    );

    $content = strtr( $content, $array );

    return $content;
}
add_filter( 'the_content', 'wpmix_shortcode_empty_paragraph_fix' );



// ========== Remove P tags from images ==========
function filter_ptags_on_images($content) {
    $content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
    return preg_replace('/<p>\s*(<iframe .*>*.<\/iframe>)\s*<\/p>/iU', '\1', $content);
}
add_filter('acf_the_content', 'filter_ptags_on_images');
add_filter('the_content', 'filter_ptags_on_images');



// ========== Add CSS class to next/prev post links ==========
function wpmix_next_posts_link_attributes() {
    return 'class="older-posts the-button"';
}
add_filter( 'next_posts_link_attributes', 'wpmix_next_posts_link_attributes' );
function wpmix_previous_posts_link_attributes() {
    return 'class="newer-posts the-button"';
}
add_filter( 'previous_posts_link_attributes', 'wpmix_previous_posts_link_attributes' );



// ========== Display image sizes in media uploader ==========
function wpmix_display_image_size_names_muploader( $sizes ) {
    $new_sizes = array();

    $added_sizes = get_intermediate_image_sizes();

    // $added_sizes is an indexed array, therefore need to convert it
    // to associative array, using $value for $key and $value
    foreach( $added_sizes as $key => $value) {
        $new_sizes[$value] = $value;
    }

    // This preserves the labels in $sizes, and merges the two arrays
    $new_sizes = array_merge( $new_sizes, $sizes );

    return $new_sizes;
}
add_filter( 'image_size_names_choose', 'wpmix_display_image_size_names_muploader', 11, 1 );



//========= Remove empty P tag from the end of the content ===========//
function remove_final_nbsp($content){
    return preg_replace('/<p>&nbsp;<\/p>$/', '', $content);
}
add_filter('the_content', 'remove_final_nbsp');
