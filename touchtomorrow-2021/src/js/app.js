import 'bootstrap';
import 'slick-carousel';
import './fancybox/jquery.fancybox.min';

(function ($) {
	// On Dom Ready
	$(function () {
		
		// Toggle mobile menu
		$('.menu-open').on('keydown click', function (e) {
			if ( 
				( e.type == 'keydown' && (e.which === 13 || e.which == 32) ) || // user pressed enter or spacebar
				( e.type === 'click' ) // user clicked
			) {
				e.preventDefault();
				// Menu is open
				if ( $('header').hasClass('menu-active') ) {
					$('.megamenu').stop().slideUp(function(){
						$('header').removeClass('menu-active');
						$('.menu-open').removeClass('is-active');	
					});
				// Menu is closed
				} else {
					$('.megamenu').stop().slideDown(function () {
						$('header').addClass('menu-active');
						$('.menu-open').addClass('is-active');
					});
				}
			}
		});

		// Toggle sub-menu in megamenu
		$('.megamenu ul.the-menu > li.menu-item-has-children > a').append('<button class="children-button"></button>');
		$('.megamenu ul.the-menu > li.menu-item-has-children > a > .children-button').on(
			'click',
			function (e) {
				e.preventDefault();
				$(this).closest('li').toggleClass('active');
				$(this).closest('li').find('> ul.sub-menu').stop().slideToggle();
			}
		);

		// Close megamenu on clicking outside
		$(document).on('click', function (event) {
			var $trigger = $('.menu-open');
			if ( $trigger !== event.target && !$trigger.has(event.target).length && $('header').hasClass('menu-active') ) {
				$('.megamenu').stop().slideUp(function () {
					$('.menu-open').removeClass('is-active');
					$('header').removeClass('menu-active');
				});
			}
		});

		$('.megamenu').on('click', function (e) {
			e.stopPropagation();
		});

		// Copy links / Share link
		var $temp = $('<input>');
		$('.share-link').on('click', function (e) {
			e.preventDefault();
			$('body').append($temp);
			let url = $(this).data('url');
			$temp.val(url).select();
			document.execCommand('copy');
			$temp.remove();
			$(this).find('.the-text').html('Copied');
			setTimeout(() => {
				$(this).find('.the-text').html('Share Link');
			}, 2000);
		});

		// Home Carousel 1
		$('.home-carousel .the-carousel.carousel-text').slick({
			autoplay: false,
			autoplaySpeed: 3000,
			dots: false,
			arrows: false,
			infinite: true,
			speed: 700,
			slidesToShow: 1,
			slidesToScroll: 1,
			adaptiveHeight: false,
			rows: 0,
			asNavFor: '.home-carousel .the-carousel.carousel-thumb',
			fade: true,

			responsive: [
				{
					breakpoint: 579,
					settings: {
						adaptiveHeight: false,
					},
				},
			],
		});
		
		var $homeCarouselThumb = $('.home-carousel .the-carousel.carousel-thumb');
		
		// Reset zoom and opacity on all slides after navigation
		$homeCarouselThumb.on(
			'beforeChange',
			function (event, slick, currentSlide, nextSlide) {
				$('.the-slide .the-thumb', this).css({
					'transform' : 'scale(0.85)',
					'opacity' : '0.6'
				});
			}
		);

		// Zoom and fade in current slide after navigation
		$homeCarouselThumb.on(
			'afterChange',
			function (event, slick, currentSlide, nextSlide) {
				$(this)
					.find('[data-slick-index="' + currentSlide + '"] .the-thumb')
					.css({
						'transform' : 'scale(1)',
						'opacity' : '1'
					});
			}
		);

		// Zoom first slide on init
		$homeCarouselThumb.on('init', function (slick) {
			$('.slick-active .the-thumb', this).css({
				'transform' : 'scale(1)',
				'opacity' : '1'
			});
		});
		
		$homeCarouselThumb.slick({
			autoplay: true,
			autoplaySpeed: 3000,
			dots: true,
			arrows: true,
			speed: 700,
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			adaptiveHeight: true,
			rows: 0,
			prevArrow: '.home-carousel .slick-prev-slide',
			nextArrow: '.home-carousel .slick-next-slide',
			lazyLoad: 'ondemand',
			asNavFor: '.home-carousel .the-carousel.carousel-text',
			variableWidth: true,
			responsive: [
				{
					breakpoint: 994,
					settings: {
						variableWidth: false,
					},
				},
			],
		});
		
		
		// =====================================
		// carousel-gallery Carousel
		// =====================================
		var $gallery_carousel = $('.carousel-gallery .the-carousel');

		// Reset zoom on all slides after navigation
		$gallery_carousel.on(
			'beforeChange',
			function (event, slick, currentSlide, nextSlide) {
				$('.the-slide', this).css('transform', 'scale(0.9)');
			}
		);

		// Zoom current slide after navigation
		$gallery_carousel.on(
			'afterChange',
			function (event, slick, currentSlide, nextSlide) {
				$(this)
					.find('[data-slick-index="' + currentSlide + '"]')
					.css('transform', 'scale(1)');
			}
		);

		// Zoom first slide on init
		$gallery_carousel.on('init', function (slick) {
			$('.slick-active', this).css('transform', 'scale(1)');
		});

		$gallery_carousel.slick({
			autoplay: false,
			dots: true,
			arrows: true,
			infinite: true,
			speed: 700,
			slidesToShow: 1,
			slidesToScroll: 1,
			adaptiveHeight: false,
			rows: 0,
			prevArrow: '.carousel-gallery .slick-prev-slide',
			nextArrow: '.carousel-gallery .slick-next-slide',
			mobileFirst: true,

			responsive: [
				{
					breakpoint: 768,
					settings: {
						centerMode: true,
						variableWidth: true,
					},
				},
			],
		});

	}); // end document ready

	$(window).on('resize orientationchange load', function () {
		setTimeout(() => {
			position_carousel_arrows();
			position_gallery_carousel_arrows();
		}, 500);
	});

	function position_carousel_arrows() {
		// reposition slick arrows
		let carousel_thumb_height = $('.home-carousel .carousel-thumb').first().height();
		let carousel_arrows = $(
			'.home-carousel .slick-prev-slide, .home-carousel .slick-next-slide'
		);
		let screen_width = $(window).width();
		let adjust_spacing = carousel_thumb_height / 2 + 5;

		if (screen_width < 1024) {
			// console.log('carousel_thumb_height', carousel_thumb_height);
			// console.log('adjust_spacing', adjust_spacing);
			carousel_arrows.css({
				top: adjust_spacing + 'px',
				'-webkit-transform': 'none',
				transform: 'none',
			});
		} else {
			carousel_arrows.css({
				top: '50%',
				'-webkit-transform': 'translateY(-50%)',
				transform: 'translateY(-50%)',
			});
		}
	}

	function position_gallery_carousel_arrows() {
		// reposition slick arrows
		let carousel_thumb_height = $('.carousel-gallery .the-image').first().height();
		let carousel_arrows = $('.carousel-gallery .slick-prev-slide, .carousel-gallery .slick-next-slide');
		let screen_width = $(window).width();
		let adjust_spacing = carousel_thumb_height / 2 - 10;

		carousel_arrows.css({
			top: adjust_spacing + 'px',
			'-webkit-transform': 'none',
			transform: 'none',
		});
	}

})(jQuery);

// Footer Robot Animation
var footerBg = document.getElementById('footer');

var leftHover = document.getElementById('frunway__left');
var rightHover = document.getElementById('frunway__right');
var robot = document.getElementById('robot');

leftHover.addEventListener('mouseover', moveLeft, false);
leftHover.addEventListener('mouseout', moveBack, false);

rightHover.addEventListener('mouseover', moveRight, false);
rightHover.addEventListener('mouseout', moveBack, false);

function moveLeft() {
	robot.classList.add('robot__left');
	footerBg.classList.remove('wpi__red');
}

function moveRight() {
	robot.classList.add('robot__right');
	footerBg.classList.add('wpi__red');
}

function moveBack() {
	robot.classList.remove('robot__left');
	robot.classList.remove('robot__right');
	footerBg.classList.remove('wpi__red');
}
