<?php
/*
Template Name: Landing Page
*/

get_header( 'landing' );

// =================================================
// :: Gets the .hero background ::
// -------------------------------------------------
if($featuredImage = get_post_thumbnail_id()){
	$featuredImage = get_post($featuredImage)->guid;
}

?>
	<div class="hero <?php if($featuredImage) echo 'imaged" style="background: url('. $featuredImage . '); background-position: center; background-repeat: no-repeat; background-size: cover; -ms-behavior: url('.get_stylesheet_directory_uri().'/js/backgroundsize.min.htc);'; ?>">
		<header>
			<h1>
				<?php if($featuredImage) echo '<span>' ?>
					<?php the_title() ?>
				<?php if($featuredImage) echo '</span>' ?>
			</h1>
		</header>
	</div>

	<section id="the-content">
		<div id="content">
			<?php //- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
			// Displays either content as full width or with sidebar
			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
			$sidebar = getSidebar('page');
			$hasSidebar = is_active_sidebar('global') || $sidebar;
			$contentWidth = $hasSidebar ? 'c8' : 'c12 aaaaa';
			?>
			<article class="main <?= $contentWidth ?>">
				<?php get_template_part('loop', 'page') ?>
			</article>
			<?php if($hasSidebar): ?>
				<aside class="main c4">
					<?= apply_filters('the_content', $sidebar) ?>
					<?php if($sidebar) echo '<hr>'; ?>
					<?php dynamic_sidebar('global') ?>
				</aside>
			<?php endif; ?>
			<div class="clear"></div>
		</div>
		</div>
	</section>

<?php get_footer() ?>