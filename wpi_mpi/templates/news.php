<?php // Template Name: News
get_header(); 

// =================================================
// :: Gets the .hero background ::
// -------------------------------------------------
if($featuredImage = get_post_thumbnail_id()){
	$featuredImage = get_post($featuredImage)->guid;
}

?>
	<div class="hero <?php if($featuredImage) echo 'imaged" style="background: url('. $featuredImage . '); background-position: center; background-repeat: no-repeat; background-size: cover; -ms-behavior: url('.get_bloginfo('template_url').'/js/backgroundsize.min.htc);'; ?>">
		<header>
			<h1>
				<?php if($featuredImage) echo '<span>' ?>
					<?php the_title() ?>
				<?php if($featuredImage) echo '</span>' ?>
			</h1>
		</header>
	</div>

	<section id="the-content">
		<div id="content">
			<?php //- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
			// Displays either content as full width or with sidebar
			//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
			$sidebar = getSidebar('news');
			$hasSidebar = is_active_sidebar('news') || $sidebar;
			$contentWidth = $hasSidebar ? 'c8' : 'c12';
			?>
			<article class="main <?= $contentWidth ?>">
				<?php //- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
				// Display the upcoming meetings
				//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
				$events = get_post_meta($post->ID, 'Events', true);
				if($events && $events['event']){
					echo '<h2>',Oz::getField('title', 'Events'),'</h2>';
					for($i = 0; $i < $events['event']; $i++): ?>
						<div class="event-block c5">
							<h2 style="color: <?= $events['event-title-color'][$i] ?>"><?= $events['event-title'][$i] ?></h2>
							<div class="content"><?= apply_filters('the_content', $events['event-content'][$i]) ?></div>
						</div>
					<?php endfor;
				}
				echo '<div class="clear"></div>';
				?>

				<?php get_template_part('loop', 'page') ?>
			</article>
			<aside class="main c4">
				<?php if($hasSidebar): ?>
					<?= apply_filters('the_content', $sidebar) ?>
					<?php // if($sidebar) echo '<hr>'; ?>
				<?php endif; ?>

				<?php
				/*
				$quote = get_posts(array(
					'post_type' => 'testimonials',
					'orderby'	=> 'rand',
					'numberposts'=> 1
				));
				if(count($quote)): $quote = $quote[0]; ?>
					<blockquote>
						<div class="meta">
							<?= get_the_post_thumbnail($quote->ID, 'testimonial-small', array('class'=>'photo alignleft')) ?>
							<div><strong><?= $quote->post_title ?></strong></div>
							<?php if($company = Oz::getField('title', 'properties', null, $quote->ID)): ?>
								<div><?= $company ?></div>
							<?php endif; ?>
							<?php if($company = Oz::getField('company', 'properties', null, $quote->ID)): ?>
								<div><?= $company ?></div>
							<?php endif; ?>
						</div>
						<div class="clear"></div>
						<?= apply_filters('the_content', $quote->post_content) ?>
					</blockquote>
				<?php endif; */ ?>
			</aside>
			<div class="clear"></div>
		</div>
		</div>
	</section>

<?php get_footer() ?>