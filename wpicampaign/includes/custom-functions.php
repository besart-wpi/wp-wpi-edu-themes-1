<?php
// =================================================
//	:: Get Top Parent Page ::
// -------------------------------------------------
function theme_get_parent_page($page_id) {	
	if($page_id) {
		$page_parent = get_post($page_id);
		if ($page_parent->post_parent)	{
			$ancestors=$page_parent->ancestors;
			$root=count($ancestors)-1;
			$parent = $ancestors[$root];
		} else {
			$parent = $page_parent->ID;
		}
		return $parent;
	}
}

// =================================================
//	:: Get Image Attributes ::
// -------------------------------------------------
function theme_get_img($attachment, $image_size = NULL, $attr = ''){
	if($attachment && !empty($attachment)) {
		
		if($image_size == NULL) {
			$image_size = 'post-thumbnail';
		}		
		
		// is attachment array or not?
		if ( (array) $attachment !== $attachment ) {
			$attachment_id = $attachment;
		} else {
			$attachment_id = $attachment['ID'];			
		}

		return wp_get_attachment_image($attachment_id, $image_size, false, $attr);
	} else {
		return false;
	}
}


function theme_get_img_attr($attachment_id, $image_size = NULL){
	if($attachment_id):
		if($image_size == NULL) {
			$image_size = 'post-thumbnail';
		}
		
		$img_src = wp_get_attachment_image_url( $attachment_id, $image_size );
		$img_srcset = wp_get_attachment_image_srcset( $attachment_id, $image_size );
		$img_sizes = wp_get_attachment_image_sizes( $attachment_id, $image_size );
		
		if($img_src){			
			return " src=\"{$img_src}\" srcset=\"{$img_srcset}\" sizes=\"{$img_sizes}\" ";			
		}
	endif;		
}

// =================================================
//	:: Get Image By Attachment ID ::
// -------------------------------------------------
function theme_get_src($attachment, $image_size = NULL){
	
	// is attachment array or not?
	if ( (array) $attachment !== $attachment ) {
		$attachment_id = $attachment;
	} else {
		$attachment_id = $attachment['ID'];
	} 
	
	if($attachment_id) {
		if($image_size == NULL) {
			$image_size = 'post-thumbnail';
		}
		$src = wp_get_attachment_image_url($attachment_id, $image_size);
		return $src;
	} else {
		return '';
	}
}

function theme_get_srcset($attachment, $image_size = NULL){
	
	// is attachment array or not?
	if ( (array) $attachment !== $attachment ) {
		$attachment_id = $attachment;
	} else {
		$attachment_id = $attachment['ID'];
	} 
	
	if($attachment_id) {
		if($image_size == NULL) {
			$image_size = 'post-thumbnail';
		}
		$srcset = wp_get_attachment_image_srcset($attachment_id, $image_size);
		return $srcset;
	} else {
		return '';
	}
}


// =================================================
//	:: Get Landing Page ::
// -------------------------------------------------
if (!function_exists( 'get_landing_page' )){
	function get_landing_page($template_name) {
		if(!$template_name) {
			return false;
		}
		
		$pages = get_posts(array(
			'post_type' => 'page',
			'meta_key' => '_wp_page_template',
			'meta_value' => $template_name
		));
		
		if(!empty($pages)){
			return $pages[0]->ID;
		} else {
			return false;
		}		
	}
}


// =================================================
//	:: Get First / Primary Category ::
// -------------------------------------------------
function theme_get_current_cat() {
	global $post;
	$category = get_the_category();
	if ($category) {
		if (class_exists('WPSEO_Primary_Term')) {
			
			// Show the post's 'Primary' category, if this Yoast feature is available, & one is set
			$wpseo_primary_term = new WPSEO_Primary_Term( 'category', get_the_id() );
			$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
			$term = get_term( $wpseo_primary_term );
			if (is_wp_error($term)) { 				
				return $category[0];
			} else { 
				return $term;
			}
			
		} else {
			return $category[0];
		}
	} else {
		return false;
	}
}


// =================================================
//	:: theme_is_has_content ::
// -------------------------------------------------
if (!function_exists( 'theme_is_has_content' )){
	function theme_is_has_content($str = false) {
		
		if(!$str) {
			global $post;
			$str = $post->post_content;
		}
		
		if(trim(str_replace('&nbsp;','',strip_tags($str))) == '') {
			return false;
		} else {
			return true;
		}
	}
}

// =================================================
//	:: the_theme_output ::
// -------------------------------------------------
if (!function_exists( 'the_theme_output' )){
	function the_theme_output( $content, $before = '', $after = '', $echo = true ) {
		if(!$content) {
			return;
		}
		if ( strlen($content) == 0 ) {
			return;
		} 
		$content = $before . $content . $after; 
		if ( $echo ) {
			echo nl2br($content);
		} else {
			return nl2br($content);
		}
	}
}

// =================================================
//	:: get link label ::
// -------------------------------------------------
if (!function_exists( 'get_link_label' )){
	function get_link_label( $label = '', $default_label = 'Learn More' ) {
		$label = trim($label);
		if(!$label) {
			$label = $default_label;
		}		
		return $label;
	}
}



// =================================================
//	:: Open link in new window ::
// -------------------------------------------------
if(!function_exists('open_new_tab')) {
	function open_new_tab($is_new_window, $echo = true) {		
		if($is_new_window) {
			if($echo) {
				echo ' target="_blank" rel="noopener" ';
			} else {
				return ' target="_blank" rel="noopener" ';
			}			
		}
	}
}


// =================================================
//	:: Get Post Object By Slug ::
// -------------------------------------------------
function theme_get_post_by_slug( $slug, $post_type = 'post', $unique = true ){
    $args=array(
        'name' => $slug,
        'post_type' => $post_type,
        'post_status' => 'publish',
        'posts_per_page' => 1
    );
    $my_posts = get_posts( $args );
    if( $my_posts ) {
        if( $unique ){
            return $my_posts[ 0 ];
        }else{
            return $my_posts;
        }
    }
    return false;
}


// =================================================
//	:: acf link ::
// -------------------------------------------------
if(!function_exists('theme_acf_link')) {
	function theme_acf_link($link, $class = '', $echo = true, $wrapper_div_class = '') {
		if(!empty($link)) {
			$link_url = esc_url($link['url']);
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self';			
			$link_class = esc_attr($class);
			$output = '';
			
			if(!$link_title) {
				$link_title = __('Learn More');
			}
			
			if($wrapper_div_class) {
				$output .= '<div class="'.$wrapper_div_class.'">';
			}
			
			$output .= '<a class="'.$link_class.'" href="'.$link_url.'" target="'.esc_attr($link_target).'">'.esc_html($link_title).'</a>';
			
			if($wrapper_div_class) {
				$output .= '</div>';
			}
			if($echo) {
				echo $output;
			} else {
				return $output;
			}			
		}	
	}
}

// =================================================
//	:: acf button ::
// -------------------------------------------------
	if(!function_exists('theme_acf_button')) {
		function theme_acf_button($link, $class = '', $echo = true) {
			theme_acf_link($link, 'button '.$class, $echo);
		}
	}


// =================================================
//	:: Clean Up Phone Number ::
// -------------------------------------------------
	function theme_phone_number_cleanup($phone) {
		return preg_replace('/[^0-9+-]/', '', $phone);
	}
	
	
// =================================================
//	:: Button ::
// -------------------------------------------------
	if(!function_exists('the_theme_button')) {	
		
		function the_theme_button($args) {
			
			$defaults = array(
				'url' => false,
				'label' => false,
				'new_tab' => false,
				'class' => false,
				'echo' => true,
				'icon' => false,
				'before' => '',
				'after' => '',
				'before_label' => '',
				'after_label' => '',
				'style' => false
			);
			
			$r = wp_parse_args( $args, $defaults );

			$btn_url = esc_url($r['url']);
			$btn_label = get_link_label($r['label']);
			if($btn_url) {
				$btn_classname = ($r['style'] == 'text-link') ? 'solo-link ' : 'btn ';
				$btn_classname .= (esc_attr($r['class'])) ? esc_attr($r['class']) : '';
				if($r['icon']) {
					if($r['style'] == 'text-link') {
						$btn_label .= '<i class="icon icon-'.$r['icon'].'"></i>';
					} else {
						$btn_label = '<i class="icon icon-'.$r['icon'].'"></i>' . $btn_label;
					}
				}
				$output = $r['before'].'<a href="'.$btn_url.'" class="'.$btn_classname.'" '.open_new_tab($r['new_tab'], false).'>'.$r['before_label'].$btn_label.$r['after_label'].'</a>'.$r['after'];
				
				if($r['echo']) {
					echo $output;
				} else {
					return $output;
				}			
			} else {
				return false;
			}
		}
	}

// =================================================
//	:: ACF Link Field - Button ::
// -------------------------------------------------
	if(!function_exists('the_theme_link_button')) {	
		
		function the_theme_link_button($link, $args = array()) {
			if(!empty($link)) {			
				
				$btn_args = array(
					'url' => $link['url'],
					'label' => get_link_label($link['title']),
					'new_tab' => $link['target']
				);
				
				$btn_args = array_merge($btn_args, $args);
				
				the_theme_button($btn_args);
						
			} else {
				return false;
			}
		}
	}
	
// =================================================
//	:: Display Flexible Content ::
// -------------------------------------------------
	/**
	 * Loop through and output ACF flexible content blocks for the current page.
	 */
	function theme_display_flexible_contents($field_name, $post_id = false) {
		if(!$post_id) {
			$post_id = get_the_ID();
		}
		if ( have_rows( $field_name, $post_id ) ) :			
			
			while ( have_rows( $field_name, $post_id ) ) : the_row();
				$row_layout = get_row_layout();
				
				if(get_row_index() == 1 && ($row_layout == 'video_hero' || $row_layout == 'interior_hero')):
					continue;
				endif;
				
				get_template_part( 'template-parts/flexible-contents/' . get_row_layout() ); // Template part name MUST match layout ID.
										
			endwhile;
			wp_reset_postdata();
			
		endif;
	}