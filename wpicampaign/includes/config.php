<?php
// =================================================
//	:: Admin Favicon ::
// -------------------------------------------------
function add_favicon() {
	$favicon_url = get_stylesheet_directory_uri() . '/favicon.ico';
	echo '<link rel="shortcut icon" type="image/x-icon" href="' . $favicon_url . '" />';
}
add_action('login_head', 'add_favicon');
add_action('admin_head', 'add_favicon');

// =================================================
//	:: Enqueue Scripts ::
// -------------------------------------------------
	function theme_enqueue_scripts(){	
		
		
		/* -- Enqueue CSS File -- */		
		wp_enqueue_style( 'typekit-fonts', "//use.typekit.net/lcs1oml.css", array(), null );
		
		wp_enqueue_style( 'theme-style', get_template_directory_uri() . '/assets/css/style.css');	
		
		/* -- Enqueue JS File -- */
		wp_enqueue_script('outdated-browser-rework', get_template_directory_uri() . '/assets/js/outdated-browser-rework.js');		
		wp_enqueue_script('jquery');		
		wp_enqueue_script( 'theme-scripts', get_template_directory_uri() . '/assets/js/scripts.min.js', array( 'jquery' ), false, true);		
		wp_enqueue_script( 'theme-main', get_template_directory_uri() . '/assets/js/main.js', array( 'theme-scripts' ), false, true);
		
		/*
		$local_var = array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'nonce' => wp_create_nonce( 'ajax-nonce' ),
		);
		wp_localize_script('theme-main', 'local_var', $local_var );
		*/
		
	}
	add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );


// =================================================
//	:: Theme Configuration ::
// -------------------------------------------------
	function theme_configuration(){

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
		add_theme_support( 'title-tag' );
		add_theme_support('editor-styles');
		add_editor_style('custom-editor-style.css');
		

		/* -------------------------------------------------
		*	Post Thumbnail Config
		* ------------------------------------------------- */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 340, 220, true );		
		add_image_size( 'full-width', 1440, 9999, false );		
		add_image_size( 'secondary-content', 498, 9999, false );		
		add_image_size( 'priority-thumbnail', 560, 350, true );		
		add_image_size( 'grid-block', 640, 420, true );		
		add_image_size( 'small-thumbnail', 200, 136, true );		
		add_image_size( 'headshot', 120, 120, true );		
		
		
		
		
		/* -------------------------------------------------
		*	Register Menus
		* ------------------------------------------------- */
		register_nav_menus( array(
			'primary_menu' => 'Primary',
			'footer_menu' => 'Footer',					
			'copyright_menu' => 'Copyright',					
		));		
		
	}
	add_action( 'after_setup_theme', 'theme_configuration' );
	

// =================================================
//	:: Add a pingback url auto-discovery header for single posts, pages, or attachments
// -------------------------------------------------	
	function theme_pingback_header() {
		if ( is_singular() && pings_open() ) {
			echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
		}
	}
	add_action( 'wp_head', 'theme_pingback_header' );
	
// =================================================
//	:: Register Sidebar ::
// -------------------------------------------------
	function theme_widgets_init() {
		register_sidebar( array(
			'name'          => __('Sidebar Page Template'),
			'id'            => 'sidebar_page',			
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h6 class="widget-title">',
			'after_title'   => '</h6>',
		) );
		
		register_sidebar( array(
			'name'          => __('Single Story'),
			'id'            => 'single_story',			
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h6 class="widget-title">',
			'after_title'   => '</h6>',
		) );
	}
	add_action( 'widgets_init', 'theme_widgets_init' );


// =================================================
//	:: JPEG image compression quality ::
// -------------------------------------------------
	function theme_compression_quality() {
		return 100;
	}
	add_filter( 'jpeg_quality', 'theme_compression_quality' );


// =================================================
//	:: Max Width Image Fix ::
// -------------------------------------------------
	function custom_max_srcset_image_width( $max_width, $size_array ) {
		return 3000;
	}
	add_filter( 'max_srcset_image_width', 'custom_max_srcset_image_width', 10, 2 );

// =================================================
//	:: The Excerpt Filter ::
// -------------------------------------------------
	function theme_custom_excerpt_length( $length ) {
		return 18;
	}
	add_filter( 'excerpt_length', 'theme_custom_excerpt_length', 999 );

	function theme_custom_excerpt_more( $more ) {
		return '<span class="hellip">&hellip;</span>';
	}
	add_filter( 'excerpt_more', 'theme_custom_excerpt_more' );
		


// =================================================
//	:: Strip H1 - H6 Tags for the_excpert() ::
// -------------------------------------------------
	function theme_strip_heading_tags( $excerpt='' ) {
	 
		$raw_excerpt = $excerpt;
		if ( '' == $excerpt ) {
			$excerpt = get_the_content('');
			$excerpt = strip_shortcodes( $excerpt );
			$excerpt = apply_filters('the_content', $excerpt);
			$excerpt = str_replace(']]>', ']]&gt;', $excerpt);
		}
	 
		$regex = '#(<h([1-6])[^>]*>)\s?(.*)?\s?(<\/h\2>)#';
		$excerpt = preg_replace($regex,'', $excerpt);

		$excerpt_length = apply_filters('excerpt_length', 30);
		$excerpt_more = apply_filters('excerpt_more', '&hellip;');
		$excerpt = wp_trim_words( $excerpt, $excerpt_length, $excerpt_more );
		
		return apply_filters('wp_trim_excerpt', preg_replace($regex,'', $excerpt), $raw_excerpt);
	}
	add_filter( 'get_the_excerpt', 'theme_strip_heading_tags', 5);
	
	
	
// =================================================
//	:: Add Button Class to Next/Prev posts link ::
// -------------------------------------------------
	function ep_posts_link_attributes() {
		return 'class="button"';
	}
	add_filter('next_posts_link_attributes', 'ep_posts_link_attributes');
	add_filter('previous_posts_link_attributes', 'ep_posts_link_attributes');
	
// =================================================
//	:: Body Classes ::
// -------------------------------------------------
	function theme_body_classes( $classes ) {
		if(!is_404()):
			$has_hero = false;
			$flex = get_field('flexible_content', get_the_ID());
			if(!empty($flex)) {
				if($flex[0]['acf_fc_layout'] == 'video_hero') {
					$classes[] = 'header-color-white';
					$has_hero = true;
				} elseif($flex[0]['acf_fc_layout'] == 'interior_hero') {
					$classes[] = 'header-color-'.$flex[0]['overlay_color'];
					$has_hero = true;
				}
			}
			
			if(is_singular('story') && !$has_hero) {
				if(has_post_thumbnail(get_the_ID())) {
					$classes[] = 'header-color-white';
				}
			}
		endif;
		return $classes;
	}
	add_filter( 'body_class','theme_body_classes' );
