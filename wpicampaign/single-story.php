<?php
get_header();

if(have_posts()): while(have_posts()): the_post();
	
	get_template_part('template-parts/hero', 'story');
	
	get_template_part('template-parts/content', 'story');
	
	theme_display_flexible_contents('flexible_content');
	
endwhile; endif;

get_footer();