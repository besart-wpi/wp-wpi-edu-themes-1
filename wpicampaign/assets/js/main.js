'use strict';
jQuery(function($){	
	
    /* SVG Inject
	------------------------------------------------ */
    SVGInject(document.querySelectorAll("img.svg-inject"));
    
	/* FitVids
	------------------------------------------------ */
	$('.the-content').fitVids();
	
	/* Campaign Priorities: Set max height
	------------------------------------------------ */
	$('.section-campaign-priorities').each(function(){
		if($('.priority-entry', this).length > 4) {
			let totalHeight = 0;
			$('.priority-entry:nth-child(-n+4)', this).each(function(){
				totalHeight += $(this).outerHeight();
			});
			
			$('.cpr-right', this).css('max-height', totalHeight);
		}
		
	});
	
	/* Mobile Menu
	------------------------------------------------ */
	$('#mobile-menu').appendTo('#header');
	$('#mobile-menu ul li.menu-item-has-children').each(function(){
		$(this).find('> a').after('<span class="mobnav-subarrow"><i class="icon icon-chevron-down"></i></span>');		
	});
	$('.mobnav-subarrow').click(
		function() {
			$(this).find('i').toggleClass('icon-chevron-down icon-chevron-up');			
			$(this).closest('li').find('> ul').slideToggle(100);
			return false;
	});
	$('.nav-toggle').click(function(){
		$('#mobile-menu').toggleClass('expanded');
		return false;
	});
	$(window).click(function() {
		$('#mobile-menu').removeClass('expanded');
	});
	$('#mobile-menu').click(function(event){
		event.stopPropagation();
	});
	
	
	/* Carousels
	------------------------------------------------ */
	var storiesCarousel = new Swiper('.stories-carousel', {
		slidesPerView: 'auto',
		spaceBetween: 18,
		loop: true,
		navigation: {
			nextEl: '.button-next',
			prevEl: '.button-prev',
		},
		breakpoints: {
			0: {
				spaceBetween: 18
			},
			601: {				
				spaceBetween: 30
			}
		}
    });
	
	var storiesCarousel = new Swiper('.events-carousel .swiper-container', {
		slidesPerView: 3,
		spaceBetween: 30,
		loop: false,
		navigation: {
			nextEl: '.button-next',
			prevEl: '.button-prev',
		},
		breakpoints: {
			0: {
				slidesPerView: 'auto',
				spaceBetween: 18
			},
			601: {
				slidesPerView: 2,
				spaceBetween: 30
			},
			1025: {
				slidesPerView: 3,
				spaceBetween: 30
			}
		}
    });
	
	/* Blockquote Fix
	------------------------------------------------ */
	$('blockquote').not('.blockquote-with-photo').append('<div class="tl"></div><div class="bl"></div>');
	
	/* Share Buttons
	------------------------------------------------ */
	$('.share-facebook').click(function(e){
		e.preventDefault();
		var facebookWindow = window.open("https://www.facebook.com/sharer/sharer.php?u=" + encodeURIComponent(window.location.href), "facebook-popup", "height=350,width=600");
		return facebookWindow.focus && facebookWindow.focus(), !1
	});
	$('.share-twitter').click(function(e){
		e.preventDefault();
		var twitterWindow = window.open("https://twitter.com/share?url=" + encodeURIComponent(window.location.href), "twitter-popup", "height=350,width=600");
		return twitterWindow.focus && twitterWindow.focus(), !1
	});
	$('.share-link').click(function(e){
		e.preventDefault();
		var $this = $(this);
		$this.find(".share-url-inline").empty().text(window.location.href);		
		var shareURL = $this.find(".share-url-inline")[0];		
		var range = document.createRange();
		range.selectNode(shareURL), window.getSelection().addRange(range);
		try {
			document.execCommand("copy") && ($this.find(".copied").css("opacity", 1), setTimeout(function() {
			  $this.find(".copied").css("opacity", 0)
			}, 2e3))
		} catch (err) {
			console.log("Oops, unable to copy")
		}
		return window.getSelection().removeAllRanges(), !1		
	});
	
	/* Dot Fix
	------------------------------------------------ */
	function dotFix() {
		$('.vertical-dot').css('top', $('.hero-content').outerHeight());
	}
	dotFix();
	$(window).resize(dotFix);
	
	
});