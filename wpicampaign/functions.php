<?php
include (trailingslashit(get_template_directory()).'includes/acf/acf-config.php');
include (trailingslashit(get_template_directory()).'includes/custom-functions.php');
include (trailingslashit(get_template_directory()).'includes/config.php');
include (trailingslashit(get_template_directory()).'includes/custom-widgets.php');
include (trailingslashit(get_template_directory()).'includes/custom-posts.php');
include (trailingslashit(get_template_directory()).'includes/custom-blocks.php');