<?php
$section_classes = 'fg-color-'.get_sub_field('foreground_color');
$section_classes .= ' bg-color-'.get_sub_field('background_color');
$bg_image = get_sub_field('background_image');
if($bg_image):
	$section_classes .= ' section-with-bg-image';
endif;
?>
<section class="section-campaign-priorities <?php echo $section_classes; ?>">
<?php
if($bg_image):
	echo '<div class="section-bg">'.theme_get_img($bg_image, 'full-width', array('data-object-fit'=>'cover')).'</div>';
endif;
?>
<div class="container">
	<div class="campaign-priority-row">
		<div class="cpr-left font-size-large font-size-mobile-small">
			<?php
			the_theme_output(get_sub_field('heading'), '<h2>', '</h2>');
			the_sub_field('introduction');
			?>
		</div>
		<div class="cpr-right">
			<?php
			if(have_rows('campaign_priorities')):
				while(have_rows('campaign_priorities')): the_row();
					$link_url = get_sub_field('link_url');
					$thumbnail_image = get_sub_field('thumbnail_image');
					$before = ($link_url) ? '<a href="'.$link_url.'">' : '';
					$after = ($link_url) ? '</a>' : '';
				?>
					<div class="priority-entry">
						<?php if($thumbnail_image): ?>
							<figure>
								<?php echo $before.theme_get_img($thumbnail_image, 'priority-thumbnail', array('alt' => get_sub_field('title'))).$after; ?>
							</figure>
						<?php endif; ?>
						<div class="priority-entry-text">
							<?php
							the_theme_output(get_sub_field('title'), '<h5 class="no-resize">'.$before, $after.'</h5>');
							echo wpautop(get_sub_field('excerpt'));
							?>
						</div>
					</div>
				<?php
				endwhile;
			endif;
			?>
		</div>
	</div>
</div>
</section>