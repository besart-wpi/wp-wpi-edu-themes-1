<?php
$bg_color = get_sub_field('background_split_top_color');
$bg_split_bottom_color = get_sub_field('background_split_bottom_color');
if($bg_color == 'none'):
	$bg_color = 'gray';
endif;
$section_classes = 'bg-color-'.$bg_color;
$section_classes .= ' bg-split-bottom-color-'.$bg_split_bottom_color;
$section_classes .= ' top-bg-'.$bg_color;
$section_classes .= ' bottom-bg-'.$bg_split_bottom_color;
?>

<section class="section-campaign-events <?php echo $section_classes; ?>">
<div class="container container-larger">	
	<div class="section-header">
		<?php
		the_theme_output(get_sub_field('heading'), '<h2>', '</h2>');
		$events_url = get_sub_field('view_events_url');
		if($events_url):
			echo '<div class="link-wrap"><a href="'.$events_url.'" class="solo-link">'.__('View Events').'<i class="icon icon-external-link"></i></a></div>';
		endif;
		?>
	</div>

	<?php
	$feed_url = get_sub_field('rss_feed_url');
	$saved_feed_url = get_transient( 'rss_feed_url' );
	if($feed_url != $saved_feed_url):
		set_transient('rss_feed_url', $feed_url, 24 * HOUR_IN_SECONDS);
		delete_transient('events_feed_data');
	endif;
	
	if ( false === ( $events_feed_data = get_transient( 'events_feed_data' ) ) ):			
		$events_feed_data = simplexml_load_file( $feed_url );
		if($events_feed_data):
			$events_feed_data = json_decode(json_encode($events_feed_data),TRUE);
		endif;
		set_transient( 'events_feed_data', $events_feed_data, 24 * HOUR_IN_SECONDS );
	endif;
	?>
	
	<?php if($events_feed_data && isset($events_feed_data['node']) && !empty($events_feed_data['node'])): ?>
	<div class="events-carousel">
		<div class="ec-border">
			<div class="border-tl"></div>
			<div class="border-tr"></div>
			<div class="border-br"></div>
			<div class="border-bl"></div>
		</div>
		<div class="swiper-container">
			<div class="swiper-wrapper">
				<?php foreach($events_feed_data['node'] as $data): ?>
					<?php
					$title = $data['Title'];
					$date = strip_tags($data['Date']);
					$image = $data['Image'];
					
					$s = explode('href="',$title);
					$t = explode('">',$s[1]);
					$url = $t[0];
					
					$title = strip_tags($title, '<em><i>');
					$s = explode(' - ', $date);
					$date = date("d", strtotime($s[0]));
					$month = date("M", strtotime($s[0]));
					$time = $s[1];					
					?>
					<div class="swiper-slide">
						<div class="event-carousel-entry">
							<figure>
								<a href="<?php echo $url; ?>">
									<?php if(!empty($image)): ?>
										<?php
										$image = preg_replace('/(src="[^"]*")/i', '$1 data-object-fit="cover"', $image);
										echo $image;										
										?>
									<?php else: ?>
										<img src="<?php echo get_template_directory_uri(); ?>/assets/images/thumbnail-default.jpg" data-object-fit="cover" alt="Default Image" />
									<?php endif; ?>
								</a>
							</figure>
							<div class="event-carousel-entry-text">
								<h5 class="no-resize no-resize-tablet-only"><?php echo $title; ?></h5>
								<div class="ce-date"><?php echo $month; ?> <span><?php echo $date; ?></span></div>
								<p class="ce-meta"><i class="icon icon-clock"></i><?php echo $time; ?></p>
								<a href="<?php echo $url; ?>" class="solo-link"><?php _e('View Event'); ?><i class="icon icon-external-link"></i></a>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
		
		<div class="button-control button-prev"><i class="icon icon-arrow-left"></i></div>
		<div class="button-control button-next"><i class="icon icon-arrow-right"></i></div>

	</div>
    <?php else: ?>
        <div class="events-carousel">
            <p><?php echo get_sub_field('no_events_message') ? esc_html(get_sub_field('no_events_message')) : 'There are currently no events scheduled.'; ?></p>
        </div>
	<?php endif; ?>

</div>
</section>