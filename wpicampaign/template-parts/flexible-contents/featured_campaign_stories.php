<?php
$bg_classes = ' bg-color-'.get_sub_field('background_color');
$bg_image = get_sub_field('background_image');
if($bg_image):
	$bg_classes .= ' section-with-bg-image';
endif;
?>
<section class="section-featured-campaign-stories">
	<div class="featured-cs-background">
		<div class="featured-cs-bg-left <?php echo $bg_classes; ?>">
			<?php
			if($bg_image):
				echo '<div class="section-bg">'.theme_get_img($bg_image, 'full-width', array('data-object-fit'=>'cover')).'</div>';
			endif;
			?>
		</div>
		<div class="bg-split-top bg-color-<?php the_sub_field('background_split_top_color'); ?>"></div>
		<div class="bg-split-bottom bg-color-<?php the_sub_field('background_split_bottom_color'); ?>"></div>
	</div>
	<div class="container">
		<div class="featured-cs-row">
			<div class="featured-cs-left font-size-large font-size-mobile-small fg-color-<?php the_sub_field('foreground_color'); ?>">
				<?php
				the_theme_output(get_sub_field('heading'), '<h2>', '</h2>');
				the_sub_field('introduction');
				?>
			</div>
			<div class="featured-cs-right">
				<?php
				$args = array(
					'post_type' => 'story',
					'posts_per_page' => 20
				);
				if(get_sub_field('stories_to_display') == 'curated'):
					$args['post__in'] = get_sub_field('curated_stories');
					$args['ignore_sticky_posts'] = true;
					$args['orderby'] = 'post__in';			
				endif;
				$loop = new WP_Query($args);
				if($loop->have_posts()):
				?>
				<div class="swiper-container stories-carousel">
					<div class="swiper-wrapper">
						<?php while($loop->have_posts()): $loop->the_post(); ?>
							<div class="swiper-slide">
								<div class="story-carousel-entry">
									<?php if(has_post_thumbnail()): ?>
										<figure>
											<a href="<?php the_permalink(); ?>">
											<?php the_post_thumbnail('post-thumbnail', array('alt' => get_the_title())); ?>
											</a>
										</figure>
									<?php endif; ?>
									<div class="story-carousel-entry-text">
										<h5 class="no-resize no-resize-tablet-only"><?php the_title(); ?></h5>
										<?php the_excerpt(); ?>
										<a href="<?php the_permalink(); ?>" class="solo-link"><?php _e('Read the Story'); ?></a>
									</div>
								</div>
							</div>
						<?php endwhile; ?>
					</div>					
				</div>
				<div class="arrows-wrap">
					<?php $btn_class = (get_sub_field('background_color') == 'none') ? '' : 'bc-white';	?>
					<div class="button-control button-prev <?php echo $btn_class; ?>"><i class="icon icon-arrow-left"></i></div>
					<div class="button-control button-next <?php echo $btn_class; ?>"><i class="icon icon-arrow-right"></i></div>
				</div>
			
				<?php
				endif;
				wp_reset_postdata();
				?>
			</div>
		</div>
	</div>
</section>