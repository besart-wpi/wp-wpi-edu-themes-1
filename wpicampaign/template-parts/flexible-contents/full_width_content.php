<?php
$section_classes = 'fg-color-'.get_sub_field('foreground_color');
$bg_color = get_sub_field('background_color');
$section_classes .= ' bg-color-'.$bg_color;
$section_classes .= ' top-bg-'.$bg_color;
$section_classes .= ' bottom-bg-'.$bg_color;

$bg_image = get_sub_field('background_image');
if($bg_image):
	$section_classes .= ' section-with-bg-image';
endif;

$secondary_content = get_sub_field('secondary_content');
$section_classes .= ' sc-type-'.$secondary_content;
$section_classes .= ' sc-align-'.get_sub_field('secondary_content_alignment');
?>
<section class="section-full-width-content <?php echo $section_classes; ?>">
<?php
if($bg_image):
	echo '<div class="section-bg">'.theme_get_img($bg_image, 'full-width', array('data-object-fit'=>'cover')).'</div>';
endif;
?>
<div class="container container-smaller">
<div class="swc-wrap clearfix">
	<div class="fwc-top the-content font-size-medium">
		<?php
		the_theme_output(get_sub_field('heading'), '<h2>', '</h2>');
		the_sub_field('introduction');
		?>
	</div>
	
	<?php if($secondary_content !== 'none'): ?>
	
		<?php if($secondary_content == 'image'): ?>
			<div class="fwc-secondary fwc-secondary-image">
				<figure>
					<?php echo theme_get_img(get_sub_field('secondary_content_image'), 'secondary-content'); ?>
				</figure>
			</div>
		<?php elseif($secondary_content == 'list-block'): ?>
			<div class="fwc-secondary fwc-secondary-list-block bg-color-<?php the_sub_field('secondary_content_list_block_background_color'); ?>">
				<?php if(have_rows('secondary_content_list_block_items')): ?>
					<ul>
						<?php while(have_rows('secondary_content_list_block_items')): the_row(); ?>
							<li><?php the_sub_field('item_text'); ?></li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</div>
		<?php endif; ?>	
		
	<?php endif; ?>
	
	<div class="fwc-content the-content">
		<?php the_sub_field('content'); ?>
	</div>
	
</div>	
</div>
</section>