<section class="section-featured-articles">
<div class="container container-smaller">
	
	<div class="section-header">
		<?php
		the_theme_output(get_sub_field('heading'), '<h2>', '</h2>');
		$cta_link = get_sub_field('cta_link');
		$link_args = array(
			'icon' => $cta_link['target'] == '_blank' ? 'external-link' : '',
			'style' => 'text-link',
			'before' => '<div class="link-wrap">',
			'after' => '</div>'
		);
		the_theme_link_button(get_sub_field('cta_link'), $link_args);		
		?>
	</div>
	
	<?php $count_articles = count(get_sub_field('articles')); ?>
	<?php if(have_rows('articles')): ?>
		
		<div class="featured-articles-wrap">
			
			<div class="featured-articles-large">
				<?php while(have_rows('articles')): the_row(); ?>
					<div class="article-large-entry">
						<figure>
							<a href="<?php the_sub_field('url'); ?>" target="<?php echo get_sub_field('open_link_in_new_tab') ? '_blank' : '_self'; ?>">
								<?php
								$image = get_sub_field('image');
								if($image):
									echo theme_get_img($image, 'priority-thumbnail', array('alt' => get_sub_field('title'), 'data-object-fit'=>'cover'));
								else:
								?>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/thumbnail-default.jpg" alt="Default Image" />
								<?php
								endif;
								?>
							</a>
						</figure>
						<div class="article-large-entry-text">
							<h5><a href="<?php the_sub_field('url'); ?>" target="<?php echo get_sub_field('open_link_in_new_tab') ? '_blank' : '_self'; ?>"><?php the_sub_field('title'); ?><?php echo get_sub_field('open_link_in_new_tab') ? '<i class="icon icon-external-link"></i>' : ''; ?></a></h5>
							<?php the_theme_output(get_sub_field('date'), '<p class="meta"><i class="icon icon-calendar"></i>','</p>'); ?>
						</div>
					</div>
				<?php break; endwhile; ?>
			</div>
			
			<?php if($count_articles > 1): ?>
				<div class="featured-articles-small">
					<?php
					while(have_rows('articles')): the_row();
					if(get_row_index() > 1):
					?>
						<div class="article-entry">
							<div class="article-entry-image">
								<figure>
									<a href="<?php the_sub_field('url'); ?>" target="<?php echo get_sub_field('open_link_in_new_tab') ? '_blank' : '_self'; ?>">
										<?php
										$image = get_sub_field('image');
										if($image):
											echo theme_get_img($image, 'small-thumbnail', array('alt' => get_sub_field('title'), 'data-object-fit'=>'cover'));
										else:
										?>
											<img src="<?php echo get_template_directory_uri(); ?>/assets/images/thumbnail-default.jpg" alt="Default Image" />
										<?php
										endif;
										?>
									</a>
								</figure>
							</div>
							<div class="article-entry-text">
								<?php
								the_theme_output(get_sub_field('title'), '<h6>', '</h6>');
								the_theme_output(get_sub_field('date'), '<p class="meta"><i class="icon icon-calendar"></i>','</p>');
								$url = get_sub_field('url');
                                $target = get_sub_field('open_link_in_new_tab') ? '_blank' : '_self';
                                $icon = $target == '_blank' ? '<i class="icon icon-external-link"></i>' : '';
								if($url):
									echo '<a href="'.$url.'" class="solo-link" target="'.$target.'">'.__('Link Off Site').$icon.'</a>';
								endif;
								?>
							</div>
						</div>
					<?php
					endif;
					endwhile;
					?>
				</div>
			<?php endif; ?>
			
		
		</div>
		
	<?php endif; ?>
	
</div>
</section>