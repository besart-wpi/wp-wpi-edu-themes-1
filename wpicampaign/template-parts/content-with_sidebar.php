<section class="section-content-with-sidebar">
<div class="container container-smaller">
	
	<div class="content-main the-content">
		<?php the_content(); ?>
	</div>
	
	<aside class="sidebar">
		<?php dynamic_sidebar('sidebar_page'); ?>	
	</aside>
	
</div>
</section>
