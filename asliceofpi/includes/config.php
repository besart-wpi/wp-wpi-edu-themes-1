<?php
// =================================================
//	:: Admin Favicon ::
// -------------------------------------------------
	function add_favicon() {
		$favicon_url = get_stylesheet_directory_uri() . '/admin-favicon.ico';
		echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';
	}
	add_action('login_head', 'add_favicon');
	add_action('admin_head', 'add_favicon');
	
// =================================================
//	:: Enqueue Scripts ::
// -------------------------------------------------
	function theme_enqueue_scripts(){
		/* -- Enqueue CSS File -- */
		wp_enqueue_style( 'source-sans-pro', '//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300italic,400,400italic,600,600italic,700,700italic' );
		
		wp_enqueue_style( 'theme-style', get_stylesheet_uri());			
		
		/* -- Enqueue JS File -- */
		wp_enqueue_script('jquery');		
		wp_enqueue_script('underscore');		
		
		wp_enqueue_script( 'jquery-floatlabel', get_template_directory_uri() . '/js/floatlabels.js', array( 'jquery' ));		
		wp_enqueue_script( 'jquery-one-page-nav', get_template_directory_uri() . '/js/jquery.nav.js', array( 'jquery' ));		
		wp_enqueue_script( 'jquery-icheck', get_template_directory_uri() . '/js/icheck.min.js', array( 'jquery' ));		
		wp_enqueue_script( 'jquery-fitvids', get_template_directory_uri() . '/js/jquery.fitvids.js', array( 'jquery' ));		
		wp_enqueue_script( 'theme-main', get_template_directory_uri() . '/js/main.js', array( 'jquery' ));		
	}
	add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );
	
	
// =================================================
//	:: Theme Configuration ::
// -------------------------------------------------
	function theme_configuration(){
		
		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );
		
		/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
		add_theme_support( 'title-tag' );		
		
		/* -------------------------------------------------
		*	Post Thumbnail Config
		* ------------------------------------------------- */
			add_theme_support( 'post-thumbnails');
			set_post_thumbnail_size( 200, 150, true );
			add_image_size( 'footer-logo', 175, 60, true );		
			add_image_size( 'thumb-big', 270, 270, true );		
			add_image_size( 'thumb-photo', 215, 215, true );		
					
		
		/* -------------------------------------------------
		*	Register Menus
		* ------------------------------------------------- */
			register_nav_menus( array(
				'offsite_menu' => 'Offsite Menu',				
			));
		
	}	
	add_action( 'after_setup_theme', 'theme_configuration' );


// =================================================
//	:: IE Fix ::
// -------------------------------------------------	
	add_filter( 'wp_head' , 'add_shiv_for_ie' );
	function add_shiv_for_ie() { ?>
		<!--[if lt IE 9]>			
			<script src="<?php bloginfo('template_url'); ?>/js/html5.js"></script>			
			<script src="<?php bloginfo('template_url'); ?>/js/respond.min.js"></script>
		<![endif]-->
		<!--[if lt IE 10]>		
			<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/ie9.css" />
		<![endif]-->
	<?php
	}