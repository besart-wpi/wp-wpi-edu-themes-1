<?php
/**
 * Initialize the custom Theme Options.
 */
add_action( 'admin_init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 *
 * @return    void
 * @since     2.0
 */
function custom_theme_options() {
  
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( ot_settings_id(), array() );
  
  
	$contact_form7_choices =  array(
		array(
			'value'       => '',
			'label'       => __( '-- Choose One --', 'homes-backend' ),
		)
	);
	
	$cf7_forms = get_posts(array('post_type' => 'wpcf7_contact_form', 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => -1));
	if(!empty($cf7_forms)){
		foreach($cf7_forms as $form){
			$contact_form7_choices[] = array(
				'value'       => $form->ID,
				'label'       => $form->post_title,
			);
		}		
	}
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
	$custom_settings = array( 
		'contextual_help' => array(
			'content'       => array( 
			array(
			'id'        => 'option_types_help',
			'title'     => __( 'Option Types', 'asop-backed' ),
			'content'   => '<p>' . __( 'Help content goes here!', 'asop-backed' ) . '</p>'
			)
			),
			'sidebar'       => '<p>' . __( 'Sidebar content goes here!', 'asop-backed' ) . '</p>'
		),
		
		/* ======== Section Settings ========= */
		'sections'        => array(
			
			array(
				'id'          => 'header_options',
				'title'       => __( 'Header Options', 'homes' )
			),
			
			array(
				'id'          => 'footer_options',
				'title'       => __( 'Footer Options', 'asop-backed' )
			),	
		),
		
		/* ======== Fields Settings ========= */
		'settings'        => array(
		
			/* ---- Header Options ------------------------------ */			
			array(
				'id'          => 'wpi_logo_url',
				'label'       => __( 'WPI Logo URL', 'homes' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'header_options',
				'rows'        => '2',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
		
			
			/* ---- Footer Options ------------------------------ */
			array(
				'id'          => 'footer_wpi_logo_image',
				'label'       => __( 'WPI Logo Image', 'asop-backed' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'upload',
				'section'     => 'footer_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => 'ot-upload-attachment-id',
				'condition'   => '',
				'operator'    => 'and'
			),
			array(
				'id'          => 'footer_wpi_logo_url',
				'label'       => __( 'WPI Logo Link URL', 'asop-backed' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'footer_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
			array(
				'id'          => 'footer_address',
				'label'       => __( 'Address', 'asop-backed' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'textarea',
				'section'     => 'footer_options',
				'rows'        => '2',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
			array(
				'id'          => 'footer_contact_link',
				'label'       => __( 'Contact Link', 'asop-backed' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'footer_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
			array(
				'id'          => 'footer_directions_link',
				'label'       => __( 'Directions Link', 'asop-backed' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'footer_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
			array(
				'id'          => 'social_instagram',
				'label'       => __( 'Instagram URL', 'asop-backed' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'footer_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
			array(
				'id'          => 'social_twitter',
				'label'       => __( 'Twitter URL', 'asop-backed' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'footer_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
			array(
				'id'          => 'social_facebook',
				'label'       => __( 'Facebook URL', 'asop-backed' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'footer_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
			array(
				'id'          => 'social_youtube',
				'label'       => __( 'YouTube URL', 'asop-backed' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'footer_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
			array(
				'id'          => 'social_inquire',
				'label'       => __( 'Inquire URL', 'asop-backed' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'footer_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
			array(
				'id'          => 'social_visit',
				'label'       => __( 'Visit URL', 'asop-backed' ),
				'desc'        => '',
				'std'         => '',
				'type'        => 'text',
				'section'     => 'footer_options',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
			
			
		) /* --- End Fields Settings --- */
	);
  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( ot_settings_id() . '_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( ot_settings_id(), $custom_settings ); 
  }
  
}