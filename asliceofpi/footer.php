	<div id="footer">
	
		<div class="footer-bg-left"></div>
		<div class="footer-bg-right"></div>
		
		<div class="clearfix container-footer">
			<div class="footer-left">
			
				<?php
				$footer_wpi_logo_image = ot_get_option('footer_wpi_logo_image');
				if($footer_wpi_logo_image):				
				?>
					<div class="footer-logo">
						<a href="<?php echo esc_url(ot_get_option('footer_wpi_logo_url')); ?>"><img src="<?php echo theme_get_ot_image_url($footer_wpi_logo_image, 'full'); ?>" alt=""></a>
					</div>
				<?php
				endif;
				?>
				
				<div class="footer-address">
					<?php echo wpautop(ot_get_option('footer_address')); ?>					
				</div>
				
				<div class="footer-links">
					<ul class="clearfix">
						<?php if($footer_contact_link = ot_get_option('footer_contact_link')): ?>
							<li><a href="<?php echo esc_url($footer_contact_link); ?>">Contact Us</a></li>
						<?php endif; ?>
						<?php if($footer_directions_link = ot_get_option('footer_directions_link')): ?>
							<li><a href="<?php echo esc_url($footer_directions_link); ?>">Directions</a></li>
						<?php endif; ?>
					</ul>
				</div>
			</div>
			
			<div class="footer-right clearfix">
				<div class="footer-social clearfix">
					<h3>Connect with us!</h3>
					<?php if($social_instagram = ot_get_option('social_instagram')): ?>
						<a href="<?php echo esc_url($social_instagram); ?>" class="fs-link" target="_blank" title="Instagram"><img src="<?php bloginfo('template_url'); ?>/images/icon_instagram.png" alt="Instagram"></a>
					<?php endif; ?>
					<?php if($social_twitter = ot_get_option('social_twitter')): ?>
						<a href="<?php echo esc_url($social_twitter); ?>" class="fs-link" target="_blank" title="Twitter"><img src="<?php bloginfo('template_url'); ?>/images/icon_twitter.png" alt="Twitter"></a>
					<?php endif; ?>
					<?php if($social_facebook = ot_get_option('social_facebook')): ?>
						<a href="<?php echo esc_url($social_facebook); ?>" class="fs-link" target="_blank" title="Facebook"><img src="<?php bloginfo('template_url'); ?>/images/icon_facebook.png" alt="Facebook"></a>
					<?php endif; ?>
					<?php if($social_youtube = ot_get_option('social_youtube')): ?>
						<a href="<?php echo esc_url($social_youtube); ?>" class="fs-link" target="_blank" title="Youtube"><img src="<?php bloginfo('template_url'); ?>/images/icon_youtube.png" alt="Youtube"></a>
					<?php endif; ?>
					<?php if($social_inquire = ot_get_option('social_inquire')): ?>
						<a href="<?php echo esc_url($social_inquire); ?>" class="fs-link" target="_blank" title="Inquire"><img src="<?php bloginfo('template_url'); ?>/images/icon_inquire.png" alt=""><span>Inquire</span></a>
					<?php endif; ?>
					<?php if($social_visit = ot_get_option('social_visit')): ?>
						<a href="<?php echo esc_url($social_visit); ?>" class="fs-link" target="_blank" title="Visit"><img src="<?php bloginfo('template_url'); ?>/images/icon_visit.png" alt=""><span>Visit</span></a>
					<?php endif; ?>
				</div>
			</div>
			
			<div class="clear"></div>
			
			<div class="footer-copyright">
				<p>&copy; <?php echo date('Y'); ?> Worcester Polytechnic Institute. All rights reserved.</p>
			</div>
		</div>
	</div>
	<?php wp_footer(); ?>	
</body>
</html>