<!--[if IE]><![endif]-->
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta http-equiv="x-ua-compatible" content="ie=edge" />
     
	<script src="//use.typekit.net/yyr8xks.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>
    
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />	
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_url'); ?>/favicon.ico" />	
	<?php wp_head(); ?>	
</head>
<body class="home">
	<header id="header" class="clearfix">
	
		<div class="bg-header-left"></div>
		
		<div class="bg-header-right"></div>
		<div class="bg-header-right-bottom"></div>
	
	
		<div class="header-container clearfix">
		
			<div class="header-inner">
			
				<div class="header-inner-inner clearfix">
					
					<div class="header-right">
						<?php $heading_tag = ( is_home() || is_front_page() ) ? 'h1' : 'div'; ?>
						<<?php echo $heading_tag; ?> id="logo">				
							<a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt=""><span class="site-name"><?php bloginfo('name'); ?></span></a>
						</<?php echo $heading_tag; ?>>
					</div>
					
					<div class="header-left">
					
						<div class="header-left-top">
							<div class="wpi-logo">
								<a href="<?php echo esc_url(ot_get_option('wpi_logo_url')); ?>"><img src="<?php bloginfo('template_url'); ?>/images/logo_wpi.png" alt="WPI"></a>
							</div>
							
							<a href="#" id="show-menu"></a>
							
							<nav id="nav">
								<?php
								$args = array(
									'post_type' => 'page',
									'orderby' => 'menu_order',
									'order' => 'ASC',
									'posts_per_page'=> -1
								);
								
								$all_pages = get_posts($args);
								if(!empty($all_pages)):
								$ctr = 1;
								?>
								<ul class="clearfix">
									<?php foreach($all_pages as $the_page): ?>
									<li class="<?php echo ($ctr==1) ? 'current-active-nav' : ''; $ctr++; ?>"><a href="<?php echo esc_url(home_url('/')); ?>#<?php echo $the_page->post_name; ?>"><?php echo $the_page->post_title; ?></a></li>
									<?php endforeach; ?>									
								</ul>
								<?php endif; ?>
							</nav>						
						</div>
						
						<div class="header-left-bottom">
							<nav class="hlb-nav">
								<?php
								$args = array(
									'container'		=> '',
									'theme_location'=> 'offsite_menu',
									'depth'			=> 1,
									'menu_class'	=> 'clearfix'
								);
								wp_nav_menu($args);
								?>								
							</nav>
						</div>						
						
					</div>					
				
				</div>
				
			</div><!-- end .header-inner -->
			
		</div><!-- end .container -->
	</header>