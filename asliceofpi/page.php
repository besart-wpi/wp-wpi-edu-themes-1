<?php get_header(); ?>
	
	<div id="main">
		<?php if(have_posts()): while(have_posts()): the_post(); ?>
			
			<div id="<?php echo $post->post_name; ?>" class="section-find-out-more section-page">
				<div class="container">
				
					<div class="section-title clearfix">
						<div class="st-number">
							<div class="st-number-bg"></div>							
							<div class="st-number-num"></div>
						</div>
						<h2><?php the_title(); ?></h2>
					</div>
					<div class="clear"></div>
					
					<div class="content-find-out-more clearfix the-content">
						<?php the_content(); ?>			
					</div>		
					
				</div>
				
			</div>
			
		<?php endwhile; endif; ?>		
	</div><!-- end #main -->

<?php get_footer();