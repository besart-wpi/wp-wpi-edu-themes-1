<?php 

/* Template Name: Home */

get_header(); ?>

<?php 
$intro_heading = get_meta('intro_heading');
$intro_text = get_meta('intro_text');
$intro_btn_url = get_meta('intro_btn_url');
$intro_btn_label = get_meta('intro_btn_label');
if ( $intro_heading || $intro_text ){ ?>
    <div id="black">
        <div class="wrap">
            <div class="inner">
                    <?php echo $intro_heading ? '<h2>'. $intro_heading .'</h2>' : '' ?>
                    <?php echo $intro_text ? '<p>'. $intro_text .'</p>' : '' ?>
            </div>
            <?php if ( $intro_btn_url ): ?>
                <a href="<?php echo $intro_btn_url ?>" class="btn"><?php echo $intro_btn_label ? $intro_btn_label : 'Click Here' ?></a>
            <?php endif; ?>
        </div>
    </div>	
<?php } ?>

<div id="content">
	<div class="wrap">  
            
	<?php
        
        $text_array = array();
        for( $t=1;$t<=5;$t++ ){
            $text = get_meta( 'grid_text_'.$t );
            if ( empty( $text ) )
                $text_array[] = " ";
            else
                $text_array[] = $text;
        }
        
        
	$profiles = new WP_Query( array('post_type' => 'profile', 'posts_per_page' => '99') );
	if ( $profiles->have_posts() ) :  ?>

		<div class="profiles"></div>
            
        <div class="clear"></div>
		<div class="grid-list">
        <?php 
		
            $x = 0; // count profiles shown
            $l = 0; // logo shown (0=no, 1=yes)
			
            while( $profiles->have_posts() ) : $profiles->the_post(); 
                $degree        = get_meta('prof_degree');
                $department    = get_meta('prof_department');
                $links         = get_meta('prof_links');
                $gm            = get_meta('prof_gm');
                $quote         = get_meta('prof_quote');
                $photo         = get_meta('prof_photo'); // Attachment ID
                $vid_url       = get_meta('prof_vid_url');  
                $video_id      = ''; 
                
                if ( $vid_url ){
                    $video_id = get_youtube_id( $vid_url );
                }

                    ?><div class="item item-profile profile-<?php echo $x; ?>">
                        <?php if (has_post_thumbnail()) { ?>
                                <?php the_post_thumbnail('profile-size'); ?>
                        <?php }else{ ?>
                                <img src="<?php bloginfo('template_url'); ?>/images/no-profile.png" alt="<?php the_title(); ?>" />
                        <?php } ?>
                            <div class="caption">
                                <div class="inner">
                                    <h4><?php the_title() ?></h4>
                                    <span class="title"><?php echo $degree ? $degree.', ' : ''; echo $department ?></span>
                                </div>
                            </div>
                    </div>
                    <section class="profile-full cf" id="prof-<?php echo sanitize_title( get_the_title() ) ?>">
                        <span class="close"></span>
                        <div class="banner">
                            <?php if ( $video_id ): ?>
                                <div class="video-wrap" data-youtube="<?php echo $video_id ?>"><!-- <span class="video-meta"><?php echo youtube_info( $video_id ) ?></span> BB --></div>
                                <!-- <span class="video-meta-mob"><?php echo youtube_info( $video_id ) ?></span> BB -->
                            <?php elseif ($photo): ?>                                            
                                <div class="img"><?php echo wp_get_attachment_image( $photo, 'profile-large-photo' ); ?></div>
                            <?php endif; ?>
                                <div class="text">
                                    <?php if ( $gm ): ?>
                                        <div class="connect"><?php echo $gm ?></div>
                                    <?php endif; ?>
                                    <?php if ( $quote ): ?>
                                        <div class="quote"><span><?php echo $quote ?></span></div>
                                    <?php endif ?>
                                </div>
                        </div><!-- .banner -->
                        <div class="main">
                            <h2 class="name"><?php the_title() ?></h2>
                            <?php if ( $degree || $department ): ?>
                                <h3 class="dept"><?php echo $degree ? $degree.', ' : ''; echo $department ?></h3>
                            <?php endif; ?>
                            <?php the_content() ?>
                        </div><!-- .main -->
                        <?php if ( $links ){ ?>
                            <div class="sidebar">
                                <h4>Featured Links</h4>
                                <ul>
                                    <?php foreach( $links as $link ){ ?>
                                        <li><a href="<?php echo $link['url'] ?>"><?php echo $link['title'] ?></a></li>
                                    <?php } ?>
                                </ul>
                            </div><!-- .sidebar -->
                        <?php } ?>  
                        <span class="close-profile icon-close"></span>  
                    </section><!-- .profile-full -->

                    <?php 
                    
                    if ( $x==3 ) { 
                        ?><div class="item item-<?php echo $x + 1 ?>">
                            <div class="text black">
                                <h5><?php echo $text_array[0] ?></h5>
                            </div>
                        </div><?php 
                    }
                    
					$x++;
					
				endwhile; 

                ?>
            </div><!-- .grid-list -->
    
    <?php endif; wp_reset_query(); ?>
            
	</div>
</div>	
    
<?php get_footer(); ?>