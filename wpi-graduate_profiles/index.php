<?php get_header(); ?>

<?php the_post() ?>

    <div id="black">
            <div class="wrap">
                    <div class="inner">
                            <h2><?php the_title() ?></h2>
                    </div>
            </div>
    </div>
    <div id="content">
            <div class="wrap">  
                    <?php the_content() ?>
            </div>
    </div>

<?php get_footer(); ?>
