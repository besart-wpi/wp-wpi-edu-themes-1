<?php
/**
 * Initialize the options before anything else. 
 */
add_action( 'admin_init', '_custom_theme_options', 1 );

/**
 * Theme Mode demo code of all the available option types.
 *
 * @return    void
 *
 * @access    private
 * @since     2.0
 */
function _custom_theme_options() {

	$saved_settings = get_option( 'option_tree_settings', array() );
  
	$custom_settings = array(
		'sections'        => array(
			array(
				'title'       => 'Header Options',
				'id'          => 'header_options'
			),
			array(
				'title'       => 'Footer Options',
				'id'          => 'footer_options'
			)
		),
		'settings'        => array(
			array(
				'label'       => 'Graduate Programs Logo URL',
				'id'          => 'gp_logo_url',
				'type'        => 'text',
				'desc'        => 'Enter URL',
				'section'     => 'header_options'
			),
			array(
				'label'       => 'WPI Logo URL',
				'id'          => 'wpi_logo_url',
				'type'        => 'text',
				'desc'        => 'Enter URL',
				'section'     => 'header_options'
			),
			array(
				'label'       => 'Contact Information',
				'id'          => 'contact_info',
				'type'        => 'tab',
				'section'     => 'footer_options'
			),
			array(
				'label'       => 'Heading',
				'id'          => 'contact_info_heading',
				'type'        => 'text',
				'desc'        => 'Enter heading',
				'section'     => 'footer_options'
			),
			array(
				'label'       => 'Contact Information',
				'id'          => 'contact_info_text',
				'type'        => 'textarea-simple',
				'rows'        => '5',
				'desc'        => 'Enter information',
				'section'     => 'footer_options'
			),
			array(
				'label'       => 'Connect with Us',
				'id'          => 'connect',
				'type'        => 'tab',
				'section'     => 'footer_options'
			),
			array(
				'label'       => 'Email Address',
				'id'          => 'connect_email',
				'type'        => 'text',
				'desc'        => 'Enter email',
				'section'     => 'footer_options'
			),
			array(
				'label'       => 'Website URL',
				'id'          => 'connect_website',
				'type'        => 'text',
				'desc'        => 'Enter URL',
				'section'     => 'footer_options'
			),
			array(
				'label'       => 'Facebook URL',
				'id'          => 'connect_facebook',
				'type'        => 'text',
				'desc'        => 'Enter URL',
				'section'     => 'footer_options'
			)
		)
	);
  
	$custom_settings = apply_filters( 'option_tree_settings_args', $custom_settings );
  
	if ( $saved_settings !== $custom_settings ) {
		update_option( 'option_tree_settings', $custom_settings ); 
	}
}