<?php

function body_classes($classes) {
		global $wp_query, $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;

		// add a class = to the name of post or page 
		$classes[] = $wp_query->queried_object->post_name;
		
		// if there is no parent ID and it's not a single post page, category page, or 404 page, give it
		// a class of "parent-page"
		if( $wp_query->post->post_parent < 1  && !is_single() && !is_archive() && !is_404() ) {
			$classes[] = 'parent-page';
		}; 
		
		// if the page/post has a parent, it's a child, give it a class of its parent name
		if($wp_query->post->post_parent > 0 ) {
			$parent_title = get_the_title($wp_query->post->post_parent);
			$parent_title = preg_replace('#\s#','-', $parent_title);
			$parent_title = strtolower($parent_title);
			$classes[] = 'parent-pagename-'.$parent_title;
			
			$parent_id = $wp_query->post->post_parent;
			$get_grandparent = get_post($parent_id);
			$classes[] = 'grand-parent-pageid-'.$get_grandparent->post_parent;
		};

		if($is_lynx) $classes[] = 'lynx';
		elseif($is_gecko) $classes[] = 'gecko';
		elseif($is_opera) $classes[] = 'opera';
		elseif($is_NS4) $classes[] = 'ns4';
		elseif($is_safari) $classes[] = 'safari';
		elseif($is_chrome) $classes[] = 'chrome';
		elseif($is_IE) $classes[] = 'ie';
		else $classes[] = 'unknown';


		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		// detecting OS 
		if ( stripos($user_agent, 'windows') !== false ) {
			$classes[] = 'win';
		} elseif ( stripos($user_agent, 'macintosh') !== false ) {
			$classes[] = 'mac';
		}

		// detecting rendering engine
		if ( stripos($user_agent, 'AppleWebKit') !== false ) {
			$classes[] = 'webkit';
		} elseif ( stripos($user_agent, 'Gecko') !== false ) {
			$classes[] = 'gecko';
		} elseif ( stripos($user_agent, 'MSIE') !== false ) {
			$classes[] = 'msie';
		} elseif ( stripos($user_agent, 'presto') !== false ) {
			$classes[] = 'presto';
		}

		// detecting browsers
		if ( stripos($user_agent, 'chrome') !== false ) {
			$classes[] = 'chrome';
		} elseif ( stripos($user_agent, 'safari') !== false ) {
			$classes[] = 'safari';
		} elseif ( stripos($user_agent, 'firefox') !== false ) {
			$classes[] = 'firefox';
		} elseif ( stripos($user_agent, 'opera') !== false ) {
			$classes[] = 'opera';
		}
		
	if(!wp_is_mobile()) {
		if($is_gecko) $classes[] = 'browser-gecko';
		elseif($is_opera) $classes[] = 'browser-opera';
		elseif($is_safari) $classes[] = 'browser-safari';
		elseif($is_chrome) $classes[] = 'browser-chrome';
        elseif($is_IE) {
            $classes[] = 'browser-ie';
            if(preg_match('/MSIE ([0-9]+)([a-zA-Z0-9.]+)/', $_SERVER['HTTP_USER_AGENT'], $browser_version))
            $classes[] = 'ie-version-'.$browser_version[1];
        }
		else $classes[] = 'browser-unknown';
	} else {
    	if(is_iphone()) $classes[] = 'browser-iphone';
        elseif(is_ipad()) $classes[] = 'browser-ipad';
        elseif(is_ipod()) $classes[] = 'browser-ipod';
        elseif(is_android()) $classes[] = 'browser-android';
        elseif(is_tablet()) $classes[] = 'device-tablet';
        elseif(is_mobile_device()) $classes[] = 'device-mobile';
        elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Kindle') !== false) $classes[] = 'browser-kindle';
        elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry') !== false) $classes[] = 'browser-blackberry';
        elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false) $classes[] = 'browser-opera-mini';
        elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mobi') !== false) $classes[] = 'browser-opera-mobi';
	}
	if(strpos($_SERVER['HTTP_USER_AGENT'], 'Windows') !== false) $classes[] = 'os-windows';
        elseif(is_android()) $classes[] = 'os-android';
        elseif(is_ios()) $classes[] = 'os-ios';
        elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Macintosh') !== false) $classes[] = 'os-mac';
        elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Linux') !== false) $classes[] = 'os-linux';
        elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Kindle') !== false) $classes[] = 'os-kindle';
        elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry') !== false) $classes[] = 'os-blackberry';
		
		if(post_password_required()) $classes[] = 'pass-protected';

		return $classes;
		
		return array_unique($classes);
};
add_filter('body_class','body_classes'); 

// add conditional statements for mobile devices
function is_ipad() {
	$is_ipad = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPad');
	if ($is_ipad)
		return true;
	else return false;
}
function is_iphone() {
	$cn_is_iphone = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPhone');
	if ($cn_is_iphone)
		return true;
	else return false;
}
function is_ipod() {
	$cn_is_iphone = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPod');
	if ($cn_is_iphone)
		return true;
	else return false;
}
function is_ios() {
	if (is_iphone() || is_ipad() || is_ipod())
		return true;
	else return false;
}
function is_android() { // detect ALL android devices
	$is_android = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'Android');
	if ($is_android)
		return true;
	else return false;
}
function is_android_mobile() { // detect ALL android devices
	$is_android   = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'Android');
	$is_android_m = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'Mobile');
	if ($is_android && $is_android_m)
		return true;
	else return false;
}
function is_android_tablet() { // detect android tablets
	if (is_android() && !is_android_mobile())
		return true;
	else return false;
}
function is_mobile_device() { // detect ALL mobile devices
	if (is_android_mobile() || is_iphone() || is_ipod())
		return true;
	else return false;
}
function is_tablet() { // detect ALL tablets
	if ((is_android() && !is_android_mobile()) || is_ipad())
		return true;
	else return false;
}
?>