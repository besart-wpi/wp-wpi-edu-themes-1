<?php

add_action( 'admin_init', 'custom_meta_boxes' );

function custom_meta_boxes() {

	$home_introduction = array(
		'id'          => 'home_introduction',
		'title'       => 'Introduction',
		'desc'        => '',
		'pages'       => array( 'page' ),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			array(
				'label'       => 'Heading',
				'id'          => 'intro_heading',
				'type'        => 'text',
				'desc'        => 'Enter heading',
			),
			array(
				'label'       => 'Text',
				'id'          => 'intro_text',
				'type'        => 'textarea-simple',
				'rows'        => '2',
				'desc'        => 'Enter text',
			),
			array(
				'label'       => 'Button Link URL',
				'id'          => 'intro_btn_url',
				'type'        => 'text',
				'desc'        => 'Enter button url',
			),
			array(
				'label'       => 'Button Label',
				'id'          => 'intro_btn_label',
				'type'        => 'text',
				'desc'        => 'Enter button label',
			),
		)
	);

	$home_grid_items = array(
		'id'          => 'home_grid_items',
		'title'       => 'Text Grid Items',
		'desc'        => '',
		'pages'       => array( 'page' ),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			array(
				'label'       => 'Text 1',
				'id'          => 'grid_text_1',
				'type'        => 'text',
				'desc'        => 'Enter text',
			),
			array(
				'label'       => 'Text 2',
				'id'          => 'grid_text_2',
				'type'        => 'text',
				'desc'        => 'Enter text',
			),
			array(
				'label'       => 'Text 3',
				'id'          => 'grid_text_3',
				'type'        => 'text',
				'desc'        => 'Enter text',
			),
			array(
				'label'       => 'Text 4',
				'id'          => 'grid_text_4',
				'type'        => 'text',
				'desc'        => 'Enter text',
			),
			array(
				'label'       => 'Text 5',
				'id'          => 'grid_text_5',
				'type'        => 'text',
				'desc'        => 'Enter text',
			),
		)
	);

	$profile_details_meta = array(
		'id'          => 'profile_details_meta',
		'title'       => 'Profile Details',
		'desc'        => '',
		'pages'       => array( 'profile' ),
		'context'     => 'normal',
		'priority'    => 'high',
		'fields'      => array(
			array(
				'label'       => 'Degree',
				'id'          => 'prof_degree',
				'type'        => 'text',
				'desc'        => 'Enter Degree',
			),
			array(
				'label'       => 'Department',
				'id'          => 'prof_department',
				'type'        => 'text',
				'desc'        => 'Enter Department',
			),
			array(
				'label'       => 'Featured Links',
				'id'          => 'prof_links',
				'type'        => 'list-item',
				'settings'    => array( 
					array(
						'label'       => 'Link URL',
						'id'          => 'url',
						'type'        => 'text',
						'desc'        => 'Enter URL'
					),
				)
                        ),
			array(
				'label'       => 'Great Minds ____',
				'id'          => 'prof_gm',
				'type'        => 'text',
				'desc'        => 'Enter text',
			),
			array(
				'label'       => 'Quote',
				'id'          => 'prof_quote',
				'type'        => 'text',
				'desc'        => 'Enter quote',
			),
			array(
				'label'       => 'Large Photo',
				'id'          => 'prof_photo',
				'type'        => 'upload',
				'desc'        => 'Upload photo',
				'class'       => 'ot-upload-attachment-id',
			),
			array(
				'label'       => 'YouTube Video URL',
				'id'          => 'prof_vid_url',
				'type'        => 'text',
				'desc'        => 'Enter youtube url',
			),
		)
	);


	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'];
	$template_file = get_post_meta($post_id, '_wp_page_template', TRUE);

	if( $template_file == 'front-page.php' ){
		ot_register_meta_box( $home_introduction );	
		ot_register_meta_box( $home_grid_items );
        }	
        
	ot_register_meta_box( $profile_details_meta );	
	
	
}