<div id="slider">
	<ul class="slides">
 
		<?php $loop = new WP_Query(array('post_type' => 'slideshow', 'posts_per_page' => option::get('featured_posts_posts'))); $m = 0; ?>

 		<?php while ( $loop->have_posts() ) : $loop->the_post(); $m++; 
 
			$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'featured');
			$small_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'portfolio-slide');
 
 			$style = ' style="background-image:url(\'' . $small_image_url[0] . '\')" data-bigimg="' . $large_image_url[0] . '"' ;
		?>

			<li<?php echo $style; ?>>

				<div class="li-wrap">
					<?php
					$url = get_post_meta(get_the_ID(), 'wpzoom_slideshow_url', true);
					if ($url != '') { ?><a href="<?php echo $url; ?>" rel="bookmark" title="<?php the_title(); ?>"><?php }

					the_title('<h3>', '</h3>');
					echo '<br/><div class="excerpt">' . strip_tags( get_the_excerpt() ) . '</div>';

					if ($url != '') { ?></a><?php }
					?>
				</div>
 			</li>
		<?php endwhile; ?>
		 
	</ul>

	<?php if ($m == 0) { echo '<div id="content-wrap"><p><strong>You are now ready to set-up your Slideshow content.</strong></p>
	<p>For more information about adding posts to the slider, please <strong><a href="http://www.wpzoom.com/documentation/venture/">read the documentation</a></strong> or <a href="'.home_url().'/wp-admin/post-new.php?post_type=slideshow">add a new post</a>.</p></div>'; } ?>

 </div>

<?php wp_reset_query(); ?>