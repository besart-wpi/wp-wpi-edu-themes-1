<?php

/*------------------------------------------*/
/* WPZOOM: Portfolio Slider Tabs            */
/*------------------------------------------*/

class Wpzoom_Portfolio_Slider_Tabs extends WP_Widget {

	function __construct() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'wpzoom-portfolio-slider-tabs', 'description' => 'A tabbed widget displaying posts from each portfolio category in slider format.' );

		/* Widget control settings. */
		$control_ops = array( 'id_base' => 'wpzoom-portfolio-slider-tabs' );

		/* Create the widget. */
		$this->WP_Widget( 'wpzoom-portfolio-slider-tabs', 'WPZOOM: Portfolio Slider Tabs', $widget_ops, $control_ops );
	}

	function widget( $args, $instance ) {

		extract( $args );

		/* User-selected settings. */
		$title 			= apply_filters('widget_title', $instance['title'] );
		$show_count 	= $instance['show_count'];
		$show_title 	= $instance['hide_title'] ? false : true;
		$show_date 		= $instance['show_date'] ? true : false;
		$exclude = (array) $instance['exclude'];

		$cat_args = array( 'hierarchical' => false, 'orderby' => 'name' );
		if ( count($exclude) > 0 ) $cat_args['exclude'] = $exclude;
		$categories = (array) get_terms( 'skill-type', $cat_args );
		if ( count($categories) < 1 ) return false;

		/* Before widget (defined by themes). */
		echo $before_widget;

		/* Title of widget (before and after defined by themes). */
		if ( $title )
			echo $before_title . $title . $after_title;

		echo '<ul class="pst-nav">';
		$first = true;
		foreach ( $categories as $category ) {
			$selected = $first ? ' class="current"' : '';
			echo '<li data-value="cat-' . esc_attr($category->slug) . '"' . $selected . '><h4>' . esc_html($category->name) . '</h4></li>';
			$first = false;
		}
		echo '</ul>';

		$first = true;
		foreach ( $categories as $category ) {
			query_posts( array(
				'posts_per_page' => $show_count,
				'post_type' => 'portfolio',
				'tax_query' => array(
					array(
						'taxonomy' => 'skill-type',
						'field' => 'id',
						'terms' => $category->term_id
					)
				)
			) );
			if ( have_posts() ) {
				$selected = $first ? ' current' : '';
				echo '<div class="pst-slider cat-' . esc_attr($category->slug) . $selected . '"><ul>';
				while ( have_posts() ) {
					the_post();
					//echo '<li><h4>' . get_the_title() . '</h4></li>';
					echo '<li><a href="' . get_permalink() . '">';
					get_the_image( array( 'size' => 'portfolio-slider-tabs-widget', 'width' => 340, 'link_to_post' => false ) );
					if ( $show_title || $show_date ) {
						echo '<div class="meta">';
						if ( $show_title ) the_title('<h4>', '</h4>');
						if ( $show_date ) printf( __('%s', 'wpzoom'),  get_the_date() );
						echo '</div>';
					}
					echo '</a></li>';
				}
				echo '</ul></div>';
			}
			$first = false;
			wp_reset_query();
		}
		/*return;

		$query_opts = apply_filters('wpzoom_query', array(
			'posts_per_page' => $show_count,
			'post_type' => 'portfolio'
		));
		if ( count($exclude) > 0 ) $query_opts['tax_query'] = array(
				array(
					'taxonomy' => 'skill-type',
					'field' => 'id',
					'terms' => $exclude,
					'operator' => 'NOT IN'
				)
			);

		query_posts($query_opts);
		if ( have_posts() ) : while ( have_posts() ) : the_post();
			echo '<li>';

				if ( $show_thumb ) {
                    get_the_image( array( 'size' => 'recent-widget', 'width' => $instance['thumb_width'], 'height' => $instance['thumb_height'] )   );
                }

				if ( $show_date ) { ?>

					<div class="date">
						<span class="day"><?php the_time('d'); ?></span>
						<span class="month"><?php the_time('M'); ?></span>
					</div>

					<span class="meta"><?php printf( __('%s', 'wpzoom'),  get_the_date()); ?></span>

					<?php }

				if ( $show_title ) echo '<h4><a href="' . get_permalink() . '">' . get_the_title() . '</a></h4>';


				if ( $show_excerpt ) {
					$the_excerpt = get_the_excerpt();

					// cut to character limit
					$the_excerpt = substr( $the_excerpt, 0, $excerpt_length );

					// cut to last space
					$the_excerpt = substr( $the_excerpt, 0, strrpos( $the_excerpt, ' '));

					echo '<span class="post-excerpt">' . $the_excerpt . '</span>';
				}
			echo '<div class="clear"></div></li>';
			endwhile; else:
			endif;

			//Reset query_posts
			wp_reset_query();
		echo '</ul><div class="clear"></div>';*/

		/* After widget (defined by themes). */
		echo $after_widget;
	}


	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags (if needed) and update the widget settings. */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['show_count'] = $new_instance['show_count'];
		$instance['hide_title'] = $new_instance['hide_title'];
		$instance['show_date'] = $new_instance['show_date'];
		$instance['exclude'] = (array) $new_instance['exclude'];

		return $instance;
	}

	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => 'Recent Works', 'show_count' => 4, 'hide_title' => false, 'show_date' => false, 'exclude' => array() );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label><br />
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" type="text" class="widefat" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'show_count' ); ?>">Show:</label>
			<input id="<?php echo $this->get_field_id( 'show_count' ); ?>" name="<?php echo $this->get_field_name( 'show_count' ); ?>" value="<?php echo $instance['show_count']; ?>" type="text" size="2" /> posts <em>(per-tab)</em>
		</p>

		<p>
			<input class="checkbox" type="checkbox" <?php checked( $instance['hide_title'], 'on' ); ?> id="<?php echo $this->get_field_id( 'hide_title' ); ?>" name="<?php echo $this->get_field_name( 'hide_title' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'hide_title' ); ?>">Hide post titles</label>
		</p>

		<p>
			<input class="checkbox" type="checkbox" <?php checked( $instance['show_date'], 'on' ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_date' ); ?>">Display post dates</label>
		</p>
		
		<div>
			<label for="<?php echo $this->get_field_id( 'exclude' ); ?>">Exclude Categories:</label><br />
			<?php
			$categories = (array) get_terms( 'skill-type', array( 'hierarchical' => false, 'orderby' => 'name' ) );
			if ( count($categories) > 0 ) {

				?><select name="<?php echo $this->get_field_name( 'exclude' ); ?>[]" id="<?php echo $this->get_field_id( 'exclude' ); ?>" class="widefat" multiple><?php

					foreach ( $categories as $category ) {
						$selected = in_array( (int) $category->term_id, (array) $instance['exclude'] ) ? ' selected="selected"' : '';
						echo '<option value="' . intval($category->term_id) . '"' . $selected . '>' . esc_attr($category->name) . '</option>';
					}

				?></select><small class="howto">You can select multiple categories</small><?php

			}
			?>
		</div>

		<?php
	}
}

function wpzoom_register_pst_widget() {
	register_widget('Wpzoom_Portfolio_Slider_Tabs');
}
add_action('widgets_init', 'wpzoom_register_pst_widget');