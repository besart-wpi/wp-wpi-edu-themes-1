<?php

/*------------------------------------------*/
/* WPZOOM: Testimonials           			*/
/*------------------------------------------*/
 
class wpzoom_testimonial extends WP_Widget {
	
	function __construct() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'wpzoom-testimonial', 'description' => 'Displays a single post from the testimonial custom post type.' );

		/* Widget control settings. */
		$control_ops = array( 'id_base' => 'wpzoom-testimonial' );

		/* Create the widget. */
		$this->WP_Widget( 'wpzoom-testimonial', 'WPZOOM: Testimonial', $widget_ops, $control_ops );
	}
	
	function widget( $args, $instance ) {
		
		extract( $args );

		/* User-selected settings. */
		$title 	= apply_filters('widget_title', $instance['title'] );		
		$show_photo = $instance['show_photo'];
		$show_author = $instance['show_author'];
		$show_author_position = $instance['show_author_position'];
		$show_author_company = $instance['show_author_company'];
		$show_author_company_link = $instance['show_author_company_link'];
		$random_post = $instance['random_post'];
		
		/* Before widget (defined by themes). */
		echo $before_widget;
		
	
		/* Title of widget (before and after defined by themes). */
	
		if ( $title )
			echo $before_title . $title . $after_title;
 		
		if ($random_post == 'on')
		{
			$orderby = 'rand';
		}
		else
		{
			$orderby = 'date';
		}

		$loop = new WP_Query( array( 'post_type' => 'testimonial', 'posts_per_page' => 1, 'orderby' => $orderby) ); 

		while ( $loop->have_posts() ) : $loop->the_post();
		
		$customFields = get_post_custom();
		
		if ($show_author == 'on') { 
			$testimonial_author = $customFields['wpzoom_testimonial_author'][0]; 
		}
		if ($show_author_position == 'on') { 
			$testimonial_position = $customFields['wpzoom_testimonial_author_position'][0];  
		}
		if ($show_author_company == 'on') { 
			$testimonial_company = $customFields['wpzoom_testimonial_author_company'][0];  
		}
		if ($show_author_company_link == 'on') { 
			$testimonial_company_url = $customFields['wpzoom_testimonial_author_company_url'][0];  
		} 
		
		?>
				<div class="testimonial">

					<?php 
					if ($show_photo == 'on') {
						get_the_image( array( 'size' => 'testimonial-widget-author-photo', 'width' => 72, 'before' => '<div class="cover">', 'after' => '</div>', 'link_to_post' => false ) );
					}

					if ($testimonial_author) echo "<h4>$testimonial_author</h4>";
					if ($testimonial_position) echo "<p class=\"position\">$testimonial_position</p>";
					if ($testimonial_company) {
						echo '<p class="company">';
						if ($testimonial_company_url) echo "<a href=\"$testimonial_company_url\">";
						echo $testimonial_company;
						if ($testimonial_company_url) echo '</a>';
						echo '</p>';
					}
					?>

				<blockquote><?php the_excerpt(); ?></blockquote>
				<div class="cleaner">&nbsp;</div>

				</div><!-- end .testimonial -->
			<?php
			endwhile; 
			
			//Reset query_posts
			wp_reset_query();			

		/* After widget (defined by themes). */
		echo $after_widget;
	}
	
	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags (if needed) and update the widget settings. */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['show_photo'] = $new_instance['show_photo'];
		$instance['show_author'] = $new_instance['show_author'];
		$instance['show_author_position'] = $new_instance['show_author_position'];
		$instance['show_author_company'] = $new_instance['show_author_company'];
		$instance['show_author_company_link'] = $new_instance['show_author_company_link'];
		$instance['random_post'] = $new_instance['random_post'];
		return $instance;
	}
	
	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => 'Widget Title', 'show_title' => 'on', 'show_count' => 1, 'show_photo' => 'on', 'show_author' => 'on', 'show_author_position' => 'on', 'show_author_company' => 'on', 'show_author_company_link' => 'on', 'random_post' => 'on');
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Widget Title:</label><br />
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" type="text" class="widefat" size="35" />
		</p>

		<p>
			<input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id('show_photo'); ?>" name="<?php echo $this->get_field_name('show_photo'); ?>" <?php if ($instance['show_photo'] == 'on') { echo ' checked="checked"';  } ?> /> 
			<label for="<?php echo $this->get_field_id('show_photo'); ?>"><?php _e('Display author photo', 'wpzoom'); ?></label>
		</p>

		<p>
			<input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id('show_author'); ?>" name="<?php echo $this->get_field_name('show_author'); ?>" <?php if ($instance['show_author'] == 'on') { echo ' checked="checked"';  } ?> /> 
			<label for="<?php echo $this->get_field_id('show_author'); ?>"><?php _e('Display author name', 'wpzoom'); ?></label>
		</p>

		<p>
			<input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id('show_author_position'); ?>" name="<?php echo $this->get_field_name('show_author_position'); ?>" <?php if ($instance['show_author_position'] == 'on') { echo ' checked="checked"';  } ?> /> 
			<label for="<?php echo $this->get_field_id('show_author_position'); ?>"><?php _e('Display author position', 'wpzoom'); ?></label>
		</p>

		<p>
			<input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id('show_author_company'); ?>" name="<?php echo $this->get_field_name('show_author_company'); ?>" <?php if ($instance['show_author_company'] == 'on') { echo ' checked="checked"';  } ?> /> 
			<label for="<?php echo $this->get_field_id('show_author_company'); ?>"><?php _e('Display author company', 'wpzoom'); ?></label>
		</p>

		<p>
			<input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id('show_author_company_link'); ?>" name="<?php echo $this->get_field_name('show_author_company_link'); ?>" <?php if ($instance['show_author_company_link'] == 'on') { echo ' checked="checked"';  } ?> /> 
			<label for="<?php echo $this->get_field_id('show_author_company_link'); ?>"><?php _e('Link author company', 'wpzoom'); ?></label>
		</p>

		<p>
			<input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id('random_post'); ?>" name="<?php echo $this->get_field_name('random_post'); ?>" <?php if ($instance['random_post'] == 'on') { echo ' checked="checked"';  } ?> /> 
			<label for="<?php echo $this->get_field_id('random_post'); ?>"><?php _e('Show a Random Testimonial', 'wpzoom'); ?></label>
		</p>
		
		<?php
	}
}

function wpzoom_register_testimonials_widget() {
	register_widget('wpzoom_testimonial');
}
add_action('widgets_init', 'wpzoom_register_testimonials_widget');