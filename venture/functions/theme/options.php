<?php return array(


/* Theme Admin Menu */
"menu" => array(
    array("id"    => "1",
          "name"  => "General"),
    
    array("id"    => "2",
          "name"  => "Homepage"),

    array("id"    => "5",
          "name"  => "Styling"),
),

/* Theme Admin Options */
"id1" => array(
    array("type"  => "preheader",
          "name"  => "Theme Settings"),
 
	array("name"  => "Logo Image",
          "desc"  => "Upload a custom logo image for your site, or you can specify an image URL directly.",
          "id"    => "misc_logo_path",
          "std"   => "",
          "type"  => "upload"),

    array("name"  => "Display Site Tagline under Logo?",
          "desc"  => "Tagline can be changed in <a href='options-general.php' target='_blank'>General Settings</a>",
          "id"    => "logo_desc",
          "std"   => "on",
          "type"  => "checkbox"),

    array("name"  => "Favicon URL",
          "desc"  => "Upload a favicon image (16&times;16px).",
          "id"    => "misc_favicon",
          "std"   => "",
          "type"  => "upload"),
          
    array("name"  => "Custom Feed URL",
          "desc"  => "Example: <strong>http://feeds.feedburner.com/wpzoom</strong>",
          "id"    => "misc_feedburner",
          "std"   => "",
          "type"  => "text"),
  
	array("name"  => "Enable comments for static pages",
          "id"    => "comments_page",
          "std"   => "off",
          "type"  => "checkbox"),
  

 	array("type"  => "preheader",
          "name"  => "Portfolio Template"),
          
	array("name"  => "Number of projects per page in Portfolio Template (ajax and paginated) ",
          "desc"  => "Default: <strong>12</strong>",
          "id"    => "paginated_posts",
          "std"   => "12",
          "type"  => "text"),

    array("name"  => "Portfolio Title",
          "desc"  => "Default: <strong>Our works</strong>",
          "id"    => "portfolio_title",
          "std"   => "Our works",
          "type"  => "text"),

    array("name" => "Portfolio Page",
         "desc"  => "Choose the page to which should link <em>All</em> button.",
         "id"    => "portfolio_url",
         "std"   => "",
         "type"  => "select-page"),
          
	array("name"  => "Display Categories on the Top ?",
          "id"    => "portfolio_tags",
          "std"   => "on",
          "type"  => "checkbox"),
          
          
 	array("type"  => "preheader",
          "name"  => "Global Posts Options"),
	
	array("name"  => "Content",
          "desc"  => "The number of posts displayed on homepage can be changed <a href=\"options-reading.php\" target=\"_blank\">here</a>.",
          "id"    => "display_content",
          "options" => array('Excerpt', 'Full Content'),
          "std"   => "Excerpt",
          "type"  => "select"),
     
    array("name"  => "Excerpt length",
          "desc"  => "Default: <strong>50</strong> (words)",
          "id"    => "excerpt_length",
          "std"   => "50",
          "type"  => "text"),

    array("type"  => "startsub",
          "name"  => "Thumbnail"),
          
        array("name"  => "Display thumbnail",
              "id"    => "index_thumb",
              "std"   => "on",
              "type"  => "checkbox"),
              
        array("name"  => "Thumbnail Width (in pixels)",
              "desc"  => "Default: <strong>600</strong> (pixels)",
              "id"    => "thumb_width",
              "std"   => "600",
              "type"  => "text"),
              
        array("name"  => "Thumbnail Height (in pixels)",
              "desc"  => "Default: <strong>230</strong> (pixels)",
              "id"    => "thumb_height",
              "std"   => "230",
              "type"  => "text"),

    array("type"  => "endsub"),


    array("type"  => "startsub",
          "name"  => "Meta"),

    	array("name"  => "Display Author",
              "id"    => "display_author",
              "std"   => "on",
              "type"  => "checkbox"),

        array("name"  => "Display Date/Time",
              "desc"  => "<strong>Date/Time format</strong> can be changed <a href='options-general.php' target='_blank'>here</a>.",
              "id"    => "display_date",
              "std"   => "on",
              "type"  => "checkbox"),  
              
        array("name"  => "Display Category",
              "id"    => "display_category",
              "std"   => "on",
              "type"  => "checkbox"),    
              
        array("name"  => "Display Comments Count",
              "id"    => "display_comments",
              "std"   => "on",
              "type"  => "checkbox"),

    array("type"  => "endsub"),

	
    array("name"  => "Display Read More Button",
          "id"    => "display_readmore",
          "std"   => "on",
          "type"  => "checkbox"),
           
          
	array("type"  => "preheader",
          "name"  => "Single Post Options"),

    array("name"  => "Header Title",
          "desc"  => "Default: <strong>Blog</strong>",
          "id"    => "blog_title",
          "std"   => "Blog",
          "type"  => "text"),

    array("type"  => "startsub",
          "name"  => "Display meta"),
      
     	array("name"  => "Date/Time",
              "desc"  => "<strong>Date/Time format</strong> can be changed <a href='options-general.php' target='_blank'>here</a>.",
              "id"    => "post_date",
              "std"   => "on",
              "type"  => "checkbox"),  
              
        array("name"  => "Author Name",
              "desc"  => "You can edit your profile on this <a href='profile.php' target='_blank'>page</a>.",
              "id"    => "post_author",
              "std"   => "on",
              "type"  => "checkbox"),

        array("name"  => "Comments",
              "id"    => "post_comments",
              "std"   => "on",
              "type"  => "checkbox"),

        array("name"  => "Category",
              "id"    => "post_category",
              "std"   => "on",
              "type"  => "checkbox"),
 

     array("type"  => "endsub"),
          
          
	array("type"  => "preheader",
          "name"  => "Translations"),
           
	array("name"  => "Overview",
          "desc"  => "Default: <em>Overview</em>",
          "id"    => "overview",
          "std"   => "Overview",
          "type"  => "text"),
          
	array("name"  => "Client",
          "desc"  => "Default: <em>Client</em>",
          "id"    => "client",
          "std"   => "Client",
          "type"  => "text"),
          
	array("name"  => "Services",
          "desc"  => "Default: <em>Services</em>",
          "id"    => "services",
          "std"   => "Services",
          "type"  => "text"),

	array("name"  => "Skills",
          "desc"  => "Default: <em>Skills</em>",
          "id"    => "skills",
          "std"   => "Skills",
          "type"  => "text"),
          
 	array("name"  => "Previous project",
          "desc"  => "Default: <em>Previous project</em>",
          "id"    => "prev_project",
          "std"   => "Previous project",
          "type"  => "text"),
          
	array("name"  => "Next project",
          "desc"  => "Default: <em>Next project</em>",
          "id"    => "next_project",
          "std"   => "Next project",
          "type"  => "text"),
),

"id2" => array(
    
    array("type"  => "preheader",
          "name"  => "Homepage Slider"),   
              
    array("name"  => "Display the slider on homepage?",
          "desc"  => "Do you want to show a featured slider on the homepage? To add posts in slider, go to <a href='edit.php?post_type=slideshow' target='_blank'>Slideshow section</a>",
          "id"    => "featured_posts_show",
          "std"   => "on",
          "type"  => "checkbox"),
    
    array("name"  => "Autoplay Slider?",
          "desc"  => "Do you want to auto-scroll the slides?",
    	  "id"    => "slideshow_auto",
          "std"   => "on",
          "type"  => "checkbox"),
            
    array("name"  => "Slider Autoplay Interval",
          "desc"  => "Select the interval (in miliseconds) at which the Slider should change posts (<strong>if autoplay is enabled</strong>). Default: 3000 (3 seconds).",
          "id"    => "slideshow_speed",
          "std"   => "3000",
          "type"  => "text"),
            
    array("name"  => "Slider Effect",
          "desc"  => "Select the effect for slides transition.",
          "id"    => "slideshow_effect",
          "options" => array('Slide', 'Fade'),
          "std"   => "Slide",
          "type"  => "select"),
            
    array("name"  => "Number of Posts in Slider",
          "desc"  => "How many posts should appear in \"Featured Slider\" on the homepage? Default: 5.",
          "id"    => "featured_posts_posts",
          "std"   => "5",
          "type"  => "text"),
          
    
    array("type"  => "preheader",
          "name"  => "Intro Message"),
    
    array("name"  => "Show Intro Message on Homepage?",
          "id"    => "homepage_intro",
          "std"   => "on",
          "type"  => "checkbox"),

    array("name"  => "Intro Message",
          "desc"  => "Add your short intro message.",
          "id"    => "homepage_intro_msg",
          "type"  => "textarea"),
 

),


"id5" => array(
    array("type"  => "preheader",
          "name"  => "Colors"),

    array("name"  => "Logo Color",
           "id"   => "logo_color",
           "type" => "color",
           "selector" => "#logo h1 a",
           "attr" => "color"),
   
    array("name"  => "Menu Link Color",
           "id"   => "menu_color",
           "type" => "color",
           "selector" => ".dropdown a:hover, .dropdown li:hover, .dropdown li:hover a, li.current_page_parent  a, li.current-menu-item a, li.current_page_item a, li.current-menu-ancestor a, li.current-menu-parent a, li.current_page_parent a",
           "attr" => "background-color"),

    array("name"  => "Menu Arrow Color",
           "id"   => "menu_border_color",
           "type" => "color",
           "selector" => ".dropdown > li > a:hover:after, li.current_page_parent  a:after, li.current-menu-item a:after, li.current_page_item a:after, li.current-menu-ancestor a:after, li.current-menu-parent a:after, li.current_page_parent a:after",
           "attr" => "border-top-color"),

    array("name"  => "Link Color",
           "id"   => "a_css_color",
           "type" => "color",
           "selector" => "a",
           "attr" => "color"),
           
    array("name"  => "Link Hover Color",
           "id"   => "ahover_css_color",
           "type" => "color",
           "selector" => "a:hover",
           "attr" => "color"),

    array("name"  => "Widget Title Color",
           "id"   => "widget_color",
           "type" => "color",
           "selector" => "h3.title",
           "attr" => "color"),
    
    array("name"  => "Buttons Color",
           "id"   => "button_color",
           "type" => "color",
           "selector" => ".wpzoom-calltoaction .cta-btn, #heading #top_button a",
           "attr" => "background-color"),

    array("name"  => "Header Title and Footer Background",
           "id"   => "headerbg_color",
           "type" => "color",
           "selector" => "#heading, #footer .wrap",
           "attr" => "background-color"),


    array("type"  => "preheader",
          "name"  => "Fonts"),

    array("name" => "General Text Font Style", 
          "id" => "typo_body", 
          "type" => "typography", 
          "selector" => "body" ),

    array("name" => "Logo Text Style", 
          "id" => "typo_logo", 
          "type" => "typography", 
          "selector" => "#logo h1 a" ),

    array("name"  => "Post Title Style",
           "id"   => "typo_post_title",
           "type" => "typography",
           "selector" => ".post h2.title a, .post h1.title a"),
 
     array("name"  => "Widget Title Style",
           "id"   => "typo_widget",
           "type" => "typography",
           "selector" => ".widget h3.title "),

)

/* end return */);