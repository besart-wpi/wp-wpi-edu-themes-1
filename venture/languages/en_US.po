msgid ""
msgstr ""
"Project-Id-Version: Venture v1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2012-11-15 17:13:18+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Poedit-Language: \n"
"X-Poedit-Country: \n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;\n"
"X-Poedit-Basepath: \n"
"X-Poedit-Bookmarks: \n"
"X-Poedit-SearchPath-0: .\n"
"X-Textdomain-Support: yes"

#: 404.php:4
#@ wpzoom
msgid "Error 404 - Nothing Found"
msgstr ""

#: 404.php:11
#@ wpzoom
msgid "The page you are looking for could not be found."
msgstr ""

#: archive.php:8
#@ wpzoom
msgid "Archive for category:"
msgstr ""

#: archive.php:9
#@ wpzoom
msgid "Post Tagged with:"
msgstr ""

#: archive.php:10
#: archive.php:11
#: archive.php:12
#@ wpzoom
msgid "Archive for"
msgstr ""

#: archive.php:14
#@ wpzoom
msgid "Archives"
msgstr ""

#: archive.php:15
#@ wpzoom
msgid "Recent Posts"
msgstr ""

#: attachment.php:24
#@ wpzoom
msgid "View full size"
msgstr ""

#: attachment.php:36
#@ wpzoom
msgid "&larr; Previous picture"
msgstr ""

#: attachment.php:37
#@ wpzoom
msgid "Next picture &rarr;"
msgstr ""

#: comments.php:19
#@ wpzoom
msgid "No Comments"
msgstr ""

#: comments.php:19
#@ wpzoom
msgid "% Comments"
msgstr ""

#: functions/theme/widgets/call-to-action.php:64
#@ wpzoom
msgid "Message:"
msgstr ""

#: footer.php:57
#@ wpzoom
msgid "Copyright"
msgstr ""

#: footer.php:57
#@ wpzoom
msgid "All Rights Reserved"
msgstr ""

#: functions/wpzoom/assets/js/shortcode-generator/dialog.php:45
#@ wpzoom
msgid "Customize the Shortcode"
msgstr ""

#: functions/wpzoom/assets/js/shortcode-generator/dialog.php:54
#@ wpzoom
msgid "Preview"
msgstr ""

#: functions/wpzoom/assets/js/shortcode-generator/dialog.php:68
#@ wpzoom
msgid "Error"
msgstr ""

#: functions/wpzoom/assets/js/shortcode-generator/dialog.php:71
#@ wpzoom
msgid "Your version of theme does not yet support shortcodes."
msgstr ""

#: functions/wpzoom/assets/js/shortcode-generator/dialog.php:76
#@ wpzoom
msgid "Looks like your active theme is not from WPZOOM. The shortcode generator only works with themes from WPZOOM."
msgstr ""

#: functions/theme/custom-post-types.php:92
#@ wpzoom
msgctxt "post type general name"
msgid "Slideshow"
msgstr ""

#: functions/theme/custom-post-types.php:93
#@ wpzoom
msgctxt "post type singular name"
msgid "Slideshow Item"
msgstr ""

#: functions/theme/custom-post-types.php:94
#@ wpzoom
msgctxt "slideshow item"
msgid "Add New"
msgstr ""

#: functions/theme/custom-post-types.php:95
#@ wpzoom
msgid "Add New Slideshow Item"
msgstr ""

#: functions/theme/custom-post-types.php:96
#@ wpzoom
msgid "Edit Slideshow Item"
msgstr ""

#: functions/theme/custom-post-types.php:97
#@ wpzoom
msgid "New Slideshow Item"
msgstr ""

#: functions/theme/custom-post-types.php:98
#@ wpzoom
msgid "View Slideshow Item"
msgstr ""

#: functions/theme/custom-post-types.php:99
#@ wpzoom
msgid "Search Slideshow"
msgstr ""

#: functions/theme/custom-post-types.php:19
#: functions/theme/custom-post-types.php:59
#: functions/theme/custom-post-types.php:100
#@ wpzoom
msgid "Nothing found"
msgstr ""

#: functions/theme/custom-post-types.php:20
#: functions/theme/custom-post-types.php:60
#: functions/theme/custom-post-types.php:101
#@ wpzoom
msgid "Nothing found in Trash"
msgstr ""

#: functions/theme/custom-post-types.php:129
#: functions/theme/custom-post-types.php:130
#: taxonomy.php:17
#: template-portfolio-ajax.php:32
#@ wpzoom
msgid "Portfolio"
msgstr ""

#: functions/theme/custom-post-types.php:131
#@ wpzoom
msgid "portfolios"
msgstr ""

#: functions/theme/custom-post-types.php:132
#@ wpzoom
msgctxt "slide"
msgid "Add New"
msgstr ""

#: functions/theme/custom-post-types.php:133
#@ wpzoom
msgid "Add New Portfolio"
msgstr ""

#: functions/theme/custom-post-types.php:134
#@ wpzoom
msgid "Edit Portfolio"
msgstr ""

#: functions/theme/custom-post-types.php:135
#@ wpzoom
msgid "New Portfolio"
msgstr ""

#: functions/theme/custom-post-types.php:136
#@ wpzoom
msgid "View Portfolio"
msgstr ""

#: functions/theme/custom-post-types.php:137
#@ wpzoom
msgid "Search Portfolio"
msgstr ""

#: functions/theme/custom-post-types.php:138
#@ wpzoom
msgid "No portfolios found"
msgstr ""

#: functions/theme/custom-post-types.php:139
#@ wpzoom
msgid "No portfolios found in Trash"
msgstr ""

#: functions/theme/custom-post-types.php:159
#: functions/theme/custom-post-types.php:169
#@ default
msgid "portfolio"
msgstr ""

#: functions/theme/custom-post-types.php:168
#: functions/theme/custom-post-types.php:217
#@ default
msgid "skill-type"
msgstr ""

#: functions/theme/custom-post-types.php:171
#@ wpzoom
msgid "Portfolio Categories"
msgstr ""

#: functions/theme/custom-post-types.php:172
#@ wpzoom
msgid "Portfolio Category"
msgstr ""

#: functions/theme/custom-post-types.php:185
#@ wpzoom
msgid "Slide Title"
msgstr ""

#: functions/theme/custom-post-types.php:200
#@ wpzoom
msgid "Title"
msgstr ""

#: functions/theme/custom-post-types.php:201
#@ wpzoom
msgid "Type"
msgstr ""

#: functions/theme/custom-post-types.php:216
#@ default
msgid "type"
msgstr ""

#: functions/theme/custom-post-types.php:241
#, php-format
#@ wpzoom
msgid "View all posts filed under %s"
msgstr ""

#: functions/theme/custom-post-types.php:257
#, php-format
#@ wpzoom
msgid "Feed for all posts filed under %s"
msgstr ""

#: functions/wpzoom/components/shortcodes/shortcodes.php:603
#: functions/wpzoom/components/shortcodes/shortcodes.php:653
#: functions/wpzoom/components/shortcodes/shortcodes.php:669
#, php-format
#@ wpzoom
msgid "My %s Profile"
msgstr ""

#: functions/wpzoom/components/shortcodes/shortcodes.php:624
#@ wpzoom
msgid "Subscribe to our RSS feed"
msgstr ""

#: single-portfolio.php:144
#: single-portfolio.php:149
#: taxonomy.php:37
#: taxonomy.php:42
#: template-portfolio-ajax.php:80
#: template-portfolio-ajax.php:85
#: template-portfolio-paginated.php:69
#: template-portfolio-paginated.php:74
#: template-portfolio.php:58
#: template-portfolio.php:63
#, php-format
#@ wpzoom
msgid "Permanent Link to %s"
msgstr ""

#: loop.php:5
#: page.php:29
#: single-portfolio.php:75
#: single.php:26
#: template-full-width.php:34
#@ wpzoom
msgid "Edit"
msgstr ""

#: page.php:35
#: single.php:41
#: template-full-width.php:40
#@ wpzoom
msgid "Pages"
msgstr ""

#: page.php:47
#: single.php:54
#: template-full-width.php:47
#@ wpzoom
msgid "Sorry, no posts matched your criteria"
msgstr ""

#: search.php:4
#@ wpzoom
msgid "Search Results for"
msgstr ""

#: loop.php:11
#: single.php:32
#@ wpzoom
msgid "1 comment"
msgstr ""

#: loop.php:11
#: single.php:32
#@ wpzoom
msgid "% comments"
msgstr ""

#: searchform.php:3
#: searchform.php:4
#@ wpzoom
msgid "Search"
msgstr ""

#: taxonomy.php:21
#: template-portfolio-ajax.php:36
#: template-portfolio-paginated.php:35
#: template-portfolio.php:37
#@ wpzoom
msgid "All"
msgstr ""

#: functions/theme/post-options.php:72
#@ wpzoom
msgid "Insert into Post"
msgstr ""

#: functions/theme/post-options.php:72
#@ wpzoom
msgid "Use this image"
msgstr ""

#: functions/theme/widgets/facebook-like-box.php:117
#@ wpzoom
msgid "Show Faces"
msgstr ""

#: functions/theme/widgets/facebook-like-box.php:120
#@ wpzoom
msgid "Show Stream"
msgstr ""

#: functions/theme/widgets/facebook-like-box.php:123
#@ wpzoom
msgid "Show Header"
msgstr ""

#: functions/theme/widgets/portfolio-slider-tabs.php:76
#: functions/theme/widgets/recentposts.php:64
#: functions/theme/widgets/twitter.php:68
#: functions/theme/widgets/twitter_bubble.php:69
#, php-format
#@ wpzoom
msgid "%s"
msgstr ""

#: functions/theme/widgets/social.php:13
#@ wpzoom
msgid "Custom WPZOOM Widget that displays links to social sharing websites."
msgstr ""

#: functions/theme/widgets/social.php:19
#@ wpzoom
msgid "WPZOOM: Social Widget"
msgstr ""

#: functions/theme/widgets/social.php:88
#@ wpzoom
msgid "Widget Title:"
msgstr ""

#: functions/theme/widgets/twitter.php:64
#: functions/theme/widgets/twitter_bubble.php:65
#, php-format
#@ wpzoom
msgid "%s ago"
msgstr ""

#: functions/wpzoom/components/medialib-uploader.php:24
#@ wpzoom
msgid "Upload"
msgstr ""

#: functions/wpzoom/components/medialib-uploader.php:43
#@ wpzoom
msgid "View File"
msgstr ""

#: functions/wpzoom/components/medialib-uploader.php:55
#@ wpzoom
msgid "WPZOOM Exporter"
msgstr ""

#: footer.php:58
#: functions/wpzoom/wpzoom.php:106
#@ wpzoom
msgid "WPZOOM"
msgstr ""

#: functions/wpzoom/wpzoom.php:107
#@ wpzoom
msgid "Theme Options"
msgstr ""

#: functions/wpzoom/wpzoom.php:110
#@ wpzoom
msgid "Framework Update"
msgstr ""

#: functions/theme/functions.php:185
#, php-format
#@ wpzoom
msgid "%s at %s"
msgstr ""

#: loop.php:11
#: single.php:32
#@ wpzoom
msgid "0 comments"
msgstr ""

#: loop.php:11
#: single.php:32
#@ wpzoom
msgid "Comments are Disabled"
msgstr ""

#: loop.php:23
#: loop.php:25
#@ wpzoom
msgid "Read more"
msgstr ""

#: archive.php:13
#@ wpzoom
msgid " Articles by: "
msgstr ""

#: comments.php:3
#@ wpzoom
msgid "This post is password protected. Enter the password to view any comments."
msgstr ""

#: comments.php:19
#@ wpzoom
msgid "One Comment"
msgstr ""

#: comments.php:23
#: comments.php:41
#@ wpzoom
msgid "<span class=\"meta-nav\">&larr;</span> Older Comments"
msgstr ""

#: comments.php:23
#: comments.php:41
#@ wpzoom
msgid "Newer Comments <span class=\"meta-nav\">&rarr;</span>"
msgstr ""

#: comments.php:53
#@ wpzoom
msgid "Comments are closed."
msgstr ""

#: comments.php:61
#@ wpzoom
msgid "Your Name"
msgstr ""

#: comments.php:67
#@ wpzoom
msgid "Your Email"
msgstr ""

#: comments.php:72
#@ wpzoom
msgid "Your Website"
msgstr ""

#: comments.php:76
#@ wpzoom
msgid "Comment"
msgstr ""

#: comments.php:79
#, php-format
#@ default
msgid "Logged in as <a href=\"%1$s\">%2$s</a>. <a href=\"%3$s\">Log out?</a>"
msgstr ""

#: comments.php:80
#@ wpzoom
msgid "Leave a Reply"
msgstr ""

#: comments.php:81
#@ wpzoom
msgid "Cancel"
msgstr ""

#: comments.php:82
#@ wpzoom
msgid "Submit"
msgstr ""

#: footer.php:58
#@ wpzoom
msgid "Designed by"
msgstr ""

#: functions/theme/custom-post-types.php:11
#@ wpzoom
msgctxt "post type general name"
msgid "Clients"
msgstr ""

#: functions/theme/custom-post-types.php:12
#@ wpzoom
msgctxt "post type singular name"
msgid "Client"
msgstr ""

#: functions/theme/custom-post-types.php:13
#@ wpzoom
msgctxt "slideshow item"
msgid "Add a New Client"
msgstr ""

#: functions/theme/custom-post-types.php:14
#@ wpzoom
msgid "Add New Client"
msgstr ""

#: functions/theme/custom-post-types.php:15
#@ wpzoom
msgid "Edit Client"
msgstr ""

#: functions/theme/custom-post-types.php:16
#@ wpzoom
msgid "New Client"
msgstr ""

#: functions/theme/custom-post-types.php:17
#@ wpzoom
msgid "View Client"
msgstr ""

#: functions/theme/custom-post-types.php:18
#@ wpzoom
msgid "Search Clients"
msgstr ""

#: functions/theme/custom-post-types.php:51
#@ wpzoom
msgctxt "post type general name"
msgid "Testimonials"
msgstr ""

#: functions/theme/custom-post-types.php:52
#@ wpzoom
msgctxt "post type singular name"
msgid "Testimonial"
msgstr ""

#: functions/theme/custom-post-types.php:53
#@ wpzoom
msgctxt "slideshow item"
msgid "Add a New"
msgstr ""

#: functions/theme/custom-post-types.php:54
#@ wpzoom
msgid "Add New Testimonial"
msgstr ""

#: functions/theme/custom-post-types.php:55
#@ wpzoom
msgid "Edit Testimonial"
msgstr ""

#: functions/theme/custom-post-types.php:56
#@ wpzoom
msgid "New Testimonial"
msgstr ""

#: functions/theme/custom-post-types.php:57
#@ wpzoom
msgid "View Testimonial"
msgstr ""

#: functions/theme/custom-post-types.php:58
#@ wpzoom
msgid "Search Testimonials"
msgstr ""

#: functions/theme/functions.php:182
#, php-format
#@ wpzoom
msgid "%s <span class=\"says\">says:</span>"
msgstr ""

#: functions/theme/functions.php:185
#: functions/theme/functions.php:211
#@ wpzoom
msgid "(Edit)"
msgstr ""

#: functions/theme/functions.php:192
#@ wpzoom
msgid "Your comment is awaiting moderation."
msgstr ""

#: functions/theme/functions.php:211
#@ wpzoom
msgid "Pingback:"
msgstr ""

#: functions/theme/post-options.php:300
#@ wpzoom
msgid "Button Title"
msgstr ""

#: functions/theme/post-options.php:305
#@ wpzoom
msgid "Button URL"
msgstr ""

#: functions/theme/widgets/call-to-action.php:12
#@ wpzoom
msgid "Custom WPZOOM widget that displays a call-to-action message and button."
msgstr ""

#: functions/theme/widgets/call-to-action.php:18
#@ wpzoom
msgid "WPZOOM: Call to Action"
msgstr ""

#: functions/theme/widgets/call-to-action.php:69
#@ wpzoom
msgid "Button Label:"
msgstr ""

#: functions/theme/widgets/call-to-action.php:74
#@ wpzoom
msgid "Button URL:"
msgstr ""

#: functions/theme/widgets/single-page.php:12
#@ wpzoom
msgid "Custom WPZOOM widget that displays a single specified static page."
msgstr ""

#: functions/theme/widgets/single-page.php:18
#@ wpzoom
msgid "WPZOOM: Single Page"
msgstr ""

#: functions/theme/widgets/single-page.php:65
#@ wpzoom
msgid "Page to Display:"
msgstr ""

#: functions/theme/widgets/single-page.php:71
#@ wpzoom
msgid "Link Page Title to Page"
msgstr ""

#: functions/theme/widgets/testimonials.php:132
#@ wpzoom
msgid "Display author photo"
msgstr ""

#: functions/theme/widgets/testimonials.php:137
#@ wpzoom
msgid "Display author name"
msgstr ""

#: functions/theme/widgets/testimonials.php:142
#@ wpzoom
msgid "Display author position"
msgstr ""

#: functions/theme/widgets/testimonials.php:147
#@ wpzoom
msgid "Display author company"
msgstr ""

#: functions/theme/widgets/testimonials.php:152
#@ wpzoom
msgid "Link author company"
msgstr ""

#: functions/theme/widgets/testimonials.php:157
#@ wpzoom
msgid "Show a Random Testimonial"
msgstr ""

#: functions/wpzoom/components/admin/settings-page.php:119
#@ wpzoom
msgid "For more information:"
msgstr ""

#: functions/wpzoom/components/admin/settings-page.php:120
#@ wpzoom
msgid "<a href=\"http://www.wpzoom.com/support/documentation\" target=\"_blank\">Documentation and Tutorials</a>"
msgstr ""

#: functions/wpzoom/components/admin/settings-page.php:121
#@ wpzoom
msgid "<a href=\"http://www.wpzoom.com/forum/\" target=\"_blank\">Support Forums</a>"
msgstr ""

#: functions/wpzoom/components/video-thumb.php:86
#@ wpzoom
msgid "<strong>NOTICE!&nbsp;&nbsp;Unable to fetch video thumbnail</strong><br/>Either an invalid embed code was provided, or there is no thumbnail available for the specified video&hellip;<br/><small id=\"wpz_autothumb_remind\"><strong>REMINDER:</strong> You can always manually upload a featured image via the WordPress Media Uploader.</small>"
msgstr ""

#: loop.php:9
#: single.php:30
#@ wpzoom
msgid "By "
msgstr ""

#: pagination.php:11
#@ wpzoom
msgid "&larr;  Newer"
msgstr ""

#: pagination.php:12
#@ wpzoom
msgid "Older  &rarr;"
msgstr ""

#: single-portfolio.php:139
#@ wpzoom
msgid "Related Projects"
msgstr ""

