<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
	<head>
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<title><?php ui::title(); ?></title>

		<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" media="screen" />

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

		<link href="https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300,300italic,900,900italic" rel="stylesheet" type="text/css" />

		<?php wp_head(); ?>
 		<?php if (is_home() || get_post_type() == 'portfolio') { ui::js("flexslider"); } ?>
	</head>

	<body <?php body_class() ?>>
		<div id="wrapper">
			<div id="inner-wrap">
				<div id="header">
					<div id="logo">
						<?php if (!option::get('misc_logo_path')) { echo "<h1>"; } ?>

						<a href="<?php echo home_url(); ?>" title="<?php bloginfo('description'); ?>">
							<?php if (!option::get('misc_logo_path')) { bloginfo('name'); } else { ?>
								<img src="<?php echo ui::logo(); ?>" alt="<?php bloginfo('name'); ?>" />
							<?php } ?>
						</a>

						<?php if (!option::get('misc_logo_path')) { echo "</h1>"; } ?>

						<?php if (option::get('logo_desc') == 'on') {  ?><p id="tagline"><?php bloginfo('description'); ?></p><?php } ?>
					</div><!-- / #logo -->
   
					<a class="btn_menu" id="toggle" href="#"></a>

 					<div id="menu">

 						<?php if (has_nav_menu( 'primary' )) {
							wp_nav_menu( array(
							'container' => 'menu',
							'container_class' => '',
							'menu_class' => 'dropdown',
							'menu_id' => 'mainmenu',
							'sort_column' => 'menu_order',
							'theme_location' => 'primary'
							));
						} else {
							echo '<p>Please set your Main navigation menu on the <strong><a href="'.get_admin_url().'nav-menus.php">Appearance > Menus</a></strong> page.</p>';
						} ?>
					</div>

					<div class="clear"></div>
 					 
				</div><!-- / #header-->

				<div id="main">

					<?php if ( is_home() && option::get('homepage_intro') == 'on' && option::get('homepage_intro_msg') != '' ) { ?>
						<div id="heading">
							<div id="intro">
								<p><?php echo esc_html( trim( option::get('homepage_intro_msg') ) ); ?></p>
							</div>
						</div>
					<?php } ?>