// Replace all "gallery_shortcode_uid"

jQuery(document).ready(function($) {
    tinymce.create('tinymce.plugins.gallery_shortcode_uid', {
        init : function(ed, url) {
                ed.addCommand('gallery_shortcode_uid', function() {
                    selected = tinyMCE.activeEditor.selection.getContent();
					
					var width = jQuery(window).width(), H = jQuery(window).height(), W = ( 720 < width ) ? 720 : width;
					W = W - 80;
					H = H - 120;
					tb_show( 'Gallery', '#TB_inline?width=' + W + '&height=' + H + '&inlineId=gallery_shortcode_uid_template' );
                    
                });
            ed.addButton('gallery_shortcode_uid', {title : 'Gallery', cmd : 'gallery_shortcode_uid', image: url + '/icon.png' });
        },   
    });
    tinymce.PluginManager.add('gallery_shortcode_uid', tinymce.plugins.gallery_shortcode_uid);
});