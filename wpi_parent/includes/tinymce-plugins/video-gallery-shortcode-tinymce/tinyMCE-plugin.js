// Replace all "vgallery_shortcode_uid"

jQuery(document).ready(function($) {
    tinymce.create('tinymce.plugins.vgallery_shortcode_uid', {
        init : function(ed, url) {
                ed.addCommand('vgallery_shortcode_uid', function() {
                    selected = tinyMCE.activeEditor.selection.getContent();
					
					var width = jQuery(window).width(), H = jQuery(window).height(), W = ( 720 < width ) ? 720 : width;
					W = W - 80;
					H = H - 120;
					tb_show( 'Video Gallery', '#TB_inline?width=' + W + '&height=' + H + '&inlineId=vgallery_shortcode_uid_template' );
                    
                });
            ed.addButton('vgallery_shortcode_uid', {title : 'Video Gallery', cmd : 'vgallery_shortcode_uid', image: url + '/icon.png' });
        },   
    });
    tinymce.PluginManager.add('vgallery_shortcode_uid', tinymce.plugins.vgallery_shortcode_uid);
});