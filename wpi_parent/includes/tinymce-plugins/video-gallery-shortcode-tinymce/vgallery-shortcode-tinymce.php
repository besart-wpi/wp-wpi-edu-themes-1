<?php 

	$vgallery_easy_TinyMCE = new Easy_TinyMCE_Class;
	$vgallery_easy_TinyMCE->title = 'Insert Video Gallery'; // Popup Window Title
	$vgallery_easy_TinyMCE->js_url = trailingslashit( get_template_directory_uri() ) . 'includes/tinymce-plugins/video-gallery-shortcode-tinymce/tinyMCE-plugin.js';
	$vgallery_easy_TinyMCE->uid = 'vgallery_shortcode_uid';
	$vgallery_easy_TinyMCE->templates = array(
		array('name'=> 'Video Gallery', 'url'=> trailingslashit( get_template_directory() ) .'includes/tinymce-plugins/video-gallery-shortcode-tinymce/vgallery-TinyMCE.php')
	);
	
?>