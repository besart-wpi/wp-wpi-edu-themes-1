<?php 

	$highlight_box_easy_TinyMCE = new Easy_TinyMCE_Class;
	$highlight_box_easy_TinyMCE->title = 'Insert Highlight Box'; // Popup Window Title
	$highlight_box_easy_TinyMCE->js_url = trailingslashit( get_template_directory_uri() ) . 'includes/tinymce-plugins/highlight-box-shortcode-tinymce/tinyMCE-plugin.js';
	$highlight_box_easy_TinyMCE->uid = 'highlight_box_shortcode_uid';
	$highlight_box_easy_TinyMCE->templates = array(
		array('name'=> 'Highlight Box', 'url'=> trailingslashit( get_template_directory() ) .'includes/tinymce-plugins/highlight-box-shortcode-tinymce/highlight-box-TinyMCE.php')
	);
	
?>