<?php
// =================================================
//	:: Post Type Frameworks ::
// -------------------------------------------------

	// Home Page Slideshow
	if(WPI_PTF_HOMEPAGESLIDESHOW){
		load_template( trailingslashit( get_template_directory() ) . 'includes/post-type-frameworks/home_page_slideshow.php' );
	}
	
	// Photo Gallery
	if(WPI_PTF_PHOTOGALLERY){
		load_template( trailingslashit( get_template_directory() ) . 'includes/post-type-frameworks/photo_gallery.php' );
	}
	
	// Logo Parade
	if(WPI_PTF_LOGOPARADE){
		load_template( trailingslashit( get_template_directory() ) . 'includes/post-type-frameworks/logo_parade.php' );
	}
	
	// Video Gallery
	if(WPI_PTF_VIDEOGALLERY){
		load_template( trailingslashit( get_template_directory() ) . 'includes/post-type-frameworks/video_gallery.php' );
	}
	
	// Testimonials
	if(WPI_PTF_TESTIMONIALS){
		load_template( trailingslashit( get_template_directory() ) . 'includes/post-type-frameworks/testimonials.php' );
	}
	
	// Project Portfolio
	if(WPI_PTF_PROJECTPORTFOLIO){
		load_template( trailingslashit( get_template_directory() ) . 'includes/post-type-frameworks/project_portfolio.php' );
	}


?>