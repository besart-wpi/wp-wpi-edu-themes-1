<?php 

	// For functions.php

	// EXAMPLE SINGLE
	$sample_easy_TinyMCE = new Easy_TinyMCE_Class;
	$sample_easy_TinyMCE->title = 'Popup Window Title';
	$sample_easy_TinyMCE->js_url = trailingslashit( get_template_directory_uri() ) . 'tinymce/tinyMCE-plugin.js';
	$sample_easy_TinyMCE->uid = 'sample_shortcode_uid';
	$sample_easy_TinyMCE->templates = array(
		array('name'=> 'Alert', 'url'=> trailingslashit( get_template_directory() ) .'tinymce/alert-TinyMCE.php')
	);
	
	// EXAMPLE MULTI
	$sample2_easy_TinyMCE = new Easy_TinyMCE_Class;
	$sample2_easy_TinyMCE->title = 'Popup Window Title';
	$sample2_easy_TinyMCE->js_url = trailingslashit( get_template_directory_uri() ) . 'tinymce/tinyMCE-plugin2.js';
	$sample2_easy_TinyMCE->uid = 'sample_shortcode_uid2';
	$sample2_easy_TinyMCE->templates = array(
		array('name'=> 'Alert1', 'url'=> trailingslashit( get_template_directory() ) .'tinymce/alert-TinyMCE.php'),
		array('name'=> 'Alert2', 'url'=> trailingslashit( get_template_directory() ) .'tinymce/alert-TinyMCE.php')
	);
?>