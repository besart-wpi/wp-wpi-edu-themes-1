// Replace all "sample_shortcode_uid"

jQuery(document).ready(function($) {
    tinymce.create('tinymce.plugins.sample_shortcode_uid2', {
        init : function(ed, url) {
                ed.addCommand('sample_shortcode_uid2', function() {
                    selected = tinyMCE.activeEditor.selection.getContent();
					
					var width = jQuery(window).width(), H = jQuery(window).height(), W = ( 720 < width ) ? 720 : width;
					W = W - 80;
					H = H - 120;
					tb_show( 'Popup Window Title Bar', '#TB_inline?width=' + W + '&height=' + H + '&inlineId=sample_shortcode_uid2_template' );
                    
                });
            ed.addButton('sample_shortcode_uid2', {title : 'Shortcode Title', cmd : 'sample_shortcode_uid2', image: url + '/icon.png' });
        },   
    });
    tinymce.PluginManager.add('sample_shortcode_uid2', tinymce.plugins.sample_shortcode_uid2);
});