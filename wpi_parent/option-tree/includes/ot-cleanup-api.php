<?php if ( ! defined( 'OT_VERSION' ) ) exit( 'No direct script access allowed' );
/**
 * OptionTree Cleanup API
 *
 * This class loads all the OptionTree Cleanup methods and helpers.
 *
 * @package   OptionTree
 * @author    Derek Herman <derek@valendesigns.com>
 * @copyright Copyright (c) 2014, Derek Herman
 */
if ( ! class_exists( 'OT_Cleanup' ) ) {

  class OT_Cleanup {
  
    /**
     * PHP5 constructor method.
     *
     * This method adds other methods of the class to specific hooks within WordPress.
     *
     * @uses      add_action()
     *
     * @return    void
     *
     * @access    public
     * @since     2.4.6
     */
    function __construct() {
      if ( ! is_admin() )
        return;

      // Load styles
      add_action( 'admin_head', array( $this, 'styles' ), 1 );
      
      // Increase timeout if allowed
      add_action( 'ot_pre_consolidate_posts', array( $this, 'increase_timeout' ) );
      
    }
    
    /**
     * Adds the cleanup styles to the admin head
     *
     * @return    string
     *
     * @access    public
     * @since     2.5.0
     */
    function styles() {

      echo '<style>#toplevel_page_ot-cleanup{display:none;}</style>';

    }
    
    /**
     * Check if OptionTree needs to be cleaned up from a previous install.
     *
     * @return    void
     *
     * @access    public
     * @since     2.4.6
     */
    public function maybe_cleanup() {
      return;      
    }
    
    /**
     * Adds an admin nag.
     *
     * @return    string
     *
     * @access    public
     * @since     2.4.6
     */
    public function cleanup_notice() {

      if ( get_current_screen()->id != 'appearance_page_ot-cleanup' )
        echo '<div class="update-nag">' . sprintf( esc_html__( 'OptionTree has outdated data that should be removed. Please go to %s for more information.', 'pulse' ), sprintf( '<a href="%s">%s</a>', admin_url( 'themes.php?page=ot-cleanup' ), apply_filters( 'ot_cleanup_menu_title', esc_html__( 'OptionTree Cleanup', 'pulse' ) ) ) ) . '</div>';
    
    }

    /**
     * Adds a Tools sub page to clean up the database with.
     *
     * @return    string
     *
     * @access    public
     * @since     2.4.6
     */
    public function options_page() {
      global $wpdb, $ot_maybe_cleanup_posts, $ot_maybe_cleanup_table;

      // Option ID
      $option_id = 'ot_media_post_ID';

      // Get the media post ID
      $post_ID = get_option( $option_id, false );

      // Zero loop count
      $count = 0;

      // Check for safe mode
      $safe_mode = ini_get( 'safe_mode' );

      echo '<div class="wrap">';

        echo '<h2>' . apply_filters( 'ot_cleanup_page_title', esc_html__( 'OptionTree Cleanup', 'pulse' ) ) . '</h2>';
 
      echo '</div>';

    }

    /**
     * Increase PHP timeout.
     *
     * This is to prevent bulk operations from timing out
     *
     * @return    void
     *
     * @access    public
     * @since     2.4.6
     */
    public function increase_timeout() {
      
      if ( ! ini_get( 'safe_mode' ) ) {
      
        @set_time_limit( 0 );
        
      }
      
    }

  }

}

new OT_Cleanup();

/* End of file ot-cleanup-api.php */
/* Location: ./includes/ot-cleanup-api.php */