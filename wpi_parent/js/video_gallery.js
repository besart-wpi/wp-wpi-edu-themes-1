// Video Gallery Shortcode

jQuery(document).ready(function($){
	
	// Accepts a YouTube URL, returns the video ID
	function youtube_parser(url){
		var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
		var match = url.match(regExp);
		if (match&&match[7].length==11){
			return match[7];
		}else{
			// alert("Url incorrecta");
		}
	}
	
	// Handle clicks on the carousel thumbnails
	$('.video_carousel a.video_thumbnail').live('click', function(event){
		event.preventDefault();
		// console.log('Loading new video');
		
		targetPlayer = $(this).closest('.wpi_video_gallery').find('.video_gallery_player_element');
		videojs_id = 'videojs_' + $(this).closest('.wpi_video_gallery').data('playerid');
		
		// Remove the old player
		if( $('#'+videojs_id).length != 0 ){
			var myPlayer = videojs(videojs_id);
			myPlayer.dispose();
		}
		
		// Get video type and URL
		var videoType = $(this).data( "video-type" );
		var videoURL = $(this).attr( "href" );
		var videoTitle = $(this).attr( "title" );
		
		if(videoType == 'youtube'){ // YOUTUBE
		
			youtube_id = youtube_parser(videoURL);
			targetPlayer.empty().html('<iframe width="480" height="270" src="https://www.youtube.com/embed/'+youtube_id+'?autoplay=0" frameborder="0" allowfullscreen></iframe>');
			
		} else { // MP4
			
			targetPlayer.empty().html(
			'<video width="100%" height="auto" id="'+videojs_id+'" class="video-js vjs-default-skin">' +
				'<source src="' + videoURL + '" type="video/mp4"></source>' + 
			'</video>');
			videojs(videojs_id, {
				"autoplay": true,
				"controls": true
			});
			
			// See http://coolestguidesontheplanet.com/videodrome/videojs/ for source of the 56.25% hack to maintain 16:9 aspect ratio
			$('#'+videojs_id).css('padding-top', '56.25%');
			
		}
		
		// FitVids
		if ($.fn.fitVids) {
			targetPlayer.fitVids(); // only works on the YouTube videos, not VideoJS
		}
		
	});
	
});