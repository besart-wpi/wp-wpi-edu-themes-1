	

	<?php get_template_part( 'parts/share-overlay' ); ?>
	<footer id="footer" class="site-footer">
		<div class="wrap">
			<span>&copy; <?php echo date( 'Y' ); ?> WPI</span>
		</div>
	</footer>
	
		</div>
	</div>

</div>
<div class="loader-anim">
	<div class="col"></div><div class="col"></div><div class="col"></div><div class="col"></div><div class="col"></div>
</div>
<?php wp_footer(); ?>
</body>
</html>