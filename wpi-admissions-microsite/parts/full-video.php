	<?php // VIDEO HOLDER ?>
	<section class="row full-video" data-current="">
		<div class="upper">
			<div class="mobile-header">
				<div class="logo"><a href="<?php bloginfo( 'url' ); ?>" tabIndex="-1" title="<?php bloginfo( 'name' ); ?>"><img class="lazy" src="<?php bloginfo( 'template_url' ); ?>/assets/images/logowhite.svg" data-src="<?php bloginfo( 'template_url' ); ?>/assets/images/logo-colored.svg" loading="lazy" data-srcset="<?php bloginfo( 'template_url' ); ?>/assets/images/logo-colored.svg" alt="Colored <?php bloginfo( 'name' ); ?>" height="50" width="150">?>"></a></div>
				<a href="#" class="btn-modal-vid-close" title="Close Modal Video"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/close.svg" alt="Close Modal Video" class="svg"></a>
			</div>
			<div class="video">
				<input type="hidden" name="current-video" value="">
				<div class="caption"></div>
				<div class="audio-toggle">
				    <div class="audio-bars" data-video="#full-vid" data-state="playing"><div class="audio-bar-1"></div><div class="audio-bar-2"></div><div class="audio-bar-3"></div><div class="audio-bar-4"></div><div class="audio-bar-5"></div>
				    </div>					
				</div>
				<div class="btn-toggle-video hidden"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/play-small.svg" alt="play/pause" class="svg"></div>		
			</div>
			<div class="video-detail">
				<div class="buttons">
					<a href="#" class="btn-vid-close" tabIndex="-1" title="Close Video"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/close.svg" alt="Close Video" class="svg"></a>
					<a href="#" class="btn-vid-arrow" tabIndex="-1" title="Expand Details"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/chev-left.svg" alt="Expand Details" class="svg"></a>
					<a href="#" class="btn-vid-share show-sharer" tabIndex="-1" title="Share Video"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/share.svg" alt="Share Video" class="svg"></a>
				</div>
				<div class="scroller">
					<div class="inner">
						<div class="text"></div>
						<?php get_template_part( 'parts/inline-share' ); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="lower"></div>
	</section>