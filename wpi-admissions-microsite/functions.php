<?php

    // ACF
    if (function_exists('acf_add_options_page')) {
        // add parent
        $parent = acf_add_options_page(array(
                'page_title'    => 'Theme Options',
                'menu_title'    => 'Theme Options',
                'redirect'      => false
        ));
    }

    // Add height field to ACF WYSIWYG
    function wysiwyg_render_field_settings($field)
    {
        acf_render_field_setting($field, array(
            'label'         => __('Height of Editor'),
            'instructions'  => __('Height of Editor after Init'),
            'name'          => 'wysiwyg_height',
            'type'          => 'number',
        ));
    }
    add_action('acf/render_field_settings/type=wysiwyg', 'wysiwyg_render_field_settings', 10, 1);

    // Menus
    register_nav_menu('primary-menu', 'Primary Navigation');
    register_nav_menu('secondary-menu', 'Secondary Header');

    class primaryWalker extends Walker_Nav_Menu
    {
        public function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
        {
            if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
                $t = '';
                $n = '';
            } else {
                $t = "\t";
                $n = "\n";
            }
            $indent = ($depth) ? str_repeat($t, $depth) : '';

            $classes   = empty($item->classes) ? array() : (array) $item->classes;
            $classes[] = 'menu-item-' . $item->ID;

            // accent color
            $accent_color = get_field('accent_color', $item->object_id);
            if ($accent_color) {
                $classes[] = $accent_color;
            }

            $args = apply_filters('nav_menu_item_args', $args, $item, $depth);

            $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args, $depth));
            $class_names = $class_names ? ' class="' . esc_attr($class_names) . '"' : '';

            $id = apply_filters('nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args, $depth);
            $id = $id ? ' id="' . esc_attr($id) . '"' : '';

            $output .= $indent . '<li' . $id . $class_names . '>';

            $atts                 = array();
            $atts['title']        = ! empty($item->attr_title) ? $item->attr_title : '';
            $atts['target']       = ! empty($item->target) ? $item->target : '';
            $atts['rel']          = ! empty($item->xfn) ? $item->xfn : '';
            $atts['href']         = ! empty($item->url) ? $item->url : '';
            $atts['aria-current'] = $item->current ? 'page' : '';


            $atts = apply_filters('nav_menu_link_attributes', $atts, $item, $args, $depth);

            $attributes = '';
            foreach ($atts as $attr => $value) {
                if (! empty($value)) {
                    $value       = ('href' === $attr) ? esc_url($value) : esc_attr($value);
                    $attributes .= ' ' . $attr . '="' . $value . '"';
                }
            }

            $title = apply_filters('the_title', $item->title, $item->ID);
            $title = apply_filters('nav_menu_item_title', $title, $item, $args, $depth);

            $item_output  = $args->before;
            $item_output .= '<a' . $attributes . '>';
            $item_output .= $args->link_before . $title . $args->link_after;
            $item_output .= '</a>';
            $item_output .= $args->after;

            $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
        }
    }





    // Title Tag
    add_theme_support('title-tag');


    // THUMBNAILS
    add_theme_support('post-thumbnails', array( 'post', 'topic', 'card' ));
    add_image_size('thumb-full-screen', 1440, 1440, false);
    add_image_size('thumb-full-screen-md', 768, 768, false);
    add_image_size('thumb-topic-md', 800, 800, true);
    add_image_size('thumb-video-caro', 560, 315, true);
    add_image_size('thumb-next-topic', 920, 518, true);
    add_image_size('thumb-card', 250, 284, true);
    add_image_size('thumb-cta', 420, 132, true);





    // Allow SVG
    add_filter('wp_check_filetype_and_ext', function ($data, $file, $filename, $mimes) {
        global $wp_version;
        if ($wp_version !== '4.7.1') {
            return $data;
        }

        $filetype = wp_check_filetype($filename, $mimes);

        return [
            'ext'             => $filetype['ext'],
            'type'            => $filetype['type'],
            'proper_filename' => $data['proper_filename']
        ];
    }, 10, 4);

    function cc_mime_types($mimes)
    {
        $mimes['svg'] = 'image/svg+xml';
        return $mimes;
    }
    add_filter('upload_mimes', 'cc_mime_types');

    function fix_svg()
    {
        echo '<style type="text/css">
            .attachment-266x266, .thumbnail img {
                 width: 100% !important;
                 height: auto !important;
            }
            </style>';
    }
    add_action('admin_head', 'fix_svg');




    // Custom Post Types
    add_action('init', 'topic_post_type_init');
    function topic_post_type_init()
    {
        $labels = array(
            'name' => _x('Topics', 'post type general name'),
            'singular_name' => _x('Topic', 'post type singular name'),
            'add_new' => _x('Add New', 'Topic'),
            'add_new_item' => __('Add new topic'),
            'edit_item' => __('Edit topic'),
            'new_item' => __('New topic'),
            'view_item' => __('View topic'),
            'search_items' => __('Search topic'),
            'not_found' =>  __('No topics found'),
            'not_found_in_trash' => __('No topics found in trash'),
            'parent_item_colon' => ''
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'has_archive' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'rewrite' => true,
            'query_var' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            '_builtin' => false,
            'rewrite' => array( 'slug' => 'topics', 'with_front' => true ),
            'show_in_nav_menus' => true,
            'menu_position' => 20,
            'menu_icon' => 'dashicons-clipboard',
            'supports' => array(
                'title',
                'editor',
                'thumbnail',
                'excerpt'
            )
        );
        register_post_type('topic', $args);
    }


    add_action('init', 'cards_post_type_init');
    function cards_post_type_init()
    {
        $labels = array(
            'name' => _x('ROI Cards', 'post type general name'),
            'singular_name' => _x('ROI Card', 'post type singular name'),
            'add_new' => _x('Add New', 'Card'),
            'add_new_item' => __('Add new card'),
            'edit_item' => __('Edit card'),
            'new_item' => __('New card'),
            'view_item' => __('View card'),
            'search_items' => __('Search card'),
            'not_found' =>  __('No cards found'),
            'not_found_in_trash' => __('No cards found in trash'),
            'parent_item_colon' => ''
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'has_archive' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'rewrite' => true,
            'query_var' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            '_builtin' => false,
            'rewrite' => array( 'slug' => 'roi-cards', 'with_front' => true ),
            'show_in_nav_menus' => true,
            'menu_position' => 20,
            'menu_icon' => 'dashicons-tickets-alt',
            'supports' => array(
                'title',
                'thumbnail'
            )
        );
        register_post_type('card', $args);
    }




    // SHORTCODES
    // Custom TinyMCE Shortcode Generator
    add_action('after_setup_theme', 'custom_theme_setup');

    if (! function_exists('custom_theme_setup')) {
        function custom_theme_setup()
        {
            add_action('init', 'custom_buttons');
        }
    }

    // TinyMCE Buttons
    if (! function_exists('custom_buttons')) {
        function custom_buttons()
        {
            if (! current_user_can('edit_posts') && ! current_user_can('edit_pages')) {
                return;
            }

            if (get_user_option('rich_editing') !== 'true') {
                return;
            }

            add_filter('mce_external_plugins', 'custom_add_buttons');
            add_filter('mce_buttons', 'custom_register_buttons');
        }
    }

    if (! function_exists('custom_add_buttons')) {
        function custom_add_buttons($plugin_array)
        {
            $plugin_array['shortcode_button'] = get_template_directory_uri().'/assets/scripts/shortcode.min.js';
            return $plugin_array;
        }
    }

    if (! function_exists('custom_register_buttons')) {
        function custom_register_buttons($buttons)
        {
            array_push($buttons, 'shortcode_button');
            return $buttons;
        }
    }

    add_action('after_wp_tiny_mce', 'custom_tinymce_extra_vars');

    if (!function_exists('custom_tinymce_extra_vars')) {
        function custom_tinymce_extra_vars() { ?>
            <script type="text/javascript">
                var tinyMCE_object = <?php echo json_encode(
            array(
                        'button_name' => '',
                        'button_title' => esc_html__('Insert client testimonial', 'wpi'),
                    )
                    );
                ?>;
            </script><?php
        }
    }

    function shortcode_empty_paragraph_fix($content)
    {
        $array = array(
            '<p>[' => '[',
            ']</p>' => ']',
            ']<br />' => ']'
        );
        $content = strtr($content, $array);
        return $content;
    }
    add_filter('the_content', 'shortcode_empty_paragraph_fix');

    function quote_shortcode($atts, $content = null)
    {
        $html = "";

        if (! empty($atts['photo'])) {
            $html .= '<div class="quotes-person">';
        } else {
            $html .= '<div class="quotes-text">';
        }

        if (! empty($atts['text'])) {
            $html .= sprintf('<blockquote><p>&ldquo;%s&rdquo;</p></blockquote>', $atts['text']);
        }

        $html .= '<div class="bottom">';

        if (! empty($atts['photo'])) {
            $image_id = attachment_url_to_postid($atts['photo']);
            $html .= sprintf('<figure>%s</figure>', wp_get_attachment_image($image_id));
        }

        if (! empty($atts['author']) || ! empty($atts['title'])) {
            $html .= sprintf('<div class="name">%s <span>%s</span></div>', $atts['author'], $atts['title']);
        }

        $html .= '</div>';

        $html .= '</div>';

        return $html;
    }
    add_shortcode('quote', 'quote_shortcode');


    // ENQEUE SCRIPTS AND STYLES
    function custom_scripts() {
        $version = '1.1';

        // styles
        wp_register_style('typekit', 'https://use.typekit.net/kcy5wmb.css', array(), $version);
        if (isset($_GET['debug'])) {
            wp_register_style('stylesheet', get_stylesheet_directory_uri() . '/style.css', array(), $version);
        } else {
            wp_register_style('stylesheet', get_stylesheet_directory_uri() . '/style.min.css', array(), $version);
        }

        wp_enqueue_style('typekit');
        wp_enqueue_style('videojs');
        wp_enqueue_style('stylesheet');


        // scripts
        // wp_register_script('barba', get_template_directory_uri() . '/assets/scripts/barba.js', array( 'jquery' ), $version, true);
        wp_register_script('plugins', get_template_directory_uri() . '/assets/scripts/plugins.min.js', array( 'jquery' ), $version, true);
        if (isset($_GET['debug'])) {
            wp_register_script('custom', get_template_directory_uri() . '/assets/scripts/custom.js', array( 'jquery' ), $version, true);
        } else {
            wp_register_script('custom', get_template_directory_uri() . '/assets/scripts/custom.min.js', array( 'jquery' ), $version, true);
        }
        wp_register_script('videojs', 'https://vjs.zencdn.net/7.4.1/video.min.js', array( 'jquery' ), $version, true);

        // wp_enqueue_script('barba');
        wp_enqueue_script('plugins');
        wp_enqueue_script('videojs');
        wp_enqueue_script('custom');
    }
    add_action('wp_enqueue_scripts', 'custom_scripts', 10);


    function custom_admin_style()
    {
        wp_register_style('custom_editor_admin_css', get_template_directory_uri() . '/editor-style.css', false, '1.0');
        wp_enqueue_style('custom_editor_admin_css');
    }
    add_action('admin_enqueue_scripts', 'custom_admin_style');



    // IMAGE BY ID
    function get_image_id($image_url)
    {
        global $wpdb;
        $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url));
        return $attachment[0];
    }



    // CUSTOM BODY CLASS
    function custom_body_classes($classes)
    {
        global $wp_query;

        if (is_singular('topic')) {
            $accent_color = get_field('accent_color', $wp_query->post->ID);
            $classes[] = 'topic-' . $accent_color;
        }

        $user_agent = $_SERVER['HTTP_USER_AGENT'];

        // detecting OS
        if (stripos($user_agent, 'windows') !== false) {
            $classes[] = 'os-win';
        } elseif (stripos($user_agent, 'macintosh') !== false) {
            $classes[] = 'os-mac';
        } elseif (stripos($user_agent, 'iPhone') !== false) {
            $classes[] = 'os-ios iphone';
        } elseif (stripos($user_agent, 'iPad') !== false) {
            $classes[] = 'os-ios ipad';
        } elseif (stripos($user_agent, 'Android') !== false) {
            $classes[] = 'os-android';
        }

        // browser versions
        if (stripos($user_agent, 'Trident/7.0;') !== false) {
            $classes[] = 'ie-11';
        }

        global $is_IE, $is_safari, $is_chrome;

        if ($is_safari) {
            $classes[] = 'safari';
        } elseif ($is_chrome) {
            $classes[] = 'chrome';
        } elseif ($is_IE) {
            $classes[] = 'ie';
        } else {
            $classes[] = 'unknown';
        }

        return $classes;

        return array_unique($classes);
    };
    add_filter('body_class', 'custom_body_classes');

function hex2rgba($color, $opacity = false) {

    $default = 'rgb(0,0,0)';

    //Return default if no color provided
    if(empty($color))
          return $default;

    //Sanitize $color if "#" is provided
        if ($color[0] == '#' ) {
            $color = substr( $color, 1 );
        }

        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
                $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
                $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
                return $default;
        }

        //Convert hexadec to rgb
        $rgb =  array_map('hexdec', $hex);

        //Check if opacity is set(rgba or rgb)
        if($opacity){
            if(abs($opacity) > 1)
                $opacity = 1.0;
            $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
            $output = 'rgb('.implode(",",$rgb).')';
        }

        //Return rgb(a) color string
        return $output;
}
?>