<?php get_header(); ?>

<?php the_post(); ?>

	<section class="row home">
		<?php 
		$bg_image 		= get_field( 'bg_image' );
		$bg_video_mp4 	= get_field( 'bg_video_mp4' );
		$bg_video_webm 	= get_field( 'bg_video_webm' );
		$bg_video_ogv 	= get_field( 'bg_video_ogv' ); 
		$video_params 	= array();

		if( $bg_video_mp4 )
			$video_params[] = sprintf( '<source data-src="%s" type="video/mp4" />', $bg_video_mp4 );

		if( $bg_video_webm )
			$video_params[] = sprintf( '<source data-src="%s" type="video/webm" />', $bg_video_webm );
		
		if( $bg_video_ogv )
			$video_params[] = sprintf( '<source data-src="%s" type="video/ogg" />', $bg_video_ogv );
		
		if( $bg_image )
			$video_params[] = sprintf( '<img src="%s" alt="home video">', $bg_image[ 'sizes' ][ 'thumb-full-screen' ] ); ?>
		<div class="video">
			<?php if( ! empty( $video_params ) ){ ?>
				<video class="lazy" playsinline autoplay muted loop poster="<?php echo $bg_image[ 'sizes' ][ 'medium_large' ];?>">
					<?php echo implode( "\n", $video_params ); ?>
				</video>
			<?php } ?>
		</div>
		
		<div class="text">
			<div class="wrap">
				<div class="inner">
					<?php 
					$heading 		= get_field( 'heading' );
					$subheading 	= get_field( 'subheading' );
					echo $heading ? sprintf( '<h1>%s</h1>', $heading ) : '';
					echo $subheading; ?>
				</div>
				<span class="arrow"><img src="<?php bloginfo( 'template_url' ); ?>/assets/images/chev-down.svg" class="svg" alt="arrow down"></span>
			</div>
		</div>

		<?php 
		$counter = 1;
		$topic_buttons = get_field( 'topic_buttons' );
		if( $topic_buttons ){ ?>
			<div class="topics">
				<?php foreach( $topic_buttons as $button ){ 
					$label 					= $button[ 'label' ];
					$url 					= $button[ 'url' ];
					$bg_image_desktop 		= $button[ 'bg_image_desktop' ];
					$bg_image_tablet_phone 	= $button[ 'bg_image_tablet_phone' ];
					$accent_color 			= $button[ 'accent_color' ]; ?>
					<div class="item item-<?php echo $counter ?> <?php echo $accent_color; ?>">
						<a href="<?php echo esc_url( $url ); ?>" title="<?php echo $label; ?>" tabindex="<?php echo $counter + 10; ?>"><span class="hidden"><?php echo $label; ?></span></a>
						<span class="label"><?php echo $label; ?></span>
				      	<div class="circle-medium"><div class="cm-outer"></div><div class="cm-inner"></div><div class="rotator"><div class="rotate0"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate10"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate20"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate30"><div class="rotate-bar"></div><div class="rotate-bar"></div></div><div class="rotate40"><div class="rotate-bar"></div><div class="rotate-bar"></div></div></div><div class="icon"><svg width="30px" height="30px" fill="white" ><polygon class="fill" points="12.0454545 5 22.0454545 15 12.0454545 25 10 22.9545455 17.9545455 15 10 7.04545455" ></polygon></svg></div></div>
						<div class="blimp"><div class="bl-inner"></div><div class="bl-outer"></div><div class="bl-blink"></div></div>
						<?php echo $bg_image_desktop ? sprintf( '<div class="lazy-background bg" style="background-color:rgba(0, 0, 0, 0.0);" data-info="%s"></div>', $bg_image_desktop[ 'sizes' ][ 'thumb-video-caro' ] ) : ''; ?>
						<?php echo $bg_image_tablet_phone ? sprintf( '<div class="bg-mobile" style="background-image: url(%s);"></div>', $bg_image_tablet_phone[ 'sizes' ][ 'medium' ] ) : ''; ?>
					</div>
				<?php 
					$counter++;
				} ?>
			</div>
		<?php } ?>
	</section>

<?php get_footer(); ?>