(function() {
    tinymce.PluginManager.add('shortcode_button', function( editor, url ) {
        editor.addButton( 'shortcode_button', {
            text: tinyMCE_object.button_name,
            icon: 'custom-icon dashicons-before dashicons-format-status',
            // image: '../wp-content/themes/wpi/assets/images/testimonial.png',
            onclick: function() {
                editor.windowManager.open( {
                    title: tinyMCE_object.button_title,
                    body: [
                        {
                            type   : 'textbox',
                            name   : 'author_photo',
                            label  : 'Author Photo URL',
                            tooltip: 'Photo',
                            value  : ''
                        },
                        {
                            type   : 'textbox',
                            multiline: true,
                            name   : 'quote_text',
                            label  : 'Text',
                            tooltip: 'What client say',
                            value  : ''
                        },
                        {
                            type   : 'textbox',
                            name   : 'quote_author',
                            label  : 'Author Name',
                            tooltip: 'ie. John Doe, CEO of Company',
                            value  : ''
                        },
                        {
                            type   : 'textbox',
                            name   : 'quote_author_title',
                            label  : 'Author Position/Title',
                            tooltip: 'ie. Biology Major',
                            value  : ''
                        },
                    ],
                    onsubmit: function( e ) {
                        editor.insertContent( '[quote text="' + e.data.quote_text + '" author="' + e.data.quote_author + '" title="' + e.data.quote_author_title + '" photo="' + e.data.author_photo + '"]');
                    }
                });
            },
        });
    });
 
})();