<?php get_header(); ?>

<?php the_post(); ?>

	<?php 
	$error_title 	= get_field( '404_title', 'option' );
	$error_content 	= get_field( '404_content', 'option' ); ?>

	<section class="row error">
		<div class="wrap">
			<div class="inner">
				<div class="spacer"></div>
				<?php 
				if( $error_title ){
					$error_title_obj 	= explode( '<br />', $error_title );
					$error_title 		= '<span>'. implode( '</span><span>', $error_title_obj ) .'</span>';

					echo sprintf( '<h2>%s</h2>', $error_title ); 
				} ?>
				<?php echo $error_content; ?>
			</div>
		</div>
	</section>

<?php get_footer(); ?>