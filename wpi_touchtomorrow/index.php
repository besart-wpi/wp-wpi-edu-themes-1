<?php get_header(); ?>
        
    <div id="main" class="main-interior">
        
        <div id="robot_background">
            <div id="robot_head"></div>
            <div id="robot_body"></div>
            <div id="robot_left_arm"></div>
            <div id="robot_right_arm"></div>
            <div id="robot_treads"></div>
        </div><!-- #robot_background -->
        
        <div id="floating_callout">
            <p>Rain or Shine!<br />
            Free and open<br />
            to the public.</p>
        </div><!-- #floating_callout -->
        
        <div id="interior_content" class="the-content">
        	
            <?php if( have_posts() ): while( have_posts() ): the_post(); ?>
            
            <h1 id="page_title"><?php the_title(); ?></h1>
            <div class="interior_content_inner">
				<?php the_content(); ?>
            </div><!-- .interior_content_inner -->
            
            <?php endwhile; endif; ?>
        </div>
        
    </div><!-- #main -->
        
<?php get_footer(); ?>