<?php

// =================================================
//	:: Register Thumbnails ::
// -------------------------------------------------

	add_image_size( 'home_page_slideshow', 788, 397, true );
	add_image_size( 'home_page_video_thumbnail', 175, 98, true );
	add_image_size( 'home_page_video_poster', 480, 270, true );



// =================================================
//	:: Theme Options ::
// -------------------------------------------------

	add_filter( 'ot_use_theme_options', '__return_true' ); // Show the Theme Options page
	load_template( trailingslashit( get_stylesheet_directory() ) . 'includes/theme-options.php' );



// =================================================
//	:: Register Menus ::
// -------------------------------------------------

	if ( function_exists( 'register_nav_menus' ) ) {
		register_nav_menus( array(			
			'main_menu' 	=> 'Main Menu'
		));
	}
	


// =================================================
//	:: Post Type Frameworks ::
// ------------------------------------------------

	define('WPI_POST_TYPE_FRAMEWORKS', true);
	define('WPI_PTF_HOMEPAGESLIDESHOW', true); 
	define('WPI_PTF_VIDEOGALLERY', true);
	
	// Remove editor from Home Page Slideshow
	add_filter( 'wpi_cpt_home_page_slideshow_supports', 'custom_wpi_cpt_home_page_slideshow_supports');
	function custom_wpi_cpt_home_page_slideshow_supports() {
		return array('title','thumbnail');
	}
	

// =================================================
//	:: Post Ordering ::
// -------------------------------------------------
// Adds an interface to drag and drop posts into a custom order (menu_order)
// $post_order_whitelist array to set allowed post types

	global $post_order_whitelist;
	$post_order_whitelist = array('home_page_slideshow');

	
// =================================================
//	:: Enqueue Scripts and Styles ::
// -------------------------------------------------

	function wpi_child_enqueue(){
		
		wp_enqueue_style( 'wpi-child', get_stylesheet_uri(), array(), CACHE_THEM_THEMES_UPDATE_VERSION );
		wp_enqueue_style( 'wpi-child-responsive', get_stylesheet_directory_uri() . '/css/responsive.css' );
		wp_enqueue_style( 'wpi-child-superfish', get_stylesheet_directory_uri() . '/css/superfish.css' );
		wp_enqueue_style( 'wpi-child-videojs', get_stylesheet_directory_uri() . '/js/videojs/video-js.min.css' );
		
		wp_enqueue_script('underscore');
        wp_enqueue_script('modernizr', get_stylesheet_directory_uri() . '/js/modernizr.custom.js');
        wp_enqueue_script('jquery');
		wp_enqueue_script('jquery-cycle-2', get_stylesheet_directory_uri() . '/js/jquery.cycle2.min.js', array( 'jquery' ), true);
		wp_enqueue_script('jquery-cycle-2-carousel', get_stylesheet_directory_uri() . '/js/jquery.cycle2.carousel.min.js', array( 'jquery', 'jquery-cycle-2' ), true);	
		wp_enqueue_script('jquery-easing', get_stylesheet_directory_uri() . '/js/jquery.easing.1.3.js', array( 'jquery' ), true);
		wp_enqueue_script('jquery-superfish', get_stylesheet_directory_uri() . '/js/superfish.js', array( 'jquery' ), true);
		wp_enqueue_script('jquery-supersubs', get_stylesheet_directory_uri() . '/js/supersubs.js', array( 'jquery' ), true);
		wp_enqueue_script('jquery-hoverintent', get_stylesheet_directory_uri() . '/js/hoverIntent.js', array( 'jquery' ), true);
		wp_enqueue_script('videojs', get_stylesheet_directory_uri() . '/js/videojs/video.js', array( 'jquery' ), true);
		
		wp_enqueue_script('wpi-child-custom', get_stylesheet_directory_uri() . '/js/custom.js', array( 'jquery' ), true);		
		
	}
	
	add_action( 'wp_enqueue_scripts', 'wpi_child_enqueue' );
	
// =================================================
//	:: VideoJS - SWF Path ::
// -------------------------------------------------

add_action('wp_head','videojs_swf_path');

function videojs_swf_path() {
	$output = '<script> videojs.options.flash.swf = "'. get_stylesheet_directory_uri() .'/js/videojs/video-js.swf" </script>';
	echo $output;
}



// =================================================
//	:: custom_get_attachment_id( $guid )
//  Returns the attachment ID of an image URL ($guid)
// -------------------------------------------------
function custom_get_attachment_id( $guid ) {
	return false;
}

/*
|--------------------------------------------------------------------------
| Admin Favicons
|--------------------------------------------------------------------------
|
| Use a b&w version of the front-facing favicon
|
*/
function add_favicon() {
    $favicon_url = get_stylesheet_directory_uri() . '/images/favicon-admin.png';
    echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';
}
add_action('login_head', 'add_favicon');
add_action('admin_head', 'add_favicon');

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Change login logo
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
function my_custom_login_logo() { ?>
    <img id="login-robot-logo" src="<?= get_stylesheet_directory_uri() ?>/images/login-logo.png">
    <style>
	    .login h1 a{background: none}
	    #login-robot-logo{
			pointer-events:none;
			position: absolute;
			left: 50%;
			margin-left: -264px;
			width: 510px;
			top: 75px;
	    }	    
    </style>
<?php }
add_action('login_head', 'my_custom_login_logo');


// =================================================
//  :: Add Class to Menu Items With Children ::
// ------------------------------------------------
function add_menu_parent_class( $items ) {
    
    $parents = array();
    foreach ( $items as $item ) {
        if ( $item->menu_item_parent && $item->menu_item_parent > 0 ) {
            $parents[] = $item->menu_item_parent;
        }
    }
    
    foreach ( $items as $item ) {
        if ( in_array( $item->ID, $parents ) ) {
            $item->classes[] = 'menu-item-has-children'; 
        }
    }
    
    return $items;    
}
add_filter( 'wp_nav_menu_objects', 'add_menu_parent_class' );